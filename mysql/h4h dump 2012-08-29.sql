SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `h4h` DEFAULT CHARACTER SET latin1 ;
USE `h4h` ;

-- -----------------------------------------------------
-- Table `h4h`.`faith`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `h4h`.`faith` (
  `faith` CHAR(60) NOT NULL ,
  PRIMARY KEY (`faith`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `h4h`.`ethnicity`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `h4h`.`ethnicity` (
  `ethnicity` CHAR(60) NOT NULL ,
  PRIMARY KEY (`ethnicity`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `h4h`.`person`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `h4h`.`person` (
  `PID` INT(11) NOT NULL AUTO_INCREMENT ,
  `Title` CHAR(10) NULL DEFAULT NULL ,
  `FName` CHAR(20) NOT NULL ,
  `LName` CHAR(20) NOT NULL ,
  `PrefName` CHAR(20) NULL DEFAULT NULL ,
  `Email` CHAR(200) NULL DEFAULT NULL ,
  `Landline` CHAR(20) NULL DEFAULT NULL ,
  `WorkPhone` CHAR(20) NULL DEFAULT NULL ,
  `Mobile` CHAR(20) NULL DEFAULT NULL ,
  `Address1` VARCHAR(200) NOT NULL ,
  `Address2` VARCHAR(200) NULL DEFAULT NULL ,
  `Suburb` VARCHAR(200) NULL DEFAULT NULL ,
  `City` VARCHAR(200) NOT NULL ,
  `Region` VARCHAR(200) NULL DEFAULT NULL ,
  `Country` VARCHAR(200) NOT NULL ,
  `PostCode` CHAR(10) NOT NULL ,
  `DateOfBirth` DATE NULL DEFAULT NULL ,
  `Ethnicity` CHAR(20) NULL DEFAULT NULL ,
  `Gender` CHAR(20) NULL DEFAULT NULL ,
  `DateOfDeath` DATE NULL DEFAULT NULL ,
  `isActive` TINYINT(1) NULL DEFAULT NULL ,
  `NatNews` varchar(20) NULL DEFAULT NULL ,
  `locNews` varchar(20) NULL DEFAULT NULL ,
  `Reports` varchar(20) NULL DEFAULT NULL ,
  `Magazines` varchar(20) NULL DEFAULT NULL ,
  `Faith` CHAR(200) NULL DEFAULT NULL ,
  `CreationDate` DATE NOT NULL ,
  `Referrer` CHAR(200) NULL DEFAULT NULL ,
  PRIMARY KEY (`PID`) ,
  INDEX `Fath_idx` (`Faith` ASC) ,
  INDEX `Ethnicity_idx` (`Ethnicity` ASC) ,
  CONSTRAINT `Fath`
    FOREIGN KEY (`Faith` )
    REFERENCES `h4h`.`faith` (`faith` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `Ethnicity`
    FOREIGN KEY (`Ethnicity` )
    REFERENCES `h4h`.`ethnicity` (`ethnicity` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `h4h`.`team`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `h4h`.`team` (
  `TID` INT(11) NOT NULL AUTO_INCREMENT ,
  `Name` CHAR(200) NOT NULL ,
  `Email` CHAR(200) NULL DEFAULT NULL ,
  `Landline` CHAR(20) NULL DEFAULT NULL ,
  `WorkPhone` CHAR(20) NULL DEFAULT NULL ,
  `Mobile` CHAR(20) NULL DEFAULT NULL ,
  `Address1` VARCHAR(200) NULL DEFAULT NULL ,
  `Address2` VARCHAR(200) NULL DEFAULT NULL ,
  `Suburb` VARCHAR(200) NULL DEFAULT NULL ,
  `City` VARCHAR(200) NULL DEFAULT NULL ,
  `Region` VARCHAR(200) NULL DEFAULT NULL ,
  `Country` VARCHAR(200) NULL DEFAULT NULL ,
  `PostCode` CHAR(10) NULL DEFAULT NULL ,
  PRIMARY KEY (`TID`) ,
  UNIQUE INDEX `Name` (`Name` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `h4h`.`org`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `h4h`.`org` (
  `OID` INT(11) NOT NULL AUTO_INCREMENT ,
  `Name` CHAR(200) NULL DEFAULT NULL ,
  `Address1` VARCHAR(200) NOT NULL ,
  `Address2` VARCHAR(200) NULL DEFAULT NULL ,
  `Suburb` VARCHAR(200) NULL DEFAULT NULL ,
  `City` VARCHAR(200) NOT NULL ,
  `Region` VARCHAR(200) NOT NULL ,
  `Country` VARCHAR(200) NOT NULL ,
  `PostCode` CHAR(10) NOT NULL ,
  PRIMARY KEY (`OID`) )
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `h4h`.`event`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `h4h`.`event` (
  `EID` INT(11) NOT NULL AUTO_INCREMENT ,
  `Name` CHAR(200) NOT NULL ,
  `EventDate` DATE NOT NULL ,
  `Address1` VARCHAR(200) NULL DEFAULT NULL ,
  `Address2` VARCHAR(200) NULL DEFAULT NULL ,
  `Suburb` VARCHAR(200) NULL DEFAULT NULL ,
  `City` VARCHAR(200) NULL DEFAULT NULL ,
  `Region` VARCHAR(200) NULL DEFAULT NULL ,
  `Country` VARCHAR(200) NULL DEFAULT NULL ,
  `PostCode` CHAR(10) NULL DEFAULT NULL ,
  PRIMARY KEY (`EID`) )
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `h4h`.`attends`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `h4h`.`attends` (
  `aid` INT(11) NOT NULL ,
  `pid` INT(11) NULL DEFAULT NULL ,
  `tid` INT(11) NULL DEFAULT NULL ,
  `oid` INT(11) NULL DEFAULT NULL ,
  `eid` INT(11) NOT NULL ,
  PRIMARY KEY (`aid`) ,
  INDEX `pid_idx` (`pid` ASC) ,
  INDEX `tid_idx` (`tid` ASC) ,
  INDEX `oid_idx` (`oid` ASC) ,
  INDEX `eid_idx` (`eid` ASC) ,
  CONSTRAINT `pid`
    FOREIGN KEY (`pid` )
    REFERENCES `h4h`.`person` (`PID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `tid`
    FOREIGN KEY (`tid` )
    REFERENCES `h4h`.`team` (`TID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `oid`
    FOREIGN KEY (`oid` )
    REFERENCES `h4h`.`org` (`OID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `eid`
    FOREIGN KEY (`eid` )
    REFERENCES `h4h`.`event` (`EID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `h4h`.`comments`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `h4h`.`comments` (
  `CID` INT(11) NOT NULL AUTO_INCREMENT ,
  `PID` INT(11) NULL DEFAULT NULL ,
  `OID` INT(11) NULL DEFAULT NULL ,
  `TID` INT(11) NULL DEFAULT NULL ,
  `dateMade` DATE NOT NULL ,
  `commenter` CHAR(20) NOT NULL ,
  `comment` TEXT NOT NULL ,
  `private` TINYINT(1) NOT NULL DEFAULT '0' ,
  `PAID` INT NULL DEFAULT NULL ,
  `FID` INT NULL DEFAULT NULL ,
  `TAID` INT NULL DEFAULT NULL ,
  `EID` INT NULL DEFAULT NULL ,
  PRIMARY KEY (`CID`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `h4h`.`contacts`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `h4h`.`contacts` (
  `CID` INT(11) NOT NULL AUTO_INCREMENT ,
  `PID` INT(11) NOT NULL ,
  `OID` INT(11) NOT NULL ,
  `title` CHAR(200) NULL DEFAULT NULL ,
  `email` CHAR(200) NULL DEFAULT NULL ,
  `Landline` CHAR(20) NULL DEFAULT NULL ,
  `WorkPhone` CHAR(20) NULL DEFAULT NULL ,
  `Mobile` CHAR(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`CID`) ,
  INDEX `PID_idx` (`PID` ASC) ,
  INDEX `OID_idx` (`OID` ASC) ,
  CONSTRAINT `PID`
    FOREIGN KEY (`PID` )
    REFERENCES `h4h`.`person` (`PID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `OID`
    FOREIGN KEY (`OID` )
    REFERENCES `h4h`.`org` (`OID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `h4h`.`fampartner`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `h4h`.`fampartner` (
  `FID` INT(11) NOT NULL AUTO_INCREMENT ,
  `Address1` VARCHAR(200) NOT NULL ,
  `Address2` VARCHAR(200) NULL DEFAULT NULL ,
  `Suburb` VARCHAR(200) NULL DEFAULT NULL ,
  `City` VARCHAR(200) NOT NULL ,
  `Region` VARCHAR(200) NULL DEFAULT NULL ,
  `Country` VARCHAR(200) NOT NULL ,
  `PostCode` CHAR(10) NOT NULL ,
  `Stage` CHAR(15) NULL DEFAULT NULL ,
  `Type` TINYINT(1) NOT NULL ,
  `Value` INT(10) UNSIGNED NULL DEFAULT NULL ,
  `Cost` INT(10) UNSIGNED NULL DEFAULT NULL ,
  `ConsentRequired` TINYINT(1) NOT NULL DEFAULT '1' ,
  `DateOfConsent` DATE NULL DEFAULT NULL ,
  `StartDate` DATE NOT NULL ,
  `EndDate` DATE NULL DEFAULT NULL ,
  `ActualEndDate` DATE NULL DEFAULT NULL ,
  PRIMARY KEY (`FID`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `h4h`.`maintenance`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `h4h`.`maintenance` (
  `MID` INT(11) NOT NULL AUTO_INCREMENT ,
  `WorkerID` INT(11) NOT NULL ,
  `OwnerID` INT(11) NOT NULL ,
  `Issue` CHAR(255) NULL DEFAULT NULL ,
  `Status` TINYINT(4) NOT NULL ,
  `Priority` TINYINT(4) NOT NULL ,
  PRIMARY KEY (`MID`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `h4h`.`member`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `h4h`.`member` (
  `mid` INT(11) NOT NULL AUTO_INCREMENT ,
  `pid` INT(11) NOT NULL ,
  `tid` INT(11) NOT NULL ,
  `title` ENUM('President','Secretary','Treasurer') NULL DEFAULT NULL ,
  PRIMARY KEY (`mid`) ,
  UNIQUE INDEX `tid_2` (`tid` ASC, `title` ASC) ,
  INDEX `pid` (`pid` ASC) ,
  INDEX `tid` (`tid` ASC) ,
  CONSTRAINT `Member_ibfk_1`
    FOREIGN KEY (`pid` )
    REFERENCES `h4h`.`person` (`PID` )
    ON DELETE CASCADE,
  CONSTRAINT `Member_ibfk_2`
    FOREIGN KEY (`tid` )
    REFERENCES `h4h`.`team` (`TID` ))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `h4h`.`task`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `h4h`.`task` (
  `TAID` INT(11) NOT NULL AUTO_INCREMENT ,
  `Name` CHAR(200) NULL DEFAULT NULL ,
  PRIMARY KEY (`TAID`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `h4h`.`oworksonev`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `h4h`.`oworksonev` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `OID` INT(11) NOT NULL ,
  `EID` INT(11) NOT NULL ,
  `TAID` INT(11) NOT NULL ,
  `DateOfWork` DATE NOT NULL ,
  `Hours` INT(10) UNSIGNED NOT NULL ,
  PRIMARY KEY (`ID`) ,
  INDEX `OID_idx` (`OID` ASC) ,
  INDEX `EID_idx` (`EID` ASC) ,
  INDEX `TAID_idx` (`TAID` ASC) ,
  CONSTRAINT `OID`
    FOREIGN KEY (`OID` )
    REFERENCES `h4h`.`org` (`OID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `EID`
    FOREIGN KEY (`EID` )
    REFERENCES `h4h`.`event` (`EID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `TAID`
    FOREIGN KEY (`TAID` )
    REFERENCES `h4h`.`task` (`TAID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `h4h`.`partnership`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `h4h`.`partnership` (
  `PAID` INT(11) NOT NULL AUTO_INCREMENT ,
  `PID` INT(11) NULL DEFAULT NULL ,
  `FID` INT(11) NULL DEFAULT NULL ,
  `SweatHours` INT(9) UNSIGNED NULL DEFAULT NULL ,
  `NumAdults` INT(4) UNSIGNED NULL DEFAULT NULL ,
  `NumChild` INT(4) UNSIGNED NULL DEFAULT NULL ,
  `Income` INT(10) UNSIGNED NOT NULL ,
  `DebtProof` TINYINT(1) NULL DEFAULT NULL ,
  `BudgetDone` TINYINT(1) NULL DEFAULT NULL ,
  `CreditCheck` TINYINT(1) NULL DEFAULT NULL ,
  `ConvictionsDeclared` TINYINT(1) NULL DEFAULT NULL ,
  `Citizenship` CHAR(20) NOT NULL ,
  `UmbrellaAgreement` DATE NULL DEFAULT NULL ,
  `RentalAgreement` DATE NULL DEFAULT NULL ,
  `RentalAgreementReview` DATE NULL DEFAULT NULL ,
  `DebtReview` DATE NULL DEFAULT NULL ,
  `LTSPADate` DATE NULL DEFAULT NULL ,
  `DateLeft` DATE NULL DEFAULT NULL ,
  `ReasonLeft` CHAR(200) NULL DEFAULT NULL ,
  `Status` CHAR(10) NULL DEFAULT NULL ,
  `MarketValue` INT(10) UNSIGNED NOT NULL ,
  `LoanPaymentAmt` INT(10) UNSIGNED NULL DEFAULT NULL ,
  `CeremonyDate` DATE NULL DEFAULT NULL ,
  PRIMARY KEY (`PAID`) ,
  INDEX `PID_idx` (`PID` ASC) ,
  INDEX `FID_idx` (`FID` ASC) ,
  CONSTRAINT `PID`
    FOREIGN KEY (`PID` )
    REFERENCES `h4h`.`person` (`PID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FID`
    FOREIGN KEY (`FID` )
    REFERENCES `h4h`.`fampartner` (`FID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `h4h`.`oworksonfam`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `h4h`.`oworksonfam` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `OID` INT(11) NOT NULL ,
  `PAID` INT(11) NOT NULL ,
  `TAID` INT(11) NOT NULL ,
  `DateOfWork` DATE NOT NULL ,
  `Hours` INT(10) UNSIGNED NOT NULL ,
  PRIMARY KEY (`ID`) ,
  INDEX `OID_idx` (`OID` ASC) ,
  INDEX `PAID_idx` (`PAID` ASC) ,
  INDEX `TAID_idx` (`TAID` ASC) ,
  CONSTRAINT `OID`
    FOREIGN KEY (`OID` )
    REFERENCES `h4h`.`org` (`OID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `PAID`
    FOREIGN KEY (`PAID` )
    REFERENCES `h4h`.`partnership` (`PAID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `TAID`
    FOREIGN KEY (`TAID` )
    REFERENCES `h4h`.`task` (`TAID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `h4h`.`skill`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `h4h`.`skill` (
  `skillID` INT(11) NOT NULL AUTO_INCREMENT ,
  `name` CHAR(40) NULL DEFAULT NULL ,
  `description` TEXT NULL DEFAULT NULL ,
  PRIMARY KEY (`skillID`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `h4h`.`personskill`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `h4h`.`personskill` (
  `PID` INT(11) NOT NULL ,
  `skillID` INT(11) NOT NULL ,
  `level` ENUM('Untrained','Amature','Professional') NULL DEFAULT NULL ,
  PRIMARY KEY (`skillID`, `PID`) ,
  INDEX `skillID_idx` (`skillID` ASC) ,
  INDEX `PID_idx` (`PID` ASC) ,
  CONSTRAINT `skillID`
    FOREIGN KEY (`skillID` )
    REFERENCES `h4h`.`skill` (`skillID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `PID`
    FOREIGN KEY (`PID` )
    REFERENCES `h4h`.`person` (`PID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `h4h`.`pworksonev`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `h4h`.`pworksonev` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `PID` INT(11) NOT NULL ,
  `EID` INT(11) NOT NULL ,
  `TAID` INT(11) NOT NULL ,
  `DateOfWork` DATE NOT NULL ,
  `Hours` INT(10) UNSIGNED NOT NULL ,
  PRIMARY KEY (`ID`) ,
  INDEX `PID_idx` (`PID` ASC) ,
  INDEX `EID_idx` (`EID` ASC) ,
  INDEX `TAID_idx` (`TAID` ASC) ,
  CONSTRAINT `PID`
    FOREIGN KEY (`PID` )
    REFERENCES `h4h`.`person` (`PID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `EID`
    FOREIGN KEY (`EID` )
    REFERENCES `h4h`.`event` (`EID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `TAID`
    FOREIGN KEY (`TAID` )
    REFERENCES `h4h`.`task` (`TAID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `h4h`.`pworksonfam`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `h4h`.`pworksonfam` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `PID` INT(11) NOT NULL ,
  `PAID` INT(11) NOT NULL ,
  `TID` INT(11) NOT NULL ,
  `DateOfWork` DATE NOT NULL ,
  `Hours` INT(10) UNSIGNED NOT NULL ,
  PRIMARY KEY (`ID`) ,
  INDEX `PID_idx` (`PID` ASC) ,
  INDEX `PAID_idx` (`PAID` ASC) ,
  CONSTRAINT `PID`
    FOREIGN KEY (`PID` )
    REFERENCES `h4h`.`person` (`PID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `PAID`
    FOREIGN KEY (`PAID` )
    REFERENCES `h4h`.`partnership` (`PAID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `h4h`.`tworksonev`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `h4h`.`tworksonev` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `TID` INT(11) NOT NULL ,
  `EID` INT(11) NOT NULL ,
  `TAID` INT(11) NOT NULL ,
  `DateOfWork` DATE NOT NULL ,
  `Hours` INT(10) UNSIGNED NOT NULL ,
  PRIMARY KEY (`ID`) ,
  INDEX `TID_idx` (`TID` ASC) ,
  INDEX `EID_idx` (`EID` ASC) ,
  INDEX `TAID_idx` (`TAID` ASC) ,
  CONSTRAINT `TID`
    FOREIGN KEY (`TID` )
    REFERENCES `h4h`.`team` (`TID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `EID`
    FOREIGN KEY (`EID` )
    REFERENCES `h4h`.`event` (`EID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `TAID`
    FOREIGN KEY (`TAID` )
    REFERENCES `h4h`.`task` (`TAID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `h4h`.`tworksonfam`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `h4h`.`tworksonfam` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `TID` INT(11) NOT NULL ,
  `PAID` INT(11) NOT NULL ,
  `TAID` INT(11) NOT NULL ,
  `DateOfWork` DATE NOT NULL ,
  `Hours` INT(10) UNSIGNED NOT NULL ,
  PRIMARY KEY (`ID`) ,
  INDEX `TID_idx` (`TID` ASC) ,
  INDEX `PAID_idx` (`PAID` ASC) ,
  INDEX `TAID_idx` (`TAID` ASC) ,
  CONSTRAINT `TID`
    FOREIGN KEY (`TID` )
    REFERENCES `h4h`.`team` (`TID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `PAID`
    FOREIGN KEY (`PAID` )
    REFERENCES `h4h`.`partnership` (`PAID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `TAID`
    FOREIGN KEY (`TAID` )
    REFERENCES `h4h`.`task` (`TAID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
