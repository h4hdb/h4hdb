-- MySQL dump 10.13  Distrib 5.5.25, for Linux (i686)
--
-- Host: localhost    Database: h4h
-- ------------------------------------------------------
-- Server version	5.5.25-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `h4h`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `h4h` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `h4h`;

--
-- Table structure for table `Attends`
--

DROP TABLE IF EXISTS `Attends`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Attends` (
  `aid` int(11) NOT NULL,
  `pid` int(11) DEFAULT NULL,
  `tid` int(11) DEFAULT NULL,
  `oid` int(11) DEFAULT NULL,
  `eid` int(11) NOT NULL,
  PRIMARY KEY (`aid`) ,
  INDEX `pid_idx` (`pid` ASC) ,
  INDEX `tid_idx` (`tid` ASC) ,
  INDEX `oid_idx` (`oid` ASC) ,
  INDEX `eid_idx` (`eid` ASC) ,
  CONSTRAINT `pid`
    FOREIGN KEY (`pid` )
    REFERENCES `h4h`.`person` (`PID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `tid`
    FOREIGN KEY (`tid` )
    REFERENCES `h4h`.`team` (`TID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `oid`
    FOREIGN KEY (`oid` )
    REFERENCES `h4h`.`org` (`OID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `eid`
    FOREIGN KEY (`eid` )
    REFERENCES `h4h`.`event` (`EID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Comments`
--

DROP TABLE IF EXISTS `Comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Comments` (
  `CID` int(11) NOT NULL AUTO_INCREMENT,
  `PID` int(11) DEFAULT NULL,
  `OID` int(11) DEFAULT NULL,
  `TID` int(11) DEFAULT NULL,
  `dateMade` date NOT NULL,
  `commenter` char(20) NOT NULL,
  `comment` text NOT NULL,
  `private` tinyint(1) NOT NULL DEFAULT '0',
  `PAID` INT NULL DEFAULT NULL ,
  `FID` INT NULL DEFAULT NULL ,
  `TAID` INT NULL DEFAULT NULL ,
  `EID` INT NULL DEFAULT NULL ,
  PRIMARY KEY (`CID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Contacts`
--

DROP TABLE IF EXISTS `Contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Contacts` (
  `CID` int(11) NOT NULL AUTO_INCREMENT,
  `PID` int(11) NOT NULL,
  `OID` int(11) NOT NULL,
  `title` char(200) DEFAULT NULL,
  `email` char(200) DEFAULT NULL,
  `Landline` char(20) DEFAULT NULL,
  `WorkPhone` char(20) DEFAULT NULL,
  `Mobile` char(20) DEFAULT NULL,
  PRIMARY KEY (`CID`),
  INDEX `PID_idx` (`PID` ASC) ,
  INDEX `OID_idx` (`OID` ASC) ,
  CONSTRAINT `PID`
    FOREIGN KEY (`PID` )
    REFERENCES `h4h`.`person` (`PID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `OID`
    FOREIGN KEY (`OID` )
    REFERENCES `h4h`.`org` (`OID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Event`
--

DROP TABLE IF EXISTS `Event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Event` (
  `EID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` char(200) NOT NULL,
  `EventDate` date NOT NULL,
  `Address1` varchar(200) DEFAULT NULL,
  `Address2` varchar(200) DEFAULT NULL,
  `Suburb` varchar(200) DEFAULT NULL,
  `City` varchar(200) DEFAULT NULL,
  `Region` varchar(200) DEFAULT NULL,
  `Country` varchar(200) DEFAULT NULL,
  `PostCode` char(10) DEFAULT NULL,
  PRIMARY KEY (`EID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `FamPartner`
--

DROP TABLE IF EXISTS `FamPartner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FamPartner` (
  `FID` int(11) NOT NULL AUTO_INCREMENT,
  `Address1` varchar(200) DEFAULT NULL,
  `Address2` varchar(200) DEFAULT NULL,
  `Suburb` varchar(200) DEFAULT NULL,
  `City` varchar(200) DEFAULT NULL,
  `Region` varchar(200) DEFAULT NULL,
  `Country` varchar(200) DEFAULT NULL,
  `PostCode` char(10) DEFAULT NULL,
  `Stage` char(15) DEFAULT NULL,
  `Type` tinyint(1) DEFAULT NULL,
  `Value` int(10) unsigned DEFAULT NULL,
  `Cost` int(10) unsigned DEFAULT NULL,
  `ConsentRequired` tinyint(1) DEFAULT '1',
  `DateOfConsent` date DEFAULT NULL,
  `StartDate` date NOT NULL,
  `EndDate` date DEFAULT NULL,
  `ActualEndDate` date DEFAULT NULL,
  PRIMARY KEY (`FID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Keyword`
--

DROP TABLE IF EXISTS `Keyword`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Keyword` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(40) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Maintenance`
--

DROP TABLE IF EXISTS `Maintenance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Maintenance` (
  `MID` int(11) NOT NULL AUTO_INCREMENT,
  `WorkerID` int(11) NOT NULL,
  `OwnerID` int(11) NOT NULL,
  `Issue` char(255) DEFAULT NULL,
  `Status` tinyint(4) NOT NULL,
  `Priority` tinyint(4) NOT NULL,
  PRIMARY KEY (`MID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Member`
--

DROP TABLE IF EXISTS `Member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Member` (
  `mid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `tid` int(11) NOT NULL,
  `title` enum('President','Secretary','Treasurer') DEFAULT NULL,
  PRIMARY KEY (`mid`),
  UNIQUE KEY `tid_2` (`tid`,`title`),
  KEY `pid` (`pid`),
  KEY `tid` (`tid`),
  CONSTRAINT `Member_ibfk_1` FOREIGN KEY (`pid`) REFERENCES `Person` (`PID`) ON DELETE CASCADE,
  CONSTRAINT `Member_ibfk_2` FOREIGN KEY (`tid`) REFERENCES `Team` (`TID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `OWorksOnEv`
--

DROP TABLE IF EXISTS `OWorksOnEv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OWorksOnEv` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `OID` int(11) NOT NULL,
  `EID` int(11) NOT NULL,
  `TID` int(11) NOT NULL,
  `DateOfWork` date NOT NULL,
  `Hours` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ID`) ,
  INDEX `OID_idx` (`OID` ASC) ,
  INDEX `EID_idx` (`EID` ASC) ,
  INDEX `TAID_idx` (`TAID` ASC) ,
  CONSTRAINT `OID`
    FOREIGN KEY (`OID` )
    REFERENCES `h4h`.`org` (`OID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `EID`
    FOREIGN KEY (`EID` )
    REFERENCES `h4h`.`event` (`EID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `TAID`
    FOREIGN KEY (`TAID` )
    REFERENCES `h4h`.`task` (`TAID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `OWorksOnFam`
--

DROP TABLE IF EXISTS `OWorksOnFam`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OWorksOnFam` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `OID` int(11) NOT NULL,
  `PAID` int(11) NOT NULL,
  `TID` int(11) NOT NULL,
  `DateOfWork` date NOT NULL,
  `Hours` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ID`) ,
  INDEX `OID_idx` (`OID` ASC) ,
  INDEX `PAID_idx` (`PAID` ASC) ,
  INDEX `TAID_idx` (`TAID` ASC) ,
  CONSTRAINT `OID`
    FOREIGN KEY (`OID` )
    REFERENCES `h4h`.`org` (`OID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `PAID`
    FOREIGN KEY (`PAID` )
    REFERENCES `h4h`.`partnership` (`PAID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `TAID`
    FOREIGN KEY (`TAID` )
    REFERENCES `h4h`.`task` (`TAID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Org`
--

DROP TABLE IF EXISTS `Org`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Org` (
  `OID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` char(200) DEFAULT NULL,
  `Address1` varchar(200) NOT NULL,
  `Address2` varchar(200) DEFAULT NULL,
  `Suburb` varchar(200) DEFAULT NULL,
  `City` varchar(200) NOT NULL,
  `Region` varchar(200) NOT NULL,
  `Country` varchar(200) NOT NULL,
  `PostCode` char(10) NOT NULL,
  PRIMARY KEY (`OID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PWorksOnEv`
--

DROP TABLE IF EXISTS `PWorksOnEv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PWorksOnEv` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PID` int(11) NOT NULL,
  `EID` int(11) NOT NULL,
  `TID` int(11) NOT NULL,
  `DateOfWork` date NOT NULL,
  `Hours` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ID`) ,
  INDEX `PID_idx` (`PID` ASC) ,
  INDEX `EID_idx` (`EID` ASC) ,
  INDEX `TAID_idx` (`TAID` ASC) ,
  CONSTRAINT `PID`
    FOREIGN KEY (`PID` )
    REFERENCES `h4h`.`person` (`PID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `EID`
    FOREIGN KEY (`EID` )
    REFERENCES `h4h`.`event` (`EID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `TAID`
    FOREIGN KEY (`TAID` )
    REFERENCES `h4h`.`task` (`TAID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PWorksOnFam`
--

DROP TABLE IF EXISTS `PWorksOnFam`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PWorksOnFam` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PID` int(11) NOT NULL,
  `PAID` int(11) NOT NULL,
  `TID` int(11) NOT NULL,
  `DateOfWork` date NOT NULL,
  `Hours` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ID`) ,
  INDEX `PID_idx` (`PID` ASC) ,
  INDEX `PAID_idx` (`PAID` ASC) ,
  CONSTRAINT `PID`
    FOREIGN KEY (`PID` )
    REFERENCES `h4h`.`person` (`PID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `PAID`
    FOREIGN KEY (`PAID` )
    REFERENCES `h4h`.`partnership` (`PAID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Partnership`
--

DROP TABLE IF EXISTS `Partnership`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Partnership` (
  `PAID` int(11) NOT NULL AUTO_INCREMENT,
  `PID` int(11) DEFAULT NULL,
  `FID` int(11) DEFAULT NULL,
  `SweatHours` int(9) unsigned DEFAULT NULL,
  `NumAdults` int(4) unsigned DEFAULT NULL,
  `NumChild` int(4) unsigned DEFAULT NULL,
  `Income` int(10) unsigned NOT NULL,
  `DebtProof` tinyint(1) DEFAULT NULL,
  `BudgetDone` tinyint(1) DEFAULT NULL,
  `CreditCheck` tinyint(1) DEFAULT NULL,
  `ConvictionsDeclared` tinyint(1) DEFAULT NULL,
  `Citizenship` char(20) NOT NULL,
  `UmbrellaAgreement` date DEFAULT NULL,
  `RentalAgreement` date DEFAULT NULL,
  `RentalAgreementReview` date NOT NULL,
  `DebtReview` date NOT NULL,
  `LTSPADate` date DEFAULT NULL,
  `DateLeft` date DEFAULT NULL,
  `ReasonLeft` char(200) DEFAULT NULL,
  `Status` char(10) DEFAULT NULL,
  `MarketValue` int(10) unsigned NOT NULL,
  `LoanPaymentAmt` int(10) unsigned NOT NULL,
  `CeremonyDate` date DEFAULT NULL,
  PRIMARY KEY (`PAID`) ,
  INDEX `PID_idx` (`PID` ASC) ,
  INDEX `FID_idx` (`FID` ASC) ,
  CONSTRAINT `PID`
    FOREIGN KEY (`PID` )
    REFERENCES `h4h`.`person` (`PID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FID`
    FOREIGN KEY (`FID` )
    REFERENCES `h4h`.`fampartner` (`FID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Person`
--

DROP TABLE IF EXISTS `Person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Person` (
  `PID` int(11) NOT NULL AUTO_INCREMENT,
  `Title` char(10) DEFAULT NULL,
  `FName` char(20) NOT NULL,
  `LName` char(20) NOT NULL,
  `PrefName` char(20) DEFAULT NULL,
  `Email` char(200) DEFAULT NULL,
  `Landline` char(20) DEFAULT NULL,
  `WorkPhone` char(20) DEFAULT NULL,
  `Mobile` char(20) DEFAULT NULL,
  `Address1` varchar(200) DEFAULT NULL,
  `Address2` varchar(200) DEFAULT NULL,
  `Suburb` varchar(200) DEFAULT NULL,
  `City` varchar(200) DEFAULT NULL,
  `Region` varchar(200) DEFAULT NULL,
  `Country` varchar(200) DEFAULT NULL,
  `PostCode` char(10) DEFAULT NULL,
  `DateOfBirth` date DEFAULT NULL,
  `Ethnicity` char(20) DEFAULT NULL,
  `Gender` char(20) DEFAULT NULL,
  `DateOfDeath` date DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  `requestNews` enum('Electronic','Hard','Both') DEFAULT NULL,
  `requestNatNews` enum('Electronic','Hard','Both') DEFAULT NULL,
  `requestReports` enum('Electronic','Hard','Both') DEFAULT NULL,
  `requestMags` enum('Electronic','Hard','Both') DEFAULT NULL,
  `Faith` char(200) DEFAULT NULL,
  `CreationDate` date DEFAULT NULL,
  `Referrer` char(200) DEFAULT NULL,
  PRIMARY KEY (`PID`) ,
  INDEX `Fath_idx` (`Faith` ASC) ,
  INDEX `Ethnicity_idx` (`Ethnicity` ASC) ,
  CONSTRAINT `Fath`
    FOREIGN KEY (`Faith` )
    REFERENCES `h4h`.`faith` (`faith` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `Ethnicity`
    FOREIGN KEY (`Ethnicity` )
    REFERENCES `h4h`.`ethnicity` (`ethnicity` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PersonKeyword`
--

DROP TABLE IF EXISTS `PersonKeyword`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PersonKeyword` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PID` int(11) NOT NULL,
  `KID` int(11) NOT NULL,
  `level` int(11) DEFAULT NULL,
  `constraints` text,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `oneperperson` (`PID`,`KID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `TWorksOnEv`
--

DROP TABLE IF EXISTS `TWorksOnEv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TWorksOnEv` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TID` int(11) NOT NULL,
  `EID` int(11) NOT NULL,
  `TAID` int(11) NOT NULL,
  `DateOfWork` date NOT NULL,
  `Hours` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ID`) ,
  INDEX `TID_idx` (`TID` ASC) ,
  INDEX `EID_idx` (`EID` ASC) ,
  INDEX `TAID_idx` (`TAID` ASC) ,
  CONSTRAINT `TID`
    FOREIGN KEY (`TID` )
    REFERENCES `h4h`.`team` (`TID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `EID`
    FOREIGN KEY (`EID` )
    REFERENCES `h4h`.`event` (`EID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `TAID`
    FOREIGN KEY (`TAID` )
    REFERENCES `h4h`.`task` (`TAID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `TWorksOnFam`
--

DROP TABLE IF EXISTS `TWorksOnFam`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TWorksOnFam` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TID` int(11) NOT NULL,
  `PAID` int(11) NOT NULL,
  `TAID` int(11) NOT NULL,
  `DateOfWork` date NOT NULL,
  `Hours` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ID`) ,
  INDEX `TID_idx` (`TID` ASC) ,
  INDEX `PAID_idx` (`PAID` ASC) ,
  INDEX `TAID_idx` (`TAID` ASC) ,
  CONSTRAINT `TID`
    FOREIGN KEY (`TID` )
    REFERENCES `h4h`.`team` (`TID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `PAID`
    FOREIGN KEY (`PAID` )
    REFERENCES `h4h`.`partnership` (`PAID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `TAID`
    FOREIGN KEY (`TAID` )
    REFERENCES `h4h`.`task` (`TAID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Task`
--

DROP TABLE IF EXISTS `Task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Task` (
  `TID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` char(200) DEFAULT NULL,
  PRIMARY KEY (`TID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Team`
--

DROP TABLE IF EXISTS `Team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Team` (
  `TID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` char(200) NOT NULL,
  `Email` char(200) DEFAULT NULL,
  `Landline` char(20) DEFAULT NULL,
  `WorkPhone` char(20) DEFAULT NULL,
  `Mobile` char(20) DEFAULT NULL,
  `Address1` varchar(200) DEFAULT NULL,
  `Address2` varchar(200) DEFAULT NULL,
  `Suburb` varchar(200) DEFAULT NULL,
  `City` varchar(200) DEFAULT NULL,
  `Region` varchar(200) DEFAULT NULL,
  `Country` varchar(200) DEFAULT NULL,
  `PostCode` char(10) DEFAULT NULL,
  PRIMARY KEY (`TID`),
  UNIQUE KEY `Name` (`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ethnicity`
--

DROP TABLE IF EXISTS `ethnicity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ethnicity` (
  `ethnicity` char(60) NOT NULL,
  PRIMARY KEY (`ethnicity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `faith`
--

DROP TABLE IF EXISTS `faith`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faith` (
  `faith` char(60) NOT NULL,
  PRIMARY KEY (`faith`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-09-09 15:00:04
