/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package h4hdb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import h4hdb.configSettings.configReader;

/**
 *
 * @author Mehendra
 */
public class DBConnectionSingleton extends Thread{
	
	private static Connection con; 
    
    @Override
	public void run() {
    	// Helps to keep the connection alive, if the connection to the db is set to timeout 
    	// after a respective amount of time. 
		while(true) {
			try {
				Thread.sleep(20000);
				if(con.isValid(getPriority())){
					con.getClientInfo();  
				}
			} catch (InterruptedException e) {
			} catch (SQLException e) {
			} 
		}
	}

	private DBConnectionSingleton() {
    }
    
    public static DBConnectionSingleton getInstance() {
        return DBConnectionSingletonHolder.INSTANCE;
    }
    
    private static class DBConnectionSingletonHolder {
        private static final DBConnectionSingleton INSTANCE = new DBConnectionSingleton();
    }
    
    public static Connection getDbConnection(String userName, String password) throws ClassNotFoundException, SQLException
    {
        DBConnectionSingleton.getInstance();
        String driverName = "org.gjt.mm.mysql.Driver";
        Class.forName(driverName);

        String serverName = new configReader().read("DatabaseConnectionString");
        String mydatabase = new configReader().read("DatabaseName");
        String url = "jdbc:mysql://" + serverName + "/" + mydatabase;
        
        con = DriverManager.getConnection(url, userName, password);

        return con; 
    }
}
