/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package h4hdb.configSettings;

import java.io.IOException;
import java.util.Properties;


/**
 * This class is intended to handle the loading of the configuration details
 * @author Mehendra
 */
public class configReader {
    
    Properties configFile;
    
        
    /**
     * default constructor
     */
    public configReader()
    {
    }
    
    /**
     * Reads a value from the configuration file (.properties file)
     * @param settingName --- Key to look for
     * @return --- The value matching key
     */
    public String read(String settingName)
    {
        try
        {
            if(configFile == null)
            {
                configFile = new Properties();
                configFile.load(this.getClass().getClassLoader().getResourceAsStream("h4hdb.properties"));        
            }
             return configFile.getProperty(settingName);            
        }
        catch(Exception ex)
        {
            return null;
        }
    }    
}

