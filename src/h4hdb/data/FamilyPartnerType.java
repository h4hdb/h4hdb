/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package h4hdb.data;

/**
 * This is the type of family partner
 * @author Mehendra
 */
public enum FamilyPartnerType {
    None(0),Build(1),BrushWithKindness(2);
    private final int id;
    
    FamilyPartnerType(int id) { this.id = id; }
    
    public int getValue() { return id; }    
}
