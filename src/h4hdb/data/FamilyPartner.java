/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package h4hdb.data;

import com.sun.org.apache.xerces.internal.dom.DocumentImpl;
import h4hdb.reporting.IReportItem;
import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.w3c.dom.*;


/**
 * Holds the family partner object, and is responsible for all data store  actions.
 * @author Geng 
 */
public class FamilyPartner implements IReportItem {
	
	public static final String FIELD_ID = "FID";
	public static final String FIELD_ADDR1 = "Address1";
	public static final String FIELD_ADDR2 = "Address2";
	public static final String FIELD_SUBURB = "Suburb";
	public static final String FIELD_CITY = "City";
	public static final String FIELD_REGION = "Region";
	public static final String FIELD_COUNTRY = "Country";
	public static final String FIELD_POSTCODE = "PostCode";
        
        
	private int fID;
	private Address location;
	private String stage;
	private FamilyPartnerType type;
	private int value;
	private int cost;
	private boolean consentRequired;
	private Date dateOfConsent;
	private Date startDate;
	private Date endDate;
	private Date actualEndDate;
	private List<String> errorMessages;
        private List<PartnerShip> partnerships;

    
    /**
     * Constructor setting all values used when building a family partnership object
     *
     * @param location --- The address of the property
     * @param stage --- Stage where the family partnership is at
     * @param type --- Type of the family partnership
     * @param value --- Value of the property
     * @param cost --- The cost of the property
     * @param consentRequired --- An indication if consent is required 
     * @param startDate --- When the partnership started
     * @param endDate --- Estimated end date
     */        
    public FamilyPartner(Address location, String stage, FamilyPartnerType type, 
            int value, int cost, boolean consentRequired, Date startDate, Date endDate) {
        errorMessages = new ArrayList<String>();
        this.location = location;
        this.stage = stage;
        this.type = type;
        this.value = value;
        this.cost = cost;
        this.consentRequired = consentRequired;
        this.startDate = startDate;
        this.endDate = endDate;
    }
    
/**
     * Constructor setting all values used when building a family partnership object
     *
     * @param fID --- Family partnership id from the database
     * @param location --- The address of the property
     * @param stage --- Stage where the family partnership is at
     * @param type --- Type of the family partnership
     * @param value --- Value of the property
     * @param cost --- The cost of the property
     * @param consentRequired --- An indication if consent is required 
     * @param dateOfConsent --- Date when consent was granted.
     * @param startDate --- When the partnership started
     * @param endDate --- Estimated end date
     * @param actualEndDate --- When the partnership was completed
     */            
	public FamilyPartner(int fID, Address location, String stage,
			FamilyPartnerType type, int value, int cost,
			boolean consentRequired, Date dateOfConsent, Date startDate,
			Date endDate, Date actualEndDate) {
		errorMessages = new ArrayList<String>();
		this.fID = fID;
		this.location = location;
		this.stage = stage;
		this.type = type;
		this.value = value;
		this.cost = cost;
		this.consentRequired = consentRequired;
		this.dateOfConsent = dateOfConsent;
		this.startDate = startDate;
		this.endDate = endDate;
		this.actualEndDate = actualEndDate;
	}
	
    /**
     * Sets the actual end date
     * @param actualEndDate
     */
    public void setActualEndDate(Date actualEndDate){
	        this.actualEndDate = actualEndDate;
	    }
	    
    /**
     * Sets the consent date
     * @param consentDate
     */
    public void consentDate(Date consentDate){
	        this.dateOfConsent = consentDate;
	    }

	private FamilyPartnerType resolveFamilyPartnerType(int type) {
		if (type == 0) {
                return FamilyPartnerType.None;
            }
		else if (type == 1) {
                return FamilyPartnerType.Build;
            }
		else if (type == 2) {
                return FamilyPartnerType.BrushWithKindness;
            }
		return null;
	}

        
     /**
     * Constructor using the result set from the database
     * @param rs --- raw result set from the database
     * @throws SQLException
     */
    public FamilyPartner(ResultSet rs) throws SQLException {
		this.fID = rs.getInt("FID");
		this.stage = rs.getString("Stage");
		this.type = resolveFamilyPartnerType(rs.getInt("type"));
		this.value = rs.getInt("value");
		this.cost = rs.getInt("cost");                
		this.consentRequired = rs.getBoolean("ConsentRequired");
		this.dateOfConsent = rs.getDate("DateOfConsent");
		this.startDate = rs.getDate("StartDate");
		this.endDate = rs.getDate("EndDate");
		this.actualEndDate = rs.getDate("actualEndDate");
		this.location = new Address(rs.getString("Address1"),
				rs.getString("Address2"), rs.getString("Suburb"),
				rs.getString("City"), rs.getString("Region"),
				rs.getString("Country"), rs.getString("PostCode"));
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 97 * hash + this.fID;
		hash = 97 * hash
				+ (this.location != null ? this.location.hashCode() : 0);
		hash = 97 * hash + (this.stage != null ? this.stage.hashCode() : 0);
		hash = 97 * hash + (this.type != null ? this.type.hashCode() : 0);
		hash = 97 * hash + this.value;
		hash = 97 * hash + this.cost;
		hash = 97 * hash + (this.consentRequired ? 1 : 0);
		hash = 97
				* hash
				+ (this.dateOfConsent != null ? this.dateOfConsent.hashCode()
						: 0);
		hash = 97 * hash
				+ (this.startDate != null ? this.startDate.hashCode() : 0);
		hash = 97 * hash + (this.endDate != null ? this.endDate.hashCode() : 0);
		hash = 97
				* hash
				+ (this.actualEndDate != null ? this.actualEndDate.hashCode()
						: 0);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final FamilyPartner other = (FamilyPartner) obj;
		if (this.fID != other.fID) {
			return false;
		}
		if (this.location != other.location
				&& (this.location == null || !this.location
						.equals(other.location))) {
			return false;
		}
		if ((this.stage == null) ? (other.stage != null) : !this.stage
				.equals(other.stage)) {
			return false;
		}
		if (this.type != other.type) {
			return false;
		}
		if (this.value != other.value) {
			return false;
		}
		if (this.cost != other.cost) {
			return false;
		}
		if (this.dateOfConsent != other.dateOfConsent
				&& (this.dateOfConsent == null || !this.dateOfConsent
						.equals(other.dateOfConsent))) {
			return false;
		}
		if (this.startDate != other.startDate
				&& (this.startDate == null || !this.startDate
						.equals(other.startDate))) {
			return false;
		}
		if (this.endDate != other.endDate
				&& (this.endDate == null || !this.endDate.equals(other.endDate))) {
			return false;
		}
		if (this.actualEndDate != other.actualEndDate
				&& (this.actualEndDate == null || !this.actualEndDate
						.equals(other.actualEndDate))) {
			return false;
		}
		return true;
	}

     /**
     * Gets Family Partner Id
     * @return  --- fId
     */
    public int getfID() {
		return fID;
	}

     /**
     * Sets the Family Partner Id
     * @return  --- fId
     */
    public void setfID(int fid) {
		this.fID = fid;
	}
    
    /**
     * Gets the location
     * @return --- Location object
     */
    public Address getLocation() {
		return location;
	}

    /**
     * Gets the stage
     * @return --- String value of the stage
     */
    public String getStage() {
		return stage;
	}

    /**
     * Sets the stage
     * @param stage --- new stage value
     */
    public void setStage(String stage) {
		this.stage = stage;
	}

    /**
     * Gets the family partnership type
     * @return --- type of the the Family partnership
     */
    public FamilyPartnerType getType() {
		return type;
	}

    /**
     * Gets value of the property
     * @return --- value of the property
     */
    public int getValue() {
		return value;
	}

    /**
     * Gets cost of the property
     * @return --- Cost
     */
    public int getCost() {
		return cost;
	}

    /**
     * Gets if contents is required
     * @return --- true if consent is required else false
     */
    public boolean isConsentRequired() {
		return consentRequired;
	}

    /**
     * Gets date of consent
     * @return --- When date of consent received
     */
    public Date getDateOfConsent() {
		return dateOfConsent;
	}

    /**
     * Gets the start date of the project
     * @return --- Start date for the project
     */
    public Date getStartDate() {
		return startDate;
	}

    /**
     * Gets estimated end date
     * @return --- End date
     */
    public Date getEndDate() {
		return endDate;
	}

    /**
     * Gets the actual end date
     * @return --- Actual end date
     */
    public Date getActualEndDate() {
		return actualEndDate;
	}

    /**
     * Gets all the error messages You need to call {@link #isValid()} on the
     * object to populate any messages. Return value is never null
     * 
     * @return a list of error messages, will be empty if no error messages.
     */
    public List<String> getErrors() {
            return errorMessages;
    }

    /**
     * Validates the family partner object, and populates error messages. Need
     * to call Use {@link #getErrors()} to retrieve all error messages.
     * 
     * @return true if the move is valid, otherwise false
     */
    public boolean isValid() {
            errorMessages.clear(); // Empty all error messages

            if(stage.isEmpty() || "Please Select".equals(stage)) {
                errorMessages.add("Stage cannot be empty");
            }

            if(type == FamilyPartnerType.None){
                errorMessages.add("Family Partner Type needs to be selected.");
            }                    

            // Checking to see if the start date is provided
            if (startDate == null) {
                    errorMessages.add("Start date cannot be empty");
            } else {
                    if (startDate != null && endDate != null) {
                            if (startDate.after(endDate)) {
                                    errorMessages.add("Start date has to before the end date");
                            }
                    }
            }
            if (!location.isValid()) {
                    errorMessages.add("Address is invalid");
            } else {
                    // Additional validations handles locally
                    if (!h4hdb.util.Validators.isCorrectFieldLength(
                                    location.getAddr1(), Address.LengthOfAddr1)) {
                            errorMessages.add(String.format(
                                            "Address line 1 has to be less than {0} long",
                                            Address.LengthOfAddr1));
                    }
                    if (!h4hdb.util.Validators.isCorrectFieldLength(
                                    location.getAddr2(), Address.LengthOfAddr2)) {
                            errorMessages.add(String.format(
                                            "Address line 2 has to be less than {0} long",
                                            Address.LengthOfAddr2));
                    }
                    if (!h4hdb.util.Validators.isCorrectFieldLength(location.getCity(),
                                    Address.LengthOfCity)) {
                            errorMessages.add(String.format(
                                            "City has to be less than {0} long",
                                            Address.LengthOfCity));
                    }
                    if (!h4hdb.util.Validators.isCorrectFieldLength(
                                    location.getCountry(), Address.LengthOfCountry)) {
                            errorMessages.add(String.format(
                                            "Country has to be less than {0} long",
                                            Address.LengthOfCountry));
                    }
                    if (!h4hdb.util.Validators.isCorrectFieldLength(
                                    location.getPostCode(), Address.LengthOfPostCode)) {
                            errorMessages.add(String.format(
                                            "Postcode has to be less than {0} long",
                                            Address.LengthOfPostCode));
                    }
                    if (!h4hdb.util.Validators.isCorrectFieldLength(
                                    location.getRegion(), Address.LengthOfRegion)) {
                            errorMessages.add(String.format(
                                            "Postcode has to be less than {0} long",
                                            Address.LengthOfRegion));
                    }
                    if (!h4hdb.util.Validators.isCorrectFieldLength(
                                    location.getSuburb(), Address.LengthOfSuburb)) {
                            errorMessages.add(String.format(
                                            "Suburb has to be less than {0} long",
                                            Address.LengthOfSuburb));
                    }

            }               

            if(consentRequired && dateOfConsent == null){
                errorMessages.add("Date of consent is required");
            }                    
            // Field lengths
            if (!h4hdb.util.Validators.isCorrectFieldLength(stage, 15)) {
                errorMessages.add("Stage has to be less than 15 characters");
            }

            return errorMessages.isEmpty();
	}

	private void addValuesToSqlStatement(PreparedStatement preparedStatement)
			throws SQLException {
		preparedStatement.setString(1, location.getAddr1());
		preparedStatement.setString(2, location.getAddr2());
		preparedStatement.setString(3, location.getSuburb());
		preparedStatement.setString(4, location.getCity());
		preparedStatement.setString(5, location.getRegion());
		preparedStatement.setString(6, location.getCountry());
		preparedStatement.setString(7, location.getPostCode());
		preparedStatement.setString(8, getStage());
		preparedStatement.setInt(9, this.type.getValue());
		preparedStatement.setInt(10, value);
		preparedStatement.setInt(11, cost);
		preparedStatement.setBoolean(12, consentRequired);
		preparedStatement.setDate(13, dateOfConsent);
		preparedStatement.setDate(14, startDate);
		preparedStatement.setDate(15, endDate);
		preparedStatement.setDate(16, actualEndDate);
	}

	/**
	 * inserts a tuple into FamPartner relation
	 * 
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public boolean insertIntoDB(Connection con) throws SQLException {
		// require values validation before insert
		// roll back if insertion fails
		int rs = -1;
                
                
                
		PreparedStatement s = con
				.prepareStatement(
						"INSERT INTO fampartner"
								+ "(Address1,Address2,Suburb,City,Region,Country,PostCode,"
								+ "Stage,Type,Value,Cost,ConsentRequired,DateOfConsent,StartDate,"
								+ "EndDate,ActualEndDate)"
								+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
						Statement.RETURN_GENERATED_KEYS);
		addValuesToSqlStatement(s);
                ArrayList<Expression> conditions = new ArrayList<Expression>(); 
                Expression e; 
                e = new Expression(FIELD_ADDR1, Expression.MatchOperator.EQUALS, location.getAddr1()); 
                conditions.add(e); 
                e = new Expression(FIELD_ADDR2, Expression.MatchOperator.EQUALS, location.getAddr2()); 
                conditions.add(e); 
                e = new Expression(FIELD_SUBURB, Expression.MatchOperator.EQUALS, location.getSuburb()); 
                conditions.add(e); 
                e = new Expression(FIELD_CITY, Expression.MatchOperator.EQUALS, location.getCity()); 
                conditions.add(e); 
                e = new Expression(FIELD_REGION, Expression.MatchOperator.EQUALS, location.getRegion()); 
                conditions.add(e); 
                e = new Expression(FIELD_COUNTRY, Expression.MatchOperator.EQUALS, location.getCountry()); 
                conditions.add(e); 
                e = new Expression(FIELD_POSTCODE, Expression.MatchOperator.EQUALS, location.getPostCode()); 
                conditions.add(e); 
                List<FamilyPartner> l = DB.searchFamPartner(conditions, con);
                if(l.isEmpty())
                    rs = s.executeUpdate();
                else
                    return false; 
		ResultSet r = s.getGeneratedKeys();
		r.next();
		this.fID = r.getInt(1);
                if (fID > 0 ){
                    for(PartnerShip p : partnerships){
                        p.setfID(fID);
                        p.saveToDatabase(con);
                    }
                }
		r.close();
		s.close();
		return rs == 1;

	}

    /**
     * Deletes family partnership
     * @param con --- database connection
     * @throws SQLException
     */
    public void deleteFromDB(Connection con) throws SQLException {
		PreparedStatement s = con.prepareStatement(
				"DELETE FROM fampartner where fid = ?",
				Statement.RETURN_GENERATED_KEYS);
		s.setInt(1, this.fID);
		s.executeUpdate();
		s.close();
    }

    /**
     * update query should be constructed on the fly where the values for SET
     * clause and WHERE clause depends on the user specification. T
     * 
     * @param con
     * @return
     * @throws SQLException
     */
    public boolean updateIntoDB(Connection con) throws SQLException {

            PreparedStatement s = con.prepareStatement("UPDATE fampartner SET "
                            + "Address1 = ?," + "Address2 = ?," + "Suburb = ?,"
                            + "City = ?," + "Region = ?," + "Country = ?,"
                            + "PostCode = ?," + "Stage = ?," + "Type = ?," + "Value = ?,"
                            + "Cost = ?," + "ConsentRequired = ?," + "DateOfConsent = ?,"
                            + "StartDate = ?," + "EndDate = ?," + "ActualEndDate = ? "
                            + "WHERE FID = ?");
            addValuesToSqlStatement(s);
            s.setInt(17, fID);
            s.executeUpdate();

            s.close();

            for(PartnerShip p : partnerships){
                p.saveToDatabase(con);
            }
            
            
            return true;

    }

    public void addPartnerShip(PartnerShip partnerShip) {
        if(this.partnerships == null)
            this.partnerships = new ArrayList<PartnerShip>();
        
        this.partnerships.add(partnerShip);
    }
    
    public void addPartnerShip(List<PartnerShip> partnerShip) {
        this.partnerships = partnerShip;
    }
    
    public List<PartnerShip> getPartnerShip(){
        return partnerships;
    }

    @Override
    public String getDataInXml() {
        
        //XMLOutputFactory xfactory = new XMLOutputFactory(new javax.xml.stream.XMLStreamWriter());
            Document xmlDoc = new DocumentImpl();
            Element root = xmlDoc.createElement("familypartnerhip");
            root.appendChild(
                xmlDoc.createElement("familypartnerhipname").appendChild(
                    xmlDoc.createTextNode(this.location.getAddr1())
                )
            );
            root.appendChild(
                xmlDoc.createElement("address").appendChild(
                    xmlDoc.createTextNode(this.location.toString())
                )
            );
            root.appendChild(
                xmlDoc.createElement("type").appendChild(
                    xmlDoc.createTextNode(this.type.toString())
                )
            );            
            root.appendChild(
                xmlDoc.createElement("startdate").appendChild(
                    xmlDoc.createTextNode(this.startDate.toString())
                )
            );
            root.appendChild(
                xmlDoc.createElement("startdate").appendChild(
                    xmlDoc.createTextNode(this.startDate.toString())
                )
            );            
            root.appendChild(
                xmlDoc.createElement("currentstage").appendChild(
                    xmlDoc.createTextNode(this.stage)
                )
            );                        
            
            Element helpers = xmlDoc.createElement("helped");
            Element living = xmlDoc.createElement("living");
            Element people = xmlDoc.createElement("people");
            for (Iterator<PartnerShip> it = partnerships.iterator(); it.hasNext();) {
                PartnerShip item = it.next();
                Element p = xmlDoc.createElement("person");
                p.appendChild(
                    xmlDoc.createElement("fullname").appendChild(
                        xmlDoc.createTextNode(item.getPerson().getfName())
                    )
                );  
                p.appendChild(
                    xmlDoc.createElement("address").appendChild(
                        xmlDoc.createTextNode(item.getPerson().getAddress().toString())
                    )
                );                          
                if(item.isWorking())
                {
                    helpers.appendChild(p);
                }
                if(item.isLiving())
                {
                    living.appendChild(p);
                }
            }                        
            root.appendChild(people);                                    
            //TODO:Need to include comments here
            /*
            for (Iterator<Comment> it = com.iterator(); it.hasNext();) {
                
            }
            */

            return root.toString();
    }

    @Override
    public String getXSLTFilename() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getOuptputFileName() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
