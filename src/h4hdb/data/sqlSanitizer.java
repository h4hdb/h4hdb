/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package h4hdb.data;

/**
 *
 * @author lelitu
 */
public class sqlSanitizer {
    
    public static String sanitizeString(String input){
        if (input == null || input.equals("")){
           return "NULL";
        }else {
            input = input.replace("'", "\\'");
            input = input.replace(";", "\\;");
            input = "'" + input + "'";
            return input;
        }
    }
}
