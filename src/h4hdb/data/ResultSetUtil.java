package h4hdb.data;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ResultSetUtil {
	/**
	 * Determines the number of rows in a ResultSet. Upon exit, if the cursor
	 * was not currently on a row, it is just before the first row in the result
	 * set (a call to {ResultSet.next()} will go to the first row).
	 * 
	 * @param set
	 *            The ResultSet to check (must be scrollable).
	 * @return The number of rows.
	 * @throws SQLException
	 *             If the ResultSet is not scrollable.
	 */
	public static int getRowCount(ResultSet set) throws SQLException {
		int rowCount;
		int currentRow = set.getRow(); // Get current row
		rowCount = set.last() ? set.getRow() : 0; // Determine number of rows
		if (currentRow == 0) // If there was no current row
			set.beforeFirst(); // We want next() to go to first row
		else
			// If there WAS a current row
			set.absolute(currentRow); // Restore it
		return rowCount;
	}

}
