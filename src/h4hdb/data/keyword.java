/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package h4hdb.data;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author lelitu
 */
public class keyword {
    private int keyID;
    private final String name;
    private final String description;

    public keyword(String name, String description) {
        this.name = name;
        this.description = description;
    }
    
    public keyword(int keyID, String name, String description) {
        this.keyID = keyID;
        this.name = name;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }
    
    public int getID(){
        return keyID;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final keyword other = (keyword) obj;
        if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
            return false;
        }
        if ((this.description == null) ? (other.description != null) : !this.description.equals(other.description)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + (this.name != null ? this.name.hashCode() : 0);
        hash = 83 * hash + (this.description != null ? this.description.hashCode() : 0);
        return hash;
    }

    public boolean insertIntoDB(Connection con) throws SQLException {
        if (con == null || con.isClosed() || this.keyID != 0) {
            return false;
        }
        Statement s = con.createStatement();
        String query = "INSERT INTO keyword VALUES(DEFAULT, "+sqlSanitizer.sanitizeString(name) 
                +", "+sqlSanitizer.sanitizeString(description) +");";
        int result = s.executeUpdate(query);
        if (result == 1){
            ResultSet rs = s.executeQuery("SELECT LAST_INSERT_ID()");
            if (rs.next()){
                this.keyID = rs.getInt(1);
                rs.close();
                s.close();
                return true;
            }
            rs.close();
        }
        s.close();
        return false;
    }

    boolean isValid() {
        if (this.name == null || this.name.equals("")){
            return false;
        }
        if (this.description == null || this.description.equals("")){
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return name;
    }
    
    
}
