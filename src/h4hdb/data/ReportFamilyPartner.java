/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package h4hdb.data;

import h4hdb.reporting.IReportItem;
import java.sql.Connection;

/**
 *
 * @author Mehendra
 */
public class ReportFamilyPartner implements IReportItem {

    @Override
    public String getDataInXml() {
            return "<?xml version=\"1.0\"?>"
            + "<familypartnerhip>"
            + "<familypartnerhipname>56, Unknown Road, Mars village, Luna, 2004</propertyname>"
            + "<address>56, Somerandom Street, Elderson, Porrirua</address>"
            + "<type>Build</type>"
            + "<startdate>20/Oct/2011</startdate>"
            + "<enddate>20/Oct/2011</enddate>"
            + "<currentstage>Building</currentstage>"
            + "<stage>Build</stage>"                    
            + "<people>"
                + "<helped>"
                        + "<fullname>John Doe</fullname>"
                        + "<address>98, Clower Street, Tepuni, Lower hutt.</address>"
                        + "<phone>08-456 3456</phone>"
                        + "<email>clower.street@gmail.lom</email>"                    
                + "</helped>"                    
                + "<living>"
                        + "<fullname>John Doe</fullname>"
                        + "<address>98, Clower Street, Tepuni, Lower hutt.</address>"
                        + "<phone>08-456 3456</phone>"
                        + "<email>clower.street@gmail.lom</email>"                    
                + "</living>"
            + "</people>"
            + "<comments>"
                + "<line>Any comments we would have stored on the database</line>"
            + "</comments>"                    
            + "</familypartnerhip>";
    }

    @Override
    public String getXSLTFilename() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getOuptputFileName() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
