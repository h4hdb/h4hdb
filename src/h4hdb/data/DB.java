package h4hdb.data;

import com.truemesh.squiggle.*;
import com.truemesh.squiggle.criteria.MatchCriteria;
import h4hdb.data.Expression.MatchOperator;
import h4hdb.mainFrame;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DB {


    /**
     * Look for people who satisfies the search criteria in the person table of
     * the database. If no one matches the condition then return value will be
     * null.
     *
     * @param where_condition
     * @return List<Person>
     * @throws SQLException
     */
    public static List<Person> searchPerson(List<Expression> where_condition, Connection con)
            throws SQLException {
        Person person;
        List<Person> people = new ArrayList<Person>();
        ResultSet rs = createAndExcuteQuery("person", where_condition, con);
        while (rs.next()) {
            person = new Person(rs);
            people.add(person);
            System.out.println(person.getTitle() + "\t"
                    + person.getfName() + "\t" + person.getlName()
                    + "\t" + person.getContacts().getEmail()); // debug

        }
        return people;

    }

    /**
     * Search the famPartner table of the database according to the where
     * condition
     *
     * @param where_condition
     * @return
     * @throws SQLException
     */
    public static List<FamilyPartner> searchFamPartner(List<Expression> where_condition, Connection con)
            throws SQLException {
        FamilyPartner fp;
        List<FamilyPartner> famPartners = new ArrayList<FamilyPartner>();
        
        ResultSet rs = createAndExcuteQuery("fampartner", where_condition, con);
        while (rs.next()) {
            fp = new FamilyPartner(rs);
            famPartners.add(fp);
        }
        return famPartners;
    }

    /**
     * Search through the partnership table via fid and return a partnership
     * object with a list of people
     *
     * @param where_condition
     * @return
     * @throws SQLException
     */
    public static List<PartnerShip> searchPartnerShip(List<Expression> where_condition, Connection con) throws SQLException {
        int fid;
        boolean living, working;
        Person person;
        List<PartnerShip> pss = new ArrayList<PartnerShip>();

        ResultSet rs = createAndExcuteQuery("partnership", where_condition, con);
        while (rs.next()) {
            fid = rs.getInt("fid");
            person = new Person(rs);
            living = rs.getBoolean("living");
            working = rs.getBoolean("working");
            System.out.println("Fid:" + fid + " Pid:" + person.getPID() + " living:" + living + " working:" + working + "\n");
            pss.add(new PartnerShip(fid, person, living, working));
        }
        return pss;
    }

    /**
     * Generates a dynamic select query for a single table, might change later
     * to allow multiple tables.
     *
     * @param tableName
     * @param columnNames
     * @param where_condition
     * @return
     */
    private static ResultSet createAndExcuteQuery(String tableName,
            List<Expression> where_condition, Connection con) throws SQLException {
        String[] t1;
        String[] t2;
        Table other;
        Expression expr;
        PreparedStatement stmt;
        List<Expression> temp = new ArrayList<Expression>();
        Table table = new Table(tableName);
        SelectQuery select = new SelectQuery();
        select.addToSelection(table.getWildcard());
        if (where_condition != null) {
            for(int i = 0; i <where_condition.size(); i++){
                expr = where_condition.get(i);
                temp.add(expr);
                if (expr.matchType().equals("JOIN")) {
                    t1 = expr.columnName().split("\\.");
                    t2 = ((String) expr.value()).split("\\.");
                    other = new Table(t2[0]);
                    select.addToSelection(other.getWildcard());
                    select.addJoin(table, t1[1], other, t2[1]);
                    temp.remove(expr);
                } else {
                    Matchable left = new Column(table, expr.columnName());
                    Matchable right = new Parameter();
                    select.addCriteria(new MatchCriteria(left, expr.matchType(), right));
                }
            }
            stmt = con.prepareStatement(select.toString());
            for (int i = 0; i < temp.size(); i++) {
                stmt.setObject(i + 1,temp.get(i).value());
            }
        } else {
            stmt = con.prepareStatement(select.toString());
        }
        System.out.println(stmt.toString());
        return stmt.executeQuery();

    }

    /**
     * For testing purpose on local host, could remove later
     *
     * @return
     */
    public static Connection getconnection() {
        Connection con = null;
        try {

            String driverName = "org.gjt.mm.mysql.Driver";
            Class.forName(driverName);

            String serverName = "h4hdbdevelopment.db.9809852.hostedresource.com";
            String mydatabase = "h4hdbdevelopment";
            String url = "jdbc:mysql://" + serverName + "/" + mydatabase;

            con = DriverManager.getConnection(url, "h4hdbdevelopment", "On1T0ug5Pass@");
        } catch (SQLException ex) {
            int error = ex.getErrorCode();
            if (error == 1044 || error == 1045) {
            } else {
                Logger.getLogger(mainFrame.class.getName()).log(Level.SEVERE,
                        null, ex);
                System.exit(1);
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(mainFrame.class.getName()).log(Level.SEVERE, null,
                    ex);
            System.exit(1);
        }
        return con;
    }

    public static void main(String[] args) {
        try {
            List<Expression> where_condition = new ArrayList<Expression>();
            where_condition.add(new Expression("fid", MatchOperator.EQUALS,
                    1));
            Connection c = getconnection();
            DB.searchPartnerShip(where_condition, c);
            c.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
}
