/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package h4hdb.data;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author lelitu
 */
public class PersonKeyword {
    private int id;
    private keyword word;
    private int person;
    private int level;
    private String constraints;

    public PersonKeyword(int id, keyword word, int person, int level, String constraints) {
        this.id = id;
        this.word = word;
        this.person = person;
        this.level = level;
        this.constraints = constraints;
    }

    public PersonKeyword(keyword word, int person, int level, String constraints) {
        this.word = word;
        this.person = person;
        this.level = level;
        this.constraints = constraints;
    }
    
    public PersonKeyword(){
    }

    public String getConstraints() {
        return constraints;
    }

    public int getLevel() {
        return level;
    }

    public int getPerson() {
        return person;
    }

    public keyword getWord() {
        return word;
    }

    public int getId() {
        return id;
    }

    public void setConstraints(String constraints) {
        this.constraints = constraints;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void setPerson(int person) {
        this.person = person;
    }

    public void setWord(keyword word) {
        this.word = word;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PersonKeyword other = (PersonKeyword) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.word != other.word && (this.word == null || !this.word.equals(other.word))) {
            return false;
        }
        if (this.person != other.person) {
            return false;
        }
        if (this.level != other.level) {
            return false;
        }
        if ((this.constraints == null) ? (other.constraints != null) : !this.constraints.equals(other.constraints)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + this.id;
        hash = 37 * hash + (this.word != null ? this.word.hashCode() : 0);
        hash = 37 * hash + this.person;
        hash = 37 * hash + this.level;
        hash = 37 * hash + (this.constraints != null ? this.constraints.hashCode() : 0);
        return hash;
    }
    
    public boolean insertIntoDB(Connection con) throws SQLException{
        if (con == null || con.isClosed() || con.isReadOnly() || this.id != 0) {
            return false; //either it's a dud connection, or already in there, either way, don't insert.
        }
        if (this.word.getID() == 0 ){//there's no valid id, it's not in the DB
            if(!this.word.insertIntoDB(con)){//so insert it.
                return false; //insert failed, so stop here.
            } 
        }
        Statement s = con.createStatement();
        String query = "INSERT INTO personkeyword VALUES(DEFAULT, "+this.person
        + ", "+this.word.getID()+", "+this.level+", "+sqlSanitizer.sanitizeString(this.constraints) +");";
        int result = s.executeUpdate(query);
        if (result == 1) {
            ResultSet rs = s.executeQuery("SELECT LAST_INSERT_ID();");
            if (rs.next()){
                this.id = rs.getInt(1);
                rs.close();
                s.close();
                return true;
            }
            rs.close();
        }
        s.close();
        return false;
    }
    
    public boolean updateIntoDB(Connection con) throws SQLException{
        if (con == null || con.isClosed() || con.isReadOnly() || this.id == 0){
            return false; //bad connection or not already in the database, either way, don't update.
        }
        if (this.word.getID() == 0) {//new keyword, insert it first
            if (!this.word.insertIntoDB(con)){
                return false;//the keyword can't be inserted. fail here.
            }
        }
        String query = "UPDATE personkeyword SET level="+this.level
                +", constraints="+sqlSanitizer.sanitizeString(this.constraints) 
                +" WHERE ID="+this.id;
        Statement s = con.createStatement();
        int result = s.executeUpdate(query);
        if (result == 1){
            s.close();
            return true;
        }
        return false;
    }
    
    /**
     * 
     * @return true iff there is a valid keyword and level 
     */
    public boolean isValid(){
        if (this.word == null || !this.word.isValid()){
            return false;
        }
        if (this.level < -1 || this.level > 3){
            return false;
        }
        return true;
    }
}
