package h4hdb.data;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**Contains contact person information
 * used by organisations. 
 *
 * @author Leliel Trethowen
 */
public class Contact {

    private int CID;
    private int OID;
    private Person person;
    private String title;
    private Contacts contacts;

    /**Constructor for comments, used to build a comment from database
     * 
     * @param CID --- int Comment id, primary key
     * @param OID --- int organisation id
     * @param person --- Person contact Person
     * @param title --- String Jobtitle
     * @param contacts --- Contacts contact details for contact person.
     */
    public Contact(int CID, int OID, Person person, String title, Contacts contacts) {
        this.CID = CID;
        this.OID = OID;
        this.person = person;
        this.title = title;
        this.contacts = contacts;
    }  

//    public Contact(Person person, String title, Contacts contacts) {
//        this.person = person;
//        this.title = title;
//        this.contacts = contacts;
//    }

    /**Creates an empty contacts, with no fields set.
     * 
     */
    public Contact() {
        this.contacts = new Contacts();
    }

    public int getPID() {
        return this.person.getPID();
    }

    public void setPID(int PID) {
        this.person.setPID(PID);
    }

    public Contacts getContacts() {
        return contacts;
    }

    public void setContacts(Contacts contacts) {
        this.contacts = contacts;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getCID() {
        return CID;
    }

    public void setCID(int CID) {
        this.CID = CID;
    }

    public int getOID() {
        return OID;
    }

    public void setOID(int OID) {
        this.OID = OID;
    }

    public Person getPerson() {
        return this.person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Contact other = (Contact) obj;
        if (this.CID != other.CID) {
            return false;
        }
        if (this.person.getPID() != other.person.getPID()) {
            return false;
        }
        if (this.OID != other.OID) {
            return false;
        }
        if ((this.title == null) ? (other.title != null) : !this.title.equals(other.title)) {
            return false;
        }
        if (this.contacts != other.contacts && (this.contacts == null || !this.contacts.equals(other.contacts))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 43 * hash + this.CID;
        hash = 43 * hash + this.person.getPID();
        hash = 43 * hash + this.OID;
        hash = 43 * hash + (this.title != null ? this.title.hashCode() : 0);
        hash = 43 * hash + (this.contacts != null ? this.contacts.hashCode() : 0);
        return hash;
    }

    /**Checks validity of the contact
     * returns true iff there is a person, with a title
     * and valid contact details.
     * 
     * @return --- True iff the contact person is valid
     */
    public boolean isValid() {
        if (this.person.getPID() == 0) {
            return false;
        }
        if (this.title == null || this.title.equals("")) {
            return false;
        }
        if (!this.contacts.isValid()) {
            return false;
        }
        return true;
    }

    /**Checks if the contact has any fields set.
     * 
     * @return --- True iff the contact has no fields set
     */
    public boolean isEmpty() {
        if (this.person.getPID() != 0) {
            return false;
        }
        if (this.title != null || !this.title.equals("")) {
            return false;
        }
        if (!this.contacts.isEmpty()) {
            return false;
        }
        return true;
    }

    /**Writes a new comment into the database
     * will not write an existing comment.
     * 
     * @param con --- Database connection to write to
     * @param OID --- organisation ID the contact belongs to
     * @return --- True iff the contact is successfully written
     * @throws SQLException
     */
    public boolean insertIntoDB(Connection con, int OID) throws SQLException {
        if (this.OID != 0) {
            return false;
        } else {
            this.OID = OID;
            Statement s = con.createStatement();
            int result = s.executeUpdate("INSERT INTO contacts VALUES(DEFAULT, " + this.person.getPID()
                    + "," + this.OID + ", " + sqlSanitizer.sanitizeString(title) + "," + sqlSanitizer.sanitizeString(contacts.getEmail())
                    + "," + sqlSanitizer.sanitizeString(contacts.gethPhone()) + "," + sqlSanitizer.sanitizeString(contacts.getwPhone())
                    + "," + sqlSanitizer.sanitizeString(contacts.getcPhone()) + ");");
            if (result == 1) {
                ResultSet rs = s.executeQuery("SELECT LAST_INSERT_ID();");
                rs.next();
                this.CID = rs.getInt(1);
                return true;
            } else {
                s.close();
                return false;
            }
        }
    }

    /**Updates a contact in the database
     * Refuses if no CID exists.
     * 
     * @param con --- Database connection to write to
     * @return --- True iff the update is successful and affects rows.
     * @throws SQLException 
     */
    public boolean updateIntoDB(Connection con) throws SQLException {
        if (this.CID == 0) {
            return false;
        } else {
            Statement s = con.createStatement();
            int result = s.executeUpdate("UPDATE contacts SET PID=" + this.person.getPID() + ", "
                    + "OID=" + this.OID + ", title=" + sqlSanitizer.sanitizeString(this.title) + ","
                    + "email=" + sqlSanitizer.sanitizeString(contacts.getEmail()) + ", Landline=" + sqlSanitizer.sanitizeString(contacts.gethPhone())
                    + ", WorkPhone=" + sqlSanitizer.sanitizeString(contacts.getwPhone()) + ", Mobile=" + sqlSanitizer.sanitizeString(contacts.getcPhone())
                    + " WHERE CID=" + this.CID + ";");
            return result == 1;
        }

    }
}
