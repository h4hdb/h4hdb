/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package h4hdb.data;

/**
 *Enum for possible forms of correspondence.
 * email, hardcopy or both.
 * @author Leliel Trethowen
 */
public enum commType {
    None, Electronic, Hard, Both;
}
