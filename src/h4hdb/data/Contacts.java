package h4hdb.data;

import h4hdb.data.sqlSanitizer;

/**Represents a set of contact details
 * email, 2 landlines and mobile.
 *
 * @author Leliel Trethowen
 */
public class Contacts {

    private String hPhone;
    private String wPhone;
    private String cPhone;
    private String email;

    /*Create an empty set of contact details
     * 
     */
    public Contacts() {
        super();
    }

    /**Create contacts from values.
     * used to build contacts from Database.
     * 
     * @param hPhone --- String first landline
     * @param wPhone --- String second landline
     * @param cPhone --- String mobile
     * @param email --- String Email
     */
    public Contacts(String hPhone, String wPhone, String cPhone, String email) {
        this.hPhone = hPhone;
        this.wPhone = wPhone;
        this.cPhone = cPhone;
        this.email = email;
    }

    public String getcPhone() {
        return cPhone;
    }

    public void setcPhone(String cPhone) {
        this.cPhone = cPhone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String gethPhone() {
        return hPhone;
    }

    public void sethPhone(String hPhone) {
        this.hPhone = hPhone;
    }

    public String getwPhone() {
        return wPhone;
    }

    public void setwPhone(String wPhone) {
        this.wPhone = wPhone;
    }

    /**Checks if this contacts has at least one of landlines, email, mobile set
     * 
     * @return --- True iff at least one value is set 
     */
    public boolean isValid() {
        if (hPhone != null && !hPhone.equals("NULL")) {
            return true;
        }
        if (wPhone != null && !wPhone.equals("NULL")) {
            return true;
        }
        if (cPhone != null && !cPhone.equals("NULL")) {
            return true;
        }
        if (email != null && !email.equals("NULL")) {
            return true;
        }
        return false;
    }

    /**Checks if this contacts has none of landlines, email, mobile set
     * 
     * @return --- True iff no value is set 
     */
    public boolean isEmpty() {
        if (hPhone != null && !hPhone.equals("")) {
            return false;
        }
        if (wPhone != null && !wPhone.equals("")) {
            return false;
        }
        if (cPhone != null && !cPhone.equals("")) {
            return false;
        }
        if (email != null && !email.equals("")) {
            return false;
        }
        return true;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Contacts other = (Contacts) obj;
        if ((this.hPhone == null) ? (other.hPhone != null) : !this.hPhone.equals(other.hPhone)) {
            return false;
        }
        if ((this.wPhone == null) ? (other.wPhone != null) : !this.wPhone.equals(other.wPhone)) {
            return false;
        }
        if ((this.cPhone == null) ? (other.cPhone != null) : !this.cPhone.equals(other.cPhone)) {
            return false;
        }
        if ((this.email == null) ? (other.email != null) : !this.email.equals(other.email)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + (this.hPhone != null ? this.hPhone.hashCode() : 0);
        hash = 37 * hash + (this.wPhone != null ? this.wPhone.hashCode() : 0);
        hash = 37 * hash + (this.cPhone != null ? this.cPhone.hashCode() : 0);
        hash = 37 * hash + (this.email != null ? this.email.hashCode() : 0);
        return hash;
    }
}
