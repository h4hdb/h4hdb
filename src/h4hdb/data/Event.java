/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package h4hdb.data;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 *Represents a event as modeled by the database
 * 
 * @author daniel
 */
public class Event {

    private int EID;
    private String name;
    private Date doe;
    private Address address;
    private List<Comment> comments;

    /**
     * Constructs an empty event.
     */
    public Event() {
        this.address = new Address();
        this.comments = new ArrayList<Comment>();
    }

    /**
     * Constructs a event with all fields set to provided values.
     * 
     * @param EID --- int Event ID, primary key
     * @param name
     * @param addr1
     * @param addr2
     * @param suburb
     * @param city
     * @param region
     * @param country
     * @param postCode
     * @param doe
     * @param comments 
     */
    public Event(int EID, String name, String addr1, String addr2, String suburb,
            String city, String region, String country, String postCode, Date doe,
            List<Comment> comments) {
        this.EID = EID;
        this.name = name;
        this.address = new Address(addr1, addr2, suburb, city, region, country, postCode);
        this.doe = doe;
        this.comments = comments;
    }

    /**
     * Constructs a event with all fields set to provided values.
     * 
     * @param EID --- int Event ID, primary key
     * @param name
     * @param address
     * @param doe
     * @param comments 
     */
    public Event(int EID, String name, Address address, Date doe, List<Comment> comments) {
        this.EID = EID;
        this.name = name;
        this.address = address;
        this.doe = doe;
        this.comments = comments;
    }

    /**
     * Constructs a event with all fields set to provided values.
     * 
     * @param EID --- int Event ID, primary key
     * @param name
     * @param addr1
     * @param addr2
     * @param suburb
     * @param city
     * @param region
     * @param country
     * @param postCode
     * @param doe
     * @param comments 
     */
    public Event(int EID, String name, String addr1, String addr2, String suburb,
            String city, String region, String country, String postCode, Date doe) {
        this.EID = EID;
        this.name = name;
        this.address = new Address(addr1, addr2, suburb, city, region, country, postCode);
        this.doe = doe;
        this.comments = new ArrayList<Comment>();
    }

    /**
     * Constructs a event with all fields set to provided values.
     * 
     * @param EID --- int Event ID, primary key
     * @param name
     * @param address
     * @param doe
     * @param comments 
     */
    public Event(int EID, String name, Address address, Date doe) {
        this.EID = EID;
        this.name = name;
        this.address = address;
        this.doe = doe;
        this.comments = new ArrayList<Comment>();
    }

    /**
     * Constucts a event with the minimum information required.
     *
     * @param EID --- int eventID
     * @param name --- String a name
     */
    public Event(int EID, String name) {
        this.EID = EID;
        this.name = name;
        this.comments = new ArrayList<Comment>();
    }

    /**
     * Constucts a event with result set return from a database
     *
     * @param rs --- reseult set return from a database
     */
    public Event(ResultSet rs) throws SQLException {
		this.EID = rs.getInt("EID");
		this.name = rs.getString("Name"); 
		this.doe = rs.getDate("EventDate");
		this.address = new Address(rs.getString("Address1"),
				rs.getString("Address2"), rs.getString("Suburb"),
				rs.getString("City"), rs.getString("Region"),
				rs.getString("Country"), rs.getString("PostCode"));
	}

    public int getEID() {
        return EID;
    }

    public void setEID(int EID) {
        this.EID = EID;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Date getDoe() {
        return doe;
    }

    public void setDoe(Date doe) {
        this.doe = doe;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Comment> getComments() {
        return this.comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public void addComment(Comment c) {
        this.comments.add(c);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Event other = (Event) obj;
        if (this.EID != other.EID) {
            return false;
        }
        if (this.name != other.name) {
        	return false; 
        }
        if (!this.address.equals(other.address)) {
        	return false; 
        }
        if (this.doe != other.doe) {
        	return false; 
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + this.EID;
        return hash;
    }

    @Override
    public String toString() {
        return name;
    }
    
    /**
     * Preparing an sql statement
     * 
     * @param preparedStatement
     * @throws SQLException
     */
	private void addValuesToSqlStatement(PreparedStatement preparedStatement)
			throws SQLException {
		preparedStatement.setString(1, this.name); 
		preparedStatement.setString(2, address.getAddr1());
		preparedStatement.setString(3, address.getAddr2());
		preparedStatement.setString(4, address.getSuburb());
		preparedStatement.setString(5, address.getCity());
		preparedStatement.setString(6, address.getRegion());
		preparedStatement.setString(7, address.getCountry());
		preparedStatement.setString(8, address.getPostCode());
		preparedStatement.setDate(9, this.doe);
	}

    /**
     * Writes a new event into the database will not attempt to insert an
     * existing event.
     *
     * @param con --- Database connection to write to
     * @return --- True if write succeeded
     * @throws SQLException
     */
    public boolean insertIntoDB(Connection con) throws SQLException {
        if (this.EID != 0) {
            return false;
        }
        
        int rs = -1;
		PreparedStatement s = con
				.prepareStatement(
						"INSERT INTO Event"
								+ "(Name,Address1,Address2,Suburb,City,Region,Country,PostCode,"
								+ "EventDate)"
								+ "VALUES(?,?,?,?,?,?,?,?,?)",
						Statement.RETURN_GENERATED_KEYS);
		addValuesToSqlStatement(s);
		rs = s.executeUpdate();
		ResultSet r = s.getGeneratedKeys();
		r.next();
		this.EID = r.getInt("EID");
		r.close();
		s.close();
		return rs == 1;
		// TODO Comments
        /*for (Comment c : this.comments) {
            s.executeUpdate("INSERT INTO Comments VALUES (DEFAULT, " + EID + ", NULL,"
                    + " NULL, " + c.getDate() + ", NULL," + sqlSanitizer.sanitizeString(c.getText()) + ");");
            r = s.executeQuery("SELECT LAST_INSERT_ID();");
            r.next();
            c.setCID(r.getInt(1));
            r.close();
        }
        s.close();
        return rs == 1;*/
    }

    /**
     * Writes updates to event into the database will not attempt to insert a new event.
     *
     * @param con --- Database connection to write to
     * @return --- True if write succeeded
     * @throws SQLException
     */
    public boolean updateIntoDB(Connection con) throws SQLException {
        if (this.EID == 0) {
            return false;
        }
        
        PreparedStatement s = con.prepareStatement("UPDATE Event SET Name = ?,"
				+ "Address1 = ?," + "Address2 = ?," + "Suburb = ?,"
				+ "City = ?," + "Region = ?," + "Country = ?,"
				+ "PostCode = ?," + "EndDate = ?" + "WHERE EID = ?");
		addValuesToSqlStatement(s);
		s.setInt(10, this.EID);
		s.executeUpdate();
		s.close();
		return true;
		//TODO Comments
        /*for (Comment c : this.comments) {
            if (c.getCID() == 0) {
                s.executeUpdate("INSERT INTO Comments VALUES (DEFAULT, " + EID + ", NULL,"
                        + " NULL, " + c.getDate() + ", NULL," + sqlSanitizer.sanitizeString(c.getText()) + ");");
                r = s.executeQuery("SELECT LAST_INSERT_ID();");
                r.next();
                c.setCID(r.getInt(1));
                r.close();
            }
        }
        return rs == 1;*/
    }

    /**
     * Delete this event from the database. 
     * 
     * @param con
     * @throws SQLException
     */
    public void deleteFromDB(Connection con) throws SQLException{
    	PreparedStatement s;
    	s = con.prepareStatement(
    			"DELETE FROM fampartner where fid = ?",
    			Statement.RETURN_GENERATED_KEYS); 
    	s.setInt(1, this.EID);
    	s.executeUpdate();
    	s.close();
    }

    /**
     * Used to generate an SQL statement to select from Event all event which
     * match the fields set in this event Must not be called on an empty
     * Event.
     *
     * @return String --- SQL Query to select event based on set fields.
     */
    public String createSelectQuery() {
        StringBuilder query = new StringBuilder("SELECT * FROM Event WHERE "); //header is constant, now build the where clause.
        if (this.name != null && !this.name.equals("")) {
            query.append("Name = ");
            query.append(sqlSanitizer.sanitizeString(name));
            query.append(" AND ");
        }
        if (this.address != null) {
            if (this.address.getAddr1() != null && !this.address.getAddr1().equals("NULL")) {
                query.append("Address1 = ");
                query.append(this.address.getAddr1());
                query.append(" AND ");
            }
            if (this.address.getAddr2() != null && !this.address.getAddr2().equals("NULL")) {
                query.append("Address2 = ");
                query.append(this.address.getAddr2());
                query.append(" AND ");
            }
            if (this.address.getSuburb() != null && !this.address.getSuburb().equals("NULL")) {
                query.append("Suburb = ");
                query.append(this.address.getSuburb());
                query.append(" AND ");
            }
            if (this.address.getCity() != null && !this.address.getCity().equals("NULL")) {
                query.append("City = ");
                query.append(this.address.getCity());
                query.append(" AND ");
            }
            if (this.address.getRegion() != null && !this.address.getRegion().equals("NULL")) {
                query.append("Region = ");
                query.append(this.address.getRegion());
                query.append(" AND ");
            }
            if (this.address.getCountry() != null && !this.address.getCountry().equals("NULL")) {
                query.append("Country = ");
                query.append(this.address.getCountry());
                query.append(" AND ");
            }
            if (this.address.getPostCode() != null && !this.address.getPostCode().equals("NULL")) {
                query.append("PostCode = ");
                query.append(this.address.getPostCode());
                query.append(" AND ");
            }
        }
        if (this.doe != null) {
            query.append("EventDate = ");
            query.append(this.doe.toString());
            query.append(" AND ");
        }

        query.delete(query.length() - 5, query.length());
        return query.toString().trim();
    }
}
