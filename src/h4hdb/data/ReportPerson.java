/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package h4hdb.data;

import h4hdb.reporting.IReportItem;

/**
 *
 * @author Mehendra
 */
public class ReportPerson implements IReportItem{

    
    public ReportPerson(String randomParameter)
    {
    }
            
            
    @Override
    public String getDataInXml() {
        return "<?xml version=\"1.0\"?>"
                + "<property>"
                + "<propertyname>56, Unknown Road, Mars village, Luna, 2004</propertyname>"                
                + "<people>"
                + "<person><firstname>John</firstname><lastname>Doe</lastname></person>"
                + "<person><firstname>Chuck</firstname><lastname>Norris</lastname></person>"
                + "<person><firstname>John</firstname><lastname>Carter</lastname></person>"
                + "<person><firstname>Bruce</firstname><lastname>Banner</lastname></person>"
                + "<person><firstname>Nick</firstname><lastname>Fury</lastname></person>"
                + "</people>"
                + "</property>";
    }

    @Override
    public String getXSLTFilename() {
        return "PersonReport.xsl";
    }

    @Override
    public String getOuptputFileName() {
        return "PersonReport";
    }
    
}
