package h4hdb.data;

import com.truemesh.squiggle.SelectQuery;
import com.truemesh.squiggle.Table;
import com.truemesh.squiggle.criteria.MatchCriteria;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * represents an organisation as seen by the database
 *
 * @author Leliel Trethowen
 */
public class Org {

    private int OID;
    private String name;
    private Address addr;
    private List<Contact> contacts;
    private List<Comment> comments;
    private List<Contact> removedContacts = new ArrayList<Contact>();

    /**
     * Constructor setting all values used when building an org from database
     *
     * @param OID --- int Organisation id, primary key
     * @param Name --- String Name of the organisation
     * @param addr --- Address of the organisation
     * @param contacts --- List of contacts for the organisation
     * @param comments --- List of comments associated with the organisation
     */
    public Org(int OID, String Name, Address addr, List<Contact> contacts, List<Comment> comments) {
        this.OID = OID;
        this.name = Name;
        this.addr = addr;
        this.contacts = contacts;
        this.comments = comments;
    }

    /**
     * Creates an empty organisation.
     *
     */
    public Org() {
        this.contacts = new ArrayList<Contact>();
        this.comments = new ArrayList<Comment>();
    }

    public int getOID() {
        return OID;
    }

    public void setOID(int OID) {
        this.OID = OID;
    }

    public String getName() {
        return name;
    }

    public void setName(String Name) {
        this.name = Name;
    }

    public Address getAddr() {
        return addr;
    }

    public void setAddr(Address addr) {
        this.addr = addr;
    }

    public List<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public void addComment(Comment c) {
        this.comments.add(c);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Org other = (Org) obj;
        if (this.OID != other.OID) {
            return false;
        }
        if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
            return false;
        }
        if (this.addr != other.addr && (this.addr == null || !this.addr.equals(other.addr))) {
            return false;
        }
        if (this.contacts != other.contacts && (this.contacts == null || !this.contacts.equals(other.contacts))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + this.OID;
        hash = 37 * hash + (this.name != null ? this.name.hashCode() : 0);
        hash = 37 * hash + (this.addr != null ? this.addr.hashCode() : 0);
        hash = 37 * hash + (this.contacts != null ? this.contacts.hashCode() : 0);
        return hash;
    }

    /**
     * Writes itself into the database does not attempt to write existing orgs
     * into the database(OID > 0)
     *
     * @param con --- Database connection to write to
     * @return --- True iff write is successful
     * @throws SQLException
     */
    public boolean insertIntoDB(Connection con) throws SQLException {
        if (this.OID != 0) {
            return false;
        } else {
            Statement s = con.createStatement();
            String query = "INSERT INTO org VALUES (DEFAULT, " + sqlSanitizer.sanitizeString(name)
                    + "," + sqlSanitizer.sanitizeString(addr.getAddr1()) + "," + sqlSanitizer.sanitizeString(addr.getAddr2())
                    + "," + sqlSanitizer.sanitizeString(addr.getSuburb()) + "," + sqlSanitizer.sanitizeString(addr.getRegion()) + "," + sqlSanitizer.sanitizeString(addr.getCity())
                    + "," + sqlSanitizer.sanitizeString(addr.getCountry()) + "," + sqlSanitizer.sanitizeString(addr.getPostCode()) + ");";
            int result = s.executeUpdate(query);
            if (result == 1) {
                ResultSet res = s.executeQuery("SELECT LAST_INSERT_ID()");
                this.OID = res.getInt(1);
                res.close();
                for (Contact c : contacts) {
                    c.insertIntoDB(con, OID);
                }
                PreparedStatement insert = con.prepareStatement("INSERT INTO comments VALUES (DEFAULT, NULL, ?, NULL,"
                        + "?, " + con.getClientInfo("user") + ", ?, ?, NULL, NULL, NULL, NULL, NULL);");
                ResultSet r;
                for (Comment c : comments) {
                    if (c.getOID() == 0) {
                        c.setOID(OID);
                        insert.setInt(1, OID);
                        insert.setDate(2, c.getDate());
                        insert.setString(3, sqlSanitizer.sanitizeString(c.getText()));
                        insert.setInt(4, c.getPrivate() ? 1 : 0);
                        insert.executeUpdate();
                        r = s.executeQuery("SELECT LAST_INSERT_ID();");
                        r.next();
                        c.setCID(r.getInt(1));
                        r.close();
                    }
                }
                return true;
            } else {
                s.close();
                return false;
            }
        }
    }

    /**
     * Updates itself into the database does not attempt to write new orgs into
     * the database(OID == 0)
     *
     * @param con --- Database connection to write to
     * @return --- True iff write is successful
     * @throws SQLException
     */
    public boolean updateIntoDB(Connection con) throws SQLException {
        if (this.OID == 0) {
            return false;
        } else {
            Statement s = con.createStatement();
            String query = "UPDATE org SET Name=" + sqlSanitizer.sanitizeString(name)
                    + ", Address1=" + sqlSanitizer.sanitizeString(addr.getAddr1()) + ",Address2=" + sqlSanitizer.sanitizeString(addr.getAddr2())
                    + ", Suburb=" + sqlSanitizer.sanitizeString(addr.getSuburb()) + ", Region=" + sqlSanitizer.sanitizeString(addr.getRegion())
                    + ", City=" + sqlSanitizer.sanitizeString(addr.getCity()) + ",  Country=" + sqlSanitizer.sanitizeString(addr.getCountry())
                    + ", PostCode=" + sqlSanitizer.sanitizeString(addr.getPostCode()) + " WHERE OID=" + this.OID + ";";
            int result = s.executeUpdate(query);
            if (result == 1) {
                for (Contact c : contacts) {
                    if (c.getOID() == 0) {
                        c.insertIntoDB(con, OID);
                    } else {
                        c.updateIntoDB(con);
                    }
                }
                PreparedStatement delete = con.prepareStatement("DELETE FROM contacts WHERE CID=?");
                for (Contact c : this.removedContacts) {
                    delete.setInt(1, c.getCID());
                    delete.executeUpdate();
                }
                PreparedStatement insert = con.prepareStatement("INSERT INTO comments VALUES (DEFAULT, NULL, ?, NULL,"
                        + "?, " + con.getClientInfo("user") + ", ?, ?, NULL, NULL, NULL, NULL, NULL);");
                ResultSet r;
                for (Comment c : comments) {
                    if (c.getOID() == 0) {
                        c.setOID(OID);
                        insert.setInt(1, OID);
                        insert.setDate(2, c.getDate());
                        insert.setString(3, sqlSanitizer.sanitizeString(c.getText()));
                        insert.setInt(4, c.getPrivate() ? 1 : 0);
                        insert.executeUpdate();
                        r = s.executeQuery("SELECT LAST_INSERT_ID();");
                        r.next();
                        c.setCID(r.getInt(1));
                        r.close();
                    }
                }
                return true;
            } else {
                s.close();
                return false;
            }
        }
    }

    /**
     * Generates an SQL Select Query based on fields searches for every org
     * matching the values contained in this object..
     *
     * @return --- STring sql query
     */
    public String generateSelectQuery() {
        SelectQuery select = new SelectQuery();
        Table org = new Table("org");
        Table contacts = new Table("contacts");
        select.addColumn(org, "*"); //we need every column
        if (this.contacts.size() > 0) {//currently uses only the first contact.
            select.addJoin(org, "OID", contacts, "OID");
            if (this.contacts.get(0).getTitle() != null) {
                select.addCriteria(new MatchCriteria(contacts, "title", MatchCriteria.EQUALS, this.contacts.get(0).getTitle()));

            }
            select.addCriteria(new MatchCriteria(contacts, "PID", MatchCriteria.EQUALS, this.contacts.get(0).getPID())); //cannot be invalid, contactPanel disallows this,
        }
        if (this.name != null && !this.name.equals("")) {
            select.addCriteria(new MatchCriteria(org, "Name", MatchCriteria.EQUALS, this.name));
        }
        if (this.addr != null) {
            if (this.addr.getAddr1() != null && !this.addr.getAddr1().equals("NULL")) {
                select.addCriteria(new MatchCriteria(org, "Address1", MatchCriteria.EQUALS, this.addr.getAddr1()));
            }
            if (this.addr.getAddr2() != null && !this.addr.getAddr2().equals("NULL")) {
                select.addCriteria(new MatchCriteria(org, "Address2", MatchCriteria.EQUALS, this.addr.getAddr2()));
            }
            if (this.addr.getSuburb() != null && !this.addr.getSuburb().equals("NULL")) {
                select.addCriteria(new MatchCriteria(org, "Suburb", MatchCriteria.EQUALS, this.addr.getSuburb()));
            }
            if (this.addr.getCity() != null && !this.addr.getCity().equals("NULL")) {
                select.addCriteria(new MatchCriteria(org, "City", MatchCriteria.EQUALS, this.addr.getCity()));
            }
            if (this.addr.getRegion() != null && !this.addr.getRegion().equals("NULL")) {
                select.addCriteria(new MatchCriteria(org, "Region", MatchCriteria.EQUALS, this.addr.getRegion()));
            }
            if (this.addr.getCountry() != null && !this.addr.getCountry().equals("NULL")) {
                select.addCriteria(new MatchCriteria(org, "Country", MatchCriteria.EQUALS, this.addr.getCountry()));
            }
            if (this.addr.getPostCode() != null && !this.addr.getPostCode().equals("NULL")) {
                select.addCriteria(new MatchCriteria(org, "PostCode", MatchCriteria.EQUALS, this.addr.getPostCode()));
            }
        }
        return select.toString();
    }

    /**
     * Checks if any values have been set.
     *
     * @return --- True iff no values are set
     */
    public boolean isEmpty() {
        if (this.name != null) {
            return false;
        }
        if (!this.addr.isEmpty()) {
            return false;
        }
        if (this.contacts.size() > 0) {
            return false;
        }
        return true;
    }

    public void removeContact(Contact c) {
        this.removedContacts.add(c);
    }
}
