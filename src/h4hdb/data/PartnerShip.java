/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package h4hdb.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Jin
 */
public class PartnerShip implements IDeletable{
    private int fID;
    private Person person;
    private boolean living;
    private boolean working;
    private boolean markedForDeletion;
    
    public PartnerShip(int fID, Person person, boolean living, boolean working) {
        this.fID = fID;
        this.person = person;
        this.living = living;
        this.working = working;
        markedForDeletion = false;
    }

    public PartnerShip(ResultSet rs) throws SQLException {
        this.fID = rs.getInt("FID");
        this.person = null;
        this.living = rs.getBoolean("Living");
        this.working =  rs.getBoolean("Working");
        markedForDeletion = false;
    }    
    /**
     * @return the fID
     */
    public int getfID() {
        return fID;
    }

    /**
     * @param fID the fID to set
     */
    public void setfID(int fID) {
        this.fID = fID;
    }

    /**
     * @return the person
     */
    public Person getPerson() {
        return person;
    }

    /**
     * @param person the person to set
     */
    public void setPerson(Person person) {
        this.person = person;
    }

    /**
     * @return the living
     */
    public boolean isLiving() {
        return living;
    }

    /**
     * @param living the living to set
     */
    public void setLiving(boolean living) {
        this.living = living;
    }

    /**
     * @return the working
     */
    public boolean isWorking() {
        return working;
    }

    /**
     * @param working the working to set
     */
    public void setWorking(boolean working) {
        this.working = working;
    }

    public void saveToDatabase(Connection con) throws SQLException
    {
        
        
        
        java.sql.CallableStatement cs = con.prepareCall("{call ModifyPartnership(?,?,?,?,?)}");
        cs.setInt(1, this.fID);
        cs.setInt(2, this.person.getPID());
        cs.setBoolean(3, this.isLiving());
        cs.setBoolean(4, this.isWorking());
        cs.setBoolean(5,this.needToDelete());
        cs.execute();
    }

    @Override
    public void markForDelete() {
        markedForDeletion = true;
    }

    @Override
    public boolean needToDelete() {
        return markedForDeletion;
    }
    
    
    
    
}
