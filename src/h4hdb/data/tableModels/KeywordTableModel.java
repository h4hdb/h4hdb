package h4hdb.data.tableModels;

import h4hdb.data.PersonKeyword;
import java.util.List;
import javax.swing.event.TableModelEvent;
import javax.swing.table.AbstractTableModel;

/**
 * Model for keywords in a table.
 * 
 * @author Leliel Trethowen
 */
public class KeywordTableModel extends AbstractTableModel {
    private String[] columnNames = {"Name", "Description", "Level", "Constraints"};
    private List<PersonKeyword> data;
    
    public KeywordTableModel(List<PersonKeyword> keywords) {
        data = keywords;
    }
    
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }
    
    @Override
    public int getRowCount() {
        return data.size();
    }
    
    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }
    
    @Override
    public String getValueAt(int row, int col) {
        switch (col) {
            case 0:
                return data.get(row).getWord().getName();
            case 1:
                if (data.get(row).getWord().getDescription() == null)
                    return "";
                return data.get(row).getWord().getDescription();
            case 2:
                switch(data.get(row).getLevel()){
                    case -1 :
                        return "N/A";
                    case 0 :
                        return "Unskilled";
                    case 1:
                        return "Semi Skilled";
                    case 2:
                        return "Skilled";
                    case 3:
                        return "Professional";
                }                
            case 3:
                if (data.get(row).getConstraints() == null)
                    return "";
                return data.get(row).getConstraints();
            default:
                throw new IndexOutOfBoundsException();
        }
    }
    
    public PersonKeyword getUnderlyingRow(int index) {
        return data.get(index);
    }
    
    @Override
    public Class getColumnClass(int c) {
        return String.class;
    }
    
    @Override
    public boolean isCellEditable(int row, int col) {
        return false;
    }
    
    @Override
    public void setValueAt(Object value, int row, int col) {
        throw new UnsupportedOperationException("Not Editable");
    }
    
    public void replaceValues(List<PersonKeyword> values) {
        this.data = values;
        TableModelEvent event = new TableModelEvent(this);
        this.fireTableChanged(event);
    }
    
    public void addRow(PersonKeyword keyword) {
        data.add(keyword);
        this.fireTableRowsInserted(data.size() - 1, data.size() - 1);
    }
    
    public void removeRow(int index) {
        data.remove(index);
        this.fireTableRowsDeleted(index, index);
    }
    
    public void replaceRow(int index, PersonKeyword keyword){
        data.set(index, keyword);
        this.fireTableRowsUpdated(index, index);
    }
}
