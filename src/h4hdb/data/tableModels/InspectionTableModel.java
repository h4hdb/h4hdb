/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package h4hdb.data.tableModels;

import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Fintan
 */
public class InspectionTableModel extends AbstractTableModel {

    private String name;
    private String[] columnNames;
    private static final int columnCount = 2;
    private String[][] data;

    public InspectionTableModel(String tableName, String[][] rows) {
        columnNames = new String[]{tableName, "Suggested Action"};
        name = tableName;
        data = new String[rows.length][columnCount];
        data = rows;
    }

    @Override
    public boolean isCellEditable(int row, int columnIndex) {
        if (columnIndex > 0) {
            return true;
        }
        return false;
    }

    @Override
    public Object getValueAt(int row, int column) {
        return data[row][column];
    }

    @Override
    public Class getColumnClass(int c) {
        return String.class;
    }

    @Override
    public void setValueAt(Object value, int row, int col) {
        if (isCellEditable(row, col)) {
            data[row][col] = (String) value;
        } else {
            throw new UnsupportedOperationException("Not Editable");
        }
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public int getRowCount() {
        return data.length;
    }

    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }
    
    public String getTableNotes(){
        String notes = null;
        for(int i = 0;i < data.length; i++){
            if(data[i][1] != null){
                notes = notes + "\n-->" + data[i][0] + ": " + data[i][1];
            }
        }
        if(notes != null){
            notes = "\n" + columnNames[0] + notes;
        }
        return notes;
    }
}
