/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package h4hdb.data.tableModels;

import h4hdb.data.Team;
import java.util.List;
import javax.swing.event.TableModelEvent;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author lelitu
 */
public class TeamTableModel extends AbstractTableModel {

    private List<Team> data;
    private String[] columnNames = {"Name", "Address1", "Address2", "Suburb", "City", "Region", "Email", "Mobile", "Landline1", "Landline2"};

    public TeamTableModel(List<Team> data) {
        this.data = data;
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int row, int col) {
        switch (col) {
            case 0:
                return data.get(row).getName() == null ? "" : data.get(row).getName();
            case 1:
                if (data.get(row).getAddress().getAddr1() == null || data.get(row).getAddress().getAddr1().equals("")) {
                    return "";
                }
                return data.get(row).getAddress().getAddr1();
            case 2:
                if (data.get(row).getAddress().getAddr2() == null || data.get(row).getAddress().getAddr2().equals("")) {
                    return "";
                }
                return data.get(row).getAddress().getAddr2();
            case 3:
                if (data.get(row).getAddress().getSuburb() == null || data.get(row).getAddress().getSuburb().equals("")) {
                    return "";
                }
                return data.get(row).getAddress().getSuburb();
            case 4:
                if (data.get(row).getAddress().getCity() == null || data.get(row).getAddress().getCity().equals("")) {
                    return "";
                }
                return data.get(row).getAddress().getCity();
            case 5:
                if (data.get(row).getAddress().getRegion() == null || data.get(row).getAddress().getRegion().equals("")) {
                    return "";
                }
                return data.get(row).getAddress().getRegion();
            case 6:
                if (data.get(row).getContacts().getEmail() == null || data.get(row).getContacts().getEmail().equals("")) {
                    return "";
                }
                return data.get(row).getContacts().getEmail();
            case 7:
                if (data.get(row).getContacts().getcPhone() == null || data.get(row).getContacts().getcPhone().equals("")) {
                    return "";
                }
                return data.get(row).getContacts().getcPhone();
            case 8:
                if (data.get(row).getContacts().gethPhone() == null || data.get(row).getContacts().gethPhone().equals("")) {
                    return "";
                }
                return data.get(row).getContacts().gethPhone();
            case 9:
                if (data.get(row).getContacts().getwPhone() == null || data.get(row).getContacts().getwPhone().equals("")) {
                    return "";
                }
                return data.get(row).getContacts().getwPhone();
            default:
                throw new IndexOutOfBoundsException();
        }
    }

    public Team getUnderlyingRow(int index) {
        return data.get(index);
    }

    @Override
    public Class getColumnClass(int c) {
        if (c != 2) {
            return String.class;
        }
        return null;//what class will it be?
    }

    @Override
    public boolean isCellEditable(int row, int col) {
        return false;
    }

    @Override
    public void setValueAt(Object value, int row, int col) {
        throw new UnsupportedOperationException("Not Editable");
    }

    public void replaceValues(List<Team> values) {
        this.data = values;
        TableModelEvent event = new TableModelEvent(this);
        this.fireTableChanged(event);
    }
}