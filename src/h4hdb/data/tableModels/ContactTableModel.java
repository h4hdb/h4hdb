/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package h4hdb.data.tableModels;

import h4hdb.data.Contact;
import h4hdb.data.Person;
import java.util.List;
import javax.swing.event.TableModelEvent;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author lelitu
 */
public class ContactTableModel extends AbstractTableModel {
    
    private String[] columnNames = {"Name", "Title", "Email", "Mobile", "Landline", "Landline"};
    private List<Contact> data;
    
    public ContactTableModel(List<Contact> contacts) {
        data = contacts;
    }
    
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }
    
    @Override
    public int getRowCount() {
        return data.size();
    }
    
    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }
    
    @Override
    public String getValueAt(int row, int col) {
        switch (col) {
            case 0:
                Person p = data.get(row).getPerson();
                if (p.getPrefName() == null || p.getPrefName().equals("")) {
                    return p.getfName() + " " + p.getlName();
                } else {
                    return p.getPrefName();
                }
            case 1:
                return data.get(row).getTitle();
            case 2:
                if (data.get(row).getContacts().getEmail() == null || data.get(row).getContacts().getEmail().equals(""))
                    return "";
                return data.get(row).getContacts().getEmail();
            case 3:
                if (data.get(row).getContacts().gethPhone() == null || data.get(row).getContacts().gethPhone().equals(""))
                    return "";
                return data.get(row).getContacts().gethPhone();
            case 4:
                if (data.get(row).getContacts().getwPhone() == null || data.get(row).getContacts().getwPhone().equals(""))
                    return "";
                return data.get(row).getContacts().getwPhone();
            case 5:
                if (data.get(row).getContacts().getcPhone() == null || data.get(row).getContacts().getcPhone().equals(""))
                    return "";
                return data.get(row).getContacts().getcPhone();
            default:
                throw new IndexOutOfBoundsException();
        }
    }
    
    public Contact getUnderlyingRow(int index) {
        return data.get(index);
    }
    
    @Override
    public Class getColumnClass(int c) {
        return String.class;
    }
    
    @Override
    public boolean isCellEditable(int row, int col) {
        return false;
    }
    
    @Override
    public void setValueAt(Object value, int row, int col) {
        throw new UnsupportedOperationException("Not Editable");
    }
    
    public void replaceValues(List<Contact> values) {
        this.data = values;
        TableModelEvent event = new TableModelEvent(this);
        this.fireTableChanged(event);
    }
    
    public void addRow(Contact contact) {
        data.add(contact);
        this.fireTableRowsInserted(data.size() - 1, data.size() - 1);
    }
    
    public void removeRow(int index) {
        data.remove(index);
        this.fireTableRowsDeleted(index, index);
    }
    
    public void replaceRow(int index, Contact contact){
        data.set(index, contact);
        this.fireTableRowsUpdated(index, index);
    }
}
