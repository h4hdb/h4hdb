/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package h4hdb.data.tableModels;

import h4hdb.data.Org;
import java.util.List;
import javax.swing.event.TableModelEvent;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author lelitu
 */
public class OrgTableModel extends AbstractTableModel{
    private List<Org> data;
    private String[] columnNames = {"Name","Contacts", "Address1", "Address2", "Suburb", "City", "Region"}; 

    public OrgTableModel(List<Org> data){
        this.data = data;
    }
    
    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int row, int col) {
        switch (col){
            case 0:
                return data.get(row).getName() == null ? "" : data.get(row).getName();
            case 1: //hrm... return a list. that's fine
                //TODO implement TableListRenderer, use what component? button with a popup? combobox?
                return  "not implemented";
            case 2:
                if (data.get(row).getAddr().getAddr1() == null || data.get(row).getAddr().getAddr1().equals(""))
                    return "";
                return data.get(row).getAddr().getAddr1();
            case 3:
                if (data.get(row).getAddr().getAddr2() == null || data.get(row).getAddr().getAddr2().equals(""))
                    return "";
                return data.get(row).getAddr().getAddr2();
            case 4:
                if (data.get(row).getAddr().getSuburb() == null || data.get(row).getAddr().getSuburb().equals(""))
                    return "";
                return data.get(row).getAddr().getSuburb();
            case 5:
                if (data.get(row).getAddr().getCity() == null || data.get(row).getAddr().getCity().equals(""))
                    return "";
                return data.get(row).getAddr().getCity();
            case 6:
                if (data.get(row).getAddr().getRegion() == null || data.get(row).getAddr().getRegion().equals(""))
                    return "";
                return data.get(row).getAddr().getRegion();
            default:
                throw new IndexOutOfBoundsException();
        }
    }
    
    public Org getUnderlyingRow(int index) {
        return data.get(index);
    }
    
    @Override
    public Class getColumnClass(int c) {
        if (c != 2)
            return String.class;
        return null;//what class will it be?
    }
    
    @Override
    public boolean isCellEditable(int row, int col) {
        return false;
    }
    
    @Override
    public void setValueAt(Object value, int row, int col) {
        throw new UnsupportedOperationException("Not Editable");
    }
    
    public void replaceValues(List<Org> values) {
        this.data = values;
        TableModelEvent event = new TableModelEvent(this);
        this.fireTableChanged(event);
    }
}
