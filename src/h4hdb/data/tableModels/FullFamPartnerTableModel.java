/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package h4hdb.data.tableModels;

import h4hdb.data.FamilyPartner;
import java.util.Date;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Fintan
 */
public class FullFamPartnerTableModel extends AbstractTableModel {

    private String[] columnNames = {"Street Address", "Suburb", "City", "Region", "Country", "Postcode",
        "Date Started", "Date Completed", "Stage",
        "Type", "Value", "Cost"};
    private List<FamilyPartner> data;

    /**
     *
     * @param people
     */
    public FullFamPartnerTableModel(List<FamilyPartner> people) {
        data = people;
    }
    
    
    public void setData(List<FamilyPartner> people){
         data = people;
         this.fireTableDataChanged();
    }
    
//    public void removeRow(int rowIndex){
//        data.remove(rowIndex);
//        this.fireTableRowsDeleted(rowIndex, rowIndex);
//    }
//    
//    public void addRow(FamilyPartner fp){
//        data.add(fp);
//        this.fireTableRowsInserted(getRowCount(), getRowCount());
//    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int row, int col) {
        switch (col) {
            case 0:
                //Street Address Addr1 + Addr2
                return data.get(row).getLocation().getAddr1() + " " + data.get(row).getLocation().getAddr2();
            case 1:
                //Suburb
                return data.get(row).getLocation().getSuburb();
            case 2:
                //City
                return data.get(row).getLocation().getCity();
            case 3:
                //Region
                return data.get(row).getLocation().getRegion();
            case 4:
                //Country
                return data.get(row).getLocation().getCountry();
            case 5:
                //Post code
                return data.get(row).getLocation().getPostCode();
            case 6:
                //Start Date
                return data.get(row).getStartDate();
            case 7:
                //End Date
                return data.get(row).getEndDate();
            case 8:
                //Stage
                return data.get(row).getStage();
            case 9:
                //Type
                return data.get(row).getType();
            case 10:
                //Value
                return data.get(row).getValue() + "";
            case 11:
                //Cost
                return data.get(row).getCost() + "";
            default:
                throw new IndexOutOfBoundsException();
        }
    }

    public FamilyPartner getUnderlyingRow(int index) {
        return data.get(index);
    }

    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }

    @Override
    public Class getColumnClass(int c) {
        if (columnNames[c].contains("Date")) {
            return Date.class;
        } else {
            return String.class;
        }
    }
}
