/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package h4hdb.data.tableModels;

import h4hdb.data.FamilyPartner;
import h4hdb.util.Converters;
import java.text.DateFormat;
import java.util.List;
import javax.swing.event.TableModelEvent;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Brian
 */
public class FamPartnerTableModel extends AbstractTableModel {

    private String[] columnNames = {"Address", "Stage", "Type", "Value", "Cost", "ConsentRequired",
            "ConsentDate", "StartDate", "EndDate", "ActualEndDate"};
    private List<FamilyPartner> data;
    
    public FamPartnerTableModel(List<FamilyPartner> fampartner) {
        data = fampartner;
    }
    
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }
    
    @Override
    public int getRowCount() {
        return data.size();
    }
    
    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }
    
    @Override
    public String getValueAt(int row, int col) {
        DateFormat f = DateFormat.getDateInstance(DateFormat.LONG);
        switch (col) {
            case 0:
                String address = "";
                if (data.get(row).getLocation().getAddr1() == null) address += "";
                else address += data.get(row).getLocation().getAddr1();
            
                if (data.get(row).getLocation().getAddr2() == null) address += "";
                else address += ", " + data.get(row).getLocation().getAddr2();
            
                if (data.get(row).getLocation().getSuburb() == null) address += "";
                else address += ", " + data.get(row).getLocation().getSuburb();
            
                if (data.get(row).getLocation().getCity() == null) address += "";
                else address += ", " + data.get(row).getLocation().getCity();
            
                if (data.get(row).getLocation().getRegion() == null) address += "";
                else address += ", " + data.get(row).getLocation().getRegion();
            
                if (data.get(row).getLocation().getCountry() == null) address += "";
                else address += ", " + data.get(row).getLocation().getCountry();
           
                if (data.get(row).getLocation().getPostCode() == null) address += "";
                else address += ", " + data.get(row).getLocation().getPostCode();
                
                return address;
            case 1:
                if (data.get(row).getStage() == null)
                    return "";
                else return data.get(row).getStage();
            case 2:
                if (data.get(row).getType() == null)
                    return "";
                else return data.get(row).getType()+"";
            case 3:
                return Converters.addtwoDecimalPlaces(data.get(row).getValue());
            case 4:
                return Converters.addtwoDecimalPlaces(data.get(row).getCost());
            case 5:
                if (data.get(row).isConsentRequired()) return "Yes";
                else return "No";
            case 6:
                if (data.get(row).getDateOfConsent() == null)
                    return "";
                else return f.format(data.get(row).getDateOfConsent());
            case 7:
                if (data.get(row).getStartDate() == null)
                    return "";
                else return f.format(data.get(row).getStartDate());
            case 8:
                if (data.get(row).getEndDate() == null)
                    return "";
                else return f.format(data.get(row).getEndDate());
            case 9:
                if (data.get(row).getActualEndDate() == null)
                    return "";
                else return f.format(data.get(row).getActualEndDate());
            default:
                throw new IndexOutOfBoundsException();
        }
    }
    
    public FamilyPartner getUnderlyingRow(int index) {
        return data.get(index);
    }
    
    @Override
    public Class getColumnClass(int c) {
        return String.class;
    }
    
    @Override
    public boolean isCellEditable(int row, int col) {
        return false;
    }
    
    @Override
    public void setValueAt(Object value, int row, int col) {
        throw new UnsupportedOperationException("Not Editable");
    }

    public void replaceValues(List<FamilyPartner> values) {
        this.data = values;
        TableModelEvent event = new TableModelEvent(this);
        this.fireTableChanged(event);
    }
    
    public void addRow(FamilyPartner fampartner) {
        data.add(fampartner);
        this.fireTableRowsInserted(data.size() - 1, data.size() - 1);
    }
    
    public void removeRow(int index) {
        data.remove(index);
        this.fireTableRowsDeleted(index, index);
    }
    
    public void replaceRow(int index, FamilyPartner fampartner){
        data.set(index, fampartner);
        this.fireTableRowsUpdated(index, index);
    }
    
}
