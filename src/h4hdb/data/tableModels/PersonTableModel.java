/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package h4hdb.data.tableModels;

import h4hdb.data.Person;
import java.util.List;
import javax.swing.event.TableModelEvent;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author lelitu
 */
 public class PersonTableModel extends AbstractTableModel {

        private String[] columnNames = {"Name", "Suburb", "City", "Region"};
        private List<Person> data;

        public PersonTableModel(List<Person> people) {
            data = people;
        }

        @Override
        public int getColumnCount() {
            return columnNames.length;
        }

        @Override
        public int getRowCount() {
            return data.size();
        }

        @Override
        public String getColumnName(int col) {
            return columnNames[col];
        }

        @Override
        public String getValueAt(int row, int col) {
            switch (col) {
                case 0:
                    Person p = data.get(row);
                    if (p.getPrefName() != null){
                        return p.getPrefName();
                    } else {
                        return p.getfName() + " "+ p.getlName(); 
                    }
                case 1:
                    return data.get(row).getAddress().getSuburb();
                case 2:
                    return data.get(row).getAddress().getCity();
                case 3:
                    return data.get(row).getAddress().getRegion();
                default:
                    throw new IndexOutOfBoundsException();
            }
        }

        public Person getUnderlyingRow(int index) {
            return data.get(index);
        }

        @Override
        public Class getColumnClass(int c) {
            return String.class;
        }

        @Override
        public boolean isCellEditable(int row, int col) {
            return false;
        }

        @Override
        public void setValueAt(Object value, int row, int col) {
            throw new UnsupportedOperationException("Not Editable");
        }

        public void replaceValues(List<Person> values) {
            this.data = values;
            TableModelEvent event = new TableModelEvent(this);
            this.fireTableChanged(event);

        }
    }
