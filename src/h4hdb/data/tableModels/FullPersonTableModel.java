/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package h4hdb.data.tableModels;

import h4hdb.data.Person;
import h4hdb.data.Title;
import java.text.DateFormat;
import java.util.List;
import javax.swing.event.TableModelEvent;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author lelitu
 */
public class FullPersonTableModel extends AbstractTableModel {

    private String[] columnNames = {"Title", "Name", "Ethnicity", "Gender", "Faith" ,
        "Date of Birth" , "Date of Death" , "Contact Date" ,
        "Active" , "Local News" , "National News" , "Reports" , "Magazines" ,
        "Email" , "Home Phone" , "Work Phone" , "Mobile" ,
        "Address1" , "Address2" , "Suburb" , "City" , "Region" , "Country" , "Post Code" };
    private List<Person> data;

    public FullPersonTableModel(List<Person> people) {
        data = people;
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }

    @Override
    public Class getColumnClass(int c) {
        if (columnNames[c].equals("Active")) {
            return Boolean.class;
        } else {
            return String.class;
        }
    }

    @Override
    public boolean isCellEditable(int row, int col) {
        return false;
    }

    @Override
    public void setValueAt(Object value, int row, int col) {
        throw new UnsupportedOperationException("Not Editable");
    }

    @Override
    public Object getValueAt(int row, int col) {
        DateFormat f = DateFormat.getDateInstance(DateFormat.LONG);
        switch (col) {
            case 0:
                if (data.get(row).getTitle() == Title.NONE)
                    return "";
                else return data.get(row).getTitle().toString();
            case 1:
                Person p = data.get(row);
                if (p.getPrefName() != null) {
                    return p.getPrefName();
                } else {
                    return p.getfName() + " " + p.getlName();
                }
            case 2:
                if (data.get(row).getEthnicity() == null)
                    return "";
                else return data.get(row).getEthnicity();
            case 3:
                    if (data.get(row).getGender() == null)
                        return "";
                return data.get(row).getGender();
            case 4:
                    if (data.get(row).getFaith() == null)
                        return "";
                return data.get(row).getFaith();
            case 5:
                return f.format(data.get(row).getDob());
            case 6:
                if (data.get(row).getDod() == null)
                    return "";
                return f.format(data.get(row).getDod());
            case 7:
                if (data.get(row).getCreationDate() == null)
                    return "";
                return f.format(data.get(row).getCreationDate());
            case 8:
                return data.get(row).isActive();
            case 9:
                if (data.get(row).getLocNews() == null)
                    return "";
                return data.get(row).getLocNews();
            case 10:
                 if (data.get(row).getNatNews() == null)
                    return "";
                return data.get(row).getNatNews();
            case 11:
                if (data.get(row).getReports() == null)
                    return "";                
                return data.get(row).getReports();
            case 12:
                if (data.get(row).getMags() == null)
                    return "";        
                return data.get(row).getMags();
            case 13:
                if (data.get(row).getContacts().getEmail() == null || data.get(row).getContacts().getEmail().equals(""))
                    return "";
                return data.get(row).getContacts().getEmail();
            case 14:
                if (data.get(row).getContacts().gethPhone() == null || data.get(row).getContacts().gethPhone().equals(""))
                    return "";
                return data.get(row).getContacts().gethPhone();
            case 15:
                if (data.get(row).getContacts().getwPhone() == null || data.get(row).getContacts().getwPhone().equals(""))
                    return "";
                return data.get(row).getContacts().getwPhone();
            case 16:
                if (data.get(row).getContacts().getcPhone() == null || data.get(row).getContacts().getcPhone().equals(""))
                    return "";
                return data.get(row).getContacts().getcPhone();
            case 17:
                if (data.get(row).getAddress().getAddr1() == null || data.get(row).getAddress().getAddr1().equals(""))
                    return "";
                return data.get(row).getAddress().getAddr1();
            case 18:
                if (data.get(row).getAddress().getAddr2() == null || data.get(row).getAddress().getAddr2().equals(""))
                    return "";
                return data.get(row).getAddress().getAddr2();
            case 19:
                if (data.get(row).getAddress().getSuburb() == null || data.get(row).getAddress().getSuburb().equals(""))
                    return "";
                return data.get(row).getAddress().getSuburb();
            case 20:
                if (data.get(row).getAddress().getCity() == null || data.get(row).getAddress().getCity().equals(""))
                    return "";
                return data.get(row).getAddress().getCity();
            case 21:
                if (data.get(row).getAddress().getRegion() == null || data.get(row).getAddress().getRegion().equals(""))
                    return "";
                return data.get(row).getAddress().getRegion();
            case 22:
                if (data.get(row).getAddress().getCountry() == null || data.get(row).getAddress().getCountry().equals(""))
                    return "";
                return data.get(row).getAddress().getCountry();
            case 23:
                if (data.get(row).getAddress().getPostCode() == null || data.get(row).getAddress().getPostCode().equals(""))
                    return "";
                return data.get(row).getAddress().getPostCode();
            default:
                throw new IndexOutOfBoundsException();
        }
    }

    public Person getUnderlyingRow(int index) {
        return data.get(index);
    }

    public void replaceValues(List<Person> values) {
        this.data = values;
        TableModelEvent event = new TableModelEvent(this);
        this.fireTableChanged(event);

    }
}
