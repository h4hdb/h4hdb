/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package h4hdb.data;

/**
 *
 * @author Mehendra
 */
public interface IDeletable {
    void markForDelete();
    boolean needToDelete(); 
}
