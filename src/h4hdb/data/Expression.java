package h4hdb.data;

/**
 * 
 * @author Geng Expression are used in WHERE clause of a SQL SELECT statement
 *         column_name(s) is expected of the form table_name.attribute_name for multiple tables
 *         or just the attribute_name for a single table (i.e searchPerson )
 *         mathType are one of the following ("=", ">", ">=", "<", "<=", "LIKE",
 *         "<>")
 */
public class Expression {
	public enum MatchOperator {

		EQUALS("="), GREATER(">"), GREATEREQUAL(">="), LESS("<"), LESSEQUAL(
				"<="), LIKE("LIKE"), NOTEQUAL("<>"), JOIN("JOIN");
		private String matchType;

		private MatchOperator(String matchType) {
			this.matchType = matchType;
		}

		private String getMatchType() {
			return matchType;
		}
	}

	private String columnName;
	private MatchOperator matchType;
	private Object value;

	public Expression(String columnName, MatchOperator op, Object value) {
		this.columnName = columnName;
		this.matchType = op;
		this.value = value;

	}

	public String matchType() {
		return matchType.getMatchType();
	}

	public String columnName() {
		return columnName;
	}


	public Object value() {
		return value;
	}

}
