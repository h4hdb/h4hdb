/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package h4hdb.data;

import com.truemesh.squiggle.SelectQuery;
import com.truemesh.squiggle.Table;
import com.truemesh.squiggle.criteria.MatchCriteria;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a person as modeled by the database
 *
 * @author Leliel Trethowen
 */
public class Person {

    public static final String FIELD_ID = "FID";
    public static final String FIELD_FIRST_NAME = "FName";
    public static final String FIELD_LAST_NAME = "LName";
    public static final String FIELD_PREF_NAME = "PrefName";
    public static final String FIELD_EMAIL = "Email";
    public static final String FIELD_LAND_LINE = "Landline";
    public static final String FIELD_WORK_PHONE = "WorkPhone";
    public static final String FIELD_MOBILE = "Mobile";
    public static final String FIELD_ADDR1 = "Address1";
    public static final String FIELD_ADDR2 = "Address2";
    public static final String FIELD_SUBURB = "Suburb";
    public static final String FIELD_CITY = "City";
    public static final String FIELD_REGION = "Region";
    public static final String FIELD_COUNTRY = "Country";
    public static final String FIELD_POSTCODE = "PostCode";
    private int PID;
    private Title title = Title.NONE;
    private String fName;
    private String lName;
    private String prefName;
    private Contacts contacts;
    private Address address;
    private Date dob;
    private Date dod;
    private Date creationDate;
    private String ethnicity;
    private String gender;
    private String faith;
    private String referrer;
    private boolean active;
    private commType natNews;
    private commType locNews;
    private commType reports;
    private commType mags;
    private List<Comment> comments;
    private List<PersonKeyword> keywords;
    private List<PersonKeyword> removedKeywords = new ArrayList<PersonKeyword>();

    /**
     * Constructs an empty person.
     */
    public Person() {
        this.address = new Address();
        this.contacts = new Contacts();
        this.comments = new ArrayList<Comment>();
        this.keywords = new ArrayList<PersonKeyword>();
    }

    public Person(ResultSet rs) throws SQLException {
        natNews = commType.None;
        locNews = commType.None;
        reports = commType.None;
        mags = commType.None;
        if (rs.getString("requestNatNews") != null) {
            natNews = commType.valueOf(rs.getString("requestNatNews"));
        }
        if (rs.getString("requestNews") != null) {
            locNews = commType.valueOf(rs.getString("requestNews"));
        }
        if (rs.getString("requestReports") != null) {
            reports = commType.valueOf(rs.getString("requestReports"));
        }
        if (rs.getString("requestMags") != null) {
            mags = commType.valueOf(rs.getString("requestMags"));
        }
        PID = rs.getInt("PID");
        title = Title.valueOf(rs.getString("Title").toUpperCase());
        fName = rs.getString("FName");
        lName = rs.getString("LName");
        prefName = rs.getString("PrefName");
        contacts = new Contacts(rs.getString("Landline"), rs.getString("WorkPhone"),
                rs.getString("Mobile"), rs.getString("Email"));
        address = new Address(rs.getString("Address1"), rs.getString("Address2"),
                rs.getString("Suburb"), rs.getString("City"),
                rs.getString("Region"), rs.getString("Country"),
                rs.getString("PostCode"));
        dob = rs.getDate("DateOfBirth");
        dod = rs.getDate("DateOfDeath");
        creationDate = rs.getDate("CreationDate");
        ethnicity = rs.getString("Ethnicity");
        gender = rs.getString("Gender");
        faith = rs.getString("Faith");
        referrer = rs.getString("Referrer");
        active = rs.getBoolean("isActive");
    }

    /**
     * Constructs a person with all fields set to provided values.
     *
     * @param PID --- int Person ID, primary key
     * @param title
     * @param fName
     * @param lName
     * @param prefName
     * @param hPhone
     * @param wPhone
     * @param cPhone
     * @param email
     * @param addr1
     * @param addr2
     * @param suburb
     * @param city
     * @param region
     * @param country
     * @param postCode
     * @param dob
     * @param dod
     * @param creationDate
     * @param ethnicity
     * @param gender
     * @param faith
     * @param referrrer
     * @param active
     * @param natNews
     * @param locNews
     * @param reports
     * @param mags
     * @param comments
     */
    public Person(int PID, Title title, String fName, String lName, String prefName,
            String hPhone, String wPhone, String cPhone, String email,
            String addr1, String addr2, String suburb, String city, String region,
            String country, String postCode, Date dob, Date dod, Date creationDate,
            String ethnicity, String gender, String faith, String referrrer, boolean active,
            commType natNews, commType locNews, commType reports, commType mags, List<Comment> comments,
            List<PersonKeyword> keywords) {
        this.PID = PID;
        this.title = title;
        this.fName = fName;
        this.lName = lName;
        this.prefName = prefName;
        this.contacts = new Contacts(hPhone, wPhone, cPhone, email);
        this.address = new Address(addr1, addr2, suburb, city, region, country, postCode);
        this.dob = dob;
        this.dod = dod;
        this.creationDate = creationDate;
        this.ethnicity = ethnicity;
        this.gender = gender;
        this.faith = faith;
        this.referrer = referrrer;
        this.active = active;
        this.locNews = locNews;
        this.natNews = natNews;
        this.reports = reports;
        this.mags = mags;
        this.comments = comments;
        this.keywords = keywords;
    }

    /**
     * Constucts a person with the minimum information required for a Contact.
     *
     * @param PID --- int PersonID
     * @param fName --- String a name
     */
    public Person(int PID, String fName) {
        this.PID = PID;
        this.fName = fName;
        this.comments = new ArrayList<Comment>();
        this.keywords = new ArrayList<PersonKeyword>();
    }

    public int getPID() {
        return PID;
    }

    public void setPID(int PID) {
        this.PID = PID;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Contacts getContacts() {
        return contacts;
    }

    public void setContacts(Contacts contacts) {
        this.contacts = contacts;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public Date getDod() {
        return dod;
    }

    public void setDod(Date dod) {
        this.dod = dod;
    }

    public String getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getFaith() {
        return faith;
    }

    public void setFaith(String faith) {
        this.faith = faith;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public String getPrefName() {
        return prefName;
    }

    public void setPrefName(String prefName) {
        this.prefName = prefName;
    }

    public String getReferrer() {
        return referrer;
    }

    public void setReferrer(String referrer) {
        this.referrer = referrer;
    }

    public Title getTitle() {
        return title;
    }

    public void setTitle(Title title) {
        this.title = title;
    }

    public List<Comment> getComments() {
        return this.comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public void addComment(Comment c) {
        this.comments.add(c);
    }

    public commType getLocNews() {
        return locNews;
    }

    public void setLocNews(commType locNews) {
        this.locNews = locNews;
    }

    public commType getMags() {
        return mags;
    }

    public void setMags(commType mags) {
        this.mags = mags;
    }

    public commType getNatNews() {
        return natNews;
    }

    public void setNatNews(commType natNews) {
        this.natNews = natNews;
    }

    public commType getReports() {
        return reports;
    }

    public void setReports(commType reports) {
        this.reports = reports;
    }

    public List<PersonKeyword> getKeywords() {
        return this.keywords;
    }

    public void setKeywords(List<PersonKeyword> keywords) {
        this.keywords = keywords;
    }

    public void removeKeyword(PersonKeyword k) {
        this.removedKeywords.add(k);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Person other = (Person) obj;
        if (this.PID != other.PID) {
            return false;
        }
        if (this.title != other.title) {
            return false;
        }
        if ((this.fName == null) ? (other.fName != null) : !this.fName.equals(other.fName)) {
            return false;
        }
        if ((this.lName == null) ? (other.lName != null) : !this.lName.equals(other.lName)) {
            return false;
        }
        if ((this.prefName == null) ? (other.prefName != null) : !this.prefName.equals(other.prefName)) {
            return false;
        }
        if (this.contacts != other.contacts && (this.contacts == null || !this.contacts.equals(other.contacts))) {
            return false;
        }
        if (this.address != other.address && (this.address == null || !this.address.equals(other.address))) {
            return false;
        }
                
        if (this.dob != other.dob && (this.dob == null || !this.dob.equals(other.dob))) {
            return false;
        }
        if (this.dod != other.dod && (this.dod == null || !this.dod.equals(other.dod))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + this.PID;
        return hash;
    }

    @Override
    public String toString() {
        return fName;
    }

    /**
     * Writes a new person into the database will not attempt to insert an
     * existing person.
     *
     * @param con --- Database connection to write to
     * @return --- True iff write succeeded
     * @throws SQLException
     */
    public boolean insertIntoDB(Connection con) throws SQLException {
        if (this.PID != 0) {
            return false;
        }
        Statement s = con.createStatement();
        String dodString = null;
        if (dod != null) {
            dodString = sqlSanitizer.sanitizeString(dod.toString());
        } else {
            dodString = "NULL";
        }
        String query = "INSERT INTO person VALUES (DEFAULT," + sqlSanitizer.sanitizeString(title.toString())
                + "," + sqlSanitizer.sanitizeString(fName) + "," + sqlSanitizer.sanitizeString(lName) + "," + sqlSanitizer.sanitizeString(prefName)
                + ", " + sqlSanitizer.sanitizeString(contacts.getEmail()) + "," + sqlSanitizer.sanitizeString(contacts.gethPhone())
                + "," + sqlSanitizer.sanitizeString(contacts.getwPhone()) + "," + sqlSanitizer.sanitizeString(contacts.getcPhone())
                + "," + sqlSanitizer.sanitizeString(address.getAddr1()) + "," + sqlSanitizer.sanitizeString(address.getAddr2())
                + "," + sqlSanitizer.sanitizeString(address.getSuburb()) + "," + sqlSanitizer.sanitizeString(address.getCity())
                + "," + sqlSanitizer.sanitizeString(address.getRegion()) + "," + sqlSanitizer.sanitizeString(address.getCountry())
                + "," + sqlSanitizer.sanitizeString(address.getPostCode()) + "," + (dob != null ? sqlSanitizer.sanitizeString(dob.toString()) : "NULL") + "," + sqlSanitizer.sanitizeString(ethnicity)
                + "," + sqlSanitizer.sanitizeString(gender) + "," + dodString + "," + (active ? 1 : 0) + ","
                + (locNews.toString().equalsIgnoreCase("none") ? "NULL" : sqlSanitizer.sanitizeString(locNews.toString())) + ","
                + (natNews.toString().equalsIgnoreCase("none") ? "NULL" : sqlSanitizer.sanitizeString(natNews.toString())) + ","
                + (reports.toString().equalsIgnoreCase("none") ? "NULL" : sqlSanitizer.sanitizeString(reports.toString())) + ","
                + (mags.toString().equalsIgnoreCase("none") ? "NULL" : sqlSanitizer.sanitizeString(mags.toString())) + ","
                + sqlSanitizer.sanitizeString(faith) + "," + (creationDate != null ? sqlSanitizer.sanitizeString(creationDate.toString()) : "NULL") + "," + sqlSanitizer.sanitizeString(referrer) + ");";
        int rs = s.executeUpdate(query);
        ResultSet r = s.executeQuery("SELECT LAST_INSERT_ID();");
        r.next();
        this.PID = r.getInt(1);
        r.close();
        PreparedStatement insert = con.prepareStatement("INSERT INTO comments VALUES (DEFAULT, ?, NULL, NULL,"
                + "?, " + con.getClientInfo("user") + ", ?, ?, NULL, NULL, NULL, NULL, NULL);");
        for (Comment c : comments) {
            if (c.getCID() == 0) {
                c.setPID(PID);
                insert.setInt(1, PID);
                insert.setDate(2, c.getDate());
                insert.setString(3, sqlSanitizer.sanitizeString(c.getText()));
                insert.setInt(4, c.getPrivate() ? 1 : 0);
                insert.executeUpdate();
                r = s.executeQuery("SELECT LAST_INSERT_ID();");
                r.next();
                c.setCID(r.getInt(1));
                r.close();
            }
        }
        s.close();
        for (PersonKeyword k : this.keywords) {
            if (k.getId() != 0) {
                k.updateIntoDB(con);
            } else {
                k.insertIntoDB(con);
            }
        }
        return rs == 1;
    }

    /**
     * Writes updates to person into the database will not attempt to insert a
     * new person.
     *
     * @param con --- Database connection to write to
     * @return --- True iff write succeeded
     * @throws SQLException
     */
    public boolean updateIntoDB(Connection con) throws SQLException {
        if (this.PID == 0) {
            return false;
        }
        Statement s = con.createStatement();
        String query = "UPDATE person SET Title = " + sqlSanitizer.sanitizeString(title.toString())
                + ", FName = " + sqlSanitizer.sanitizeString(fName) + ", LName = " + sqlSanitizer.sanitizeString(lName) + ", PrefName = " + sqlSanitizer.sanitizeString(prefName)
                + ", Email = " + sqlSanitizer.sanitizeString(contacts.getEmail()) + ", LandLine = " + sqlSanitizer.sanitizeString(contacts.gethPhone()) + ", WorkPhone = "
                + sqlSanitizer.sanitizeString(contacts.getwPhone()) + ", Mobile = " + sqlSanitizer.sanitizeString(contacts.getcPhone())
                + ", Address1 = " + sqlSanitizer.sanitizeString(address.getAddr1()) + ", Address2 = " + sqlSanitizer.sanitizeString(address.getAddr2())
                + ", Suburb = " + sqlSanitizer.sanitizeString(address.getSuburb()) + ", City = " + sqlSanitizer.sanitizeString(address.getCity())
                + ", Region = " + sqlSanitizer.sanitizeString(address.getRegion()) + ", Country  = " + sqlSanitizer.sanitizeString(address.getCountry())
                + ", PostCode = " + sqlSanitizer.sanitizeString(address.getPostCode()) + ", DateOfBirth = " + sqlSanitizer.sanitizeString(dob.toString())
                + ", Ethnicity = " + sqlSanitizer.sanitizeString(ethnicity) + ", Gender = " + sqlSanitizer.sanitizeString(gender)
                + ", DateOFDeath = " + (dod != null ? dod.toString() : "NULL") + ", isActive = " + (active ? 1 : 0)
                + ", requestNews = " + sqlSanitizer.sanitizeString(locNews.toString()) + ", requestNatNews = " + sqlSanitizer.sanitizeString(natNews.toString())
                + ", requestReports = " + sqlSanitizer.sanitizeString(reports.toString()) + ", requestMags = " + sqlSanitizer.sanitizeString(mags.toString())
                + ", Faith = " + sqlSanitizer.sanitizeString(faith) + ", CreationDate = " + (creationDate != null ? creationDate.toString() : "NULL")
                + ", Referrer = " + sqlSanitizer.sanitizeString(referrer) + " WHERE PID=" + PID + ";";
        int rs = s.executeUpdate(query);
        PreparedStatement insert = con.prepareStatement("INSERT INTO comments VALUES (DEFAULT, ?, NULL, NULL,"
                + "?, " + con.getClientInfo("user") + ", ?, ?, NULL, NULL, NULL, NULL, NULL);");
        ResultSet r;
        for (Comment c : comments) {
            if (c.getCID() == 0) {
                insert.setInt(1, PID);
                insert.setDate(2, c.getDate());
                insert.setString(3, sqlSanitizer.sanitizeString(c.getText()));
                insert.setInt(4, c.getPrivate() ? 1 : 0);
                insert.executeUpdate();
                r = s.executeQuery("SELECT LAST_INSERT_ID();");
                r.next();
                c.setCID(r.getInt(1));
                r.close();
            }
        }
        s.close();
        for (PersonKeyword k : this.keywords) {
            if (k.getId() != 0) {
                k.updateIntoDB(con);
            } else {
                k.insertIntoDB(con);
            }
        }
        PreparedStatement state = con.prepareStatement("DELETE FROM personkeyword WHERE ID=?");
        for (PersonKeyword k : this.removedKeywords) {
            state.setInt(1, k.getId());
            state.executeUpdate();
        }
        state.close();
        return rs == 1;
    }

    /**
     * Used to generate an SQL statement to select from People all people which
     * match the fields set in this person Must not be called on an empty
     * Person.
     *
     * @return String --- SQL Query to select people based on set fields.
     */
    public String createSelectQuery() {
        SelectQuery squiggle = new SelectQuery();
        Table person = new Table("person");
        Table personKeyword = new Table("personkeyword");
        Table Keyword = new Table("keyword");
        squiggle.addColumn(person, "*"); //add the lot
        if (keywords.size() > 0) {
            squiggle.addJoin(person, "PID", personKeyword, "PID");
            squiggle.addJoin(personKeyword, "PID", Keyword, "ID");
            squiggle.addColumn(personKeyword, "level");
            squiggle.addColumn(Keyword, "name");
        }
        for (PersonKeyword k : keywords) {
            squiggle.addCriteria(new MatchCriteria(Keyword, "name", MatchCriteria.EQUALS, k.getWord().getName()));
            if (k.getLevel() > -1 && k.getLevel() < 4) {
                squiggle.addCriteria(new MatchCriteria(personKeyword, "level", MatchCriteria.EQUALS, k.getLevel()));
            }
        }

        if (this.title != Title.NONE) {
            squiggle.addCriteria(new MatchCriteria(person, "Title", MatchCriteria.EQUALS, title.toString()));
        }
        if (this.fName != null && !this.fName.equals("")) {
            squiggle.addCriteria(new MatchCriteria(person, "FName", MatchCriteria.EQUALS, fName));
        }
        if (this.lName != null && !this.lName.equals("")) {
            squiggle.addCriteria(new MatchCriteria(person, "LName", MatchCriteria.EQUALS, lName));
        }
        if (this.prefName != null && !this.prefName.equals("")) {
            squiggle.addCriteria(new MatchCriteria(person, "PrefName", MatchCriteria.EQUALS, prefName));
        }
        if (this.contacts != null) {
            if (this.contacts.getEmail() != null && !this.contacts.getEmail().equals("NULL")) {
                squiggle.addCriteria(new MatchCriteria(person, "Email", MatchCriteria.EQUALS, this.contacts.getEmail()));
            }
            if (this.contacts.gethPhone() != null && !this.contacts.gethPhone().equals("NULL")) {
                squiggle.addCriteria(new MatchCriteria(person, "LandLine", MatchCriteria.EQUALS, this.contacts.gethPhone()));
            }
            if (this.contacts.getwPhone() != null && !this.contacts.getwPhone().equals("NULL")) {
                squiggle.addCriteria(new MatchCriteria(person, "WorkPhone", MatchCriteria.EQUALS, this.contacts.getwPhone()));
            }
            if (this.contacts.getcPhone() != null && !this.contacts.getcPhone().equals("NULL")) {
                squiggle.addCriteria(new MatchCriteria(person, "Mobile", MatchCriteria.EQUALS, this.contacts.getcPhone()));
            }
        }
        if (this.address != null) {
            if (this.address.getAddr1() != null && !this.address.getAddr1().equals("NULL")) {
                squiggle.addCriteria(new MatchCriteria(person, "Address1", MatchCriteria.EQUALS, this.address.getAddr1()));
            }
            if (this.address.getAddr2() != null && !this.address.getAddr2().equals("NULL")) {
                squiggle.addCriteria(new MatchCriteria(person, "Address2", MatchCriteria.EQUALS, this.address.getAddr2()));
            }
            if (this.address.getSuburb() != null && !this.address.getSuburb().equals("NULL")) {
                squiggle.addCriteria(new MatchCriteria(person, "Suburb", MatchCriteria.EQUALS, this.address.getSuburb()));
            }
            if (this.address.getCity() != null && !this.address.getCity().equals("NULL")) {
                squiggle.addCriteria(new MatchCriteria(person, "City", MatchCriteria.EQUALS, this.address.getCity()));
            }
            if (this.address.getRegion() != null && !this.address.getRegion().equals("NULL")) {
                squiggle.addCriteria(new MatchCriteria(person, "Region", MatchCriteria.EQUALS, this.address.getRegion()));
            }
            if (this.address.getCountry() != null && !this.address.getCountry().equals("NULL")) {
                squiggle.addCriteria(new MatchCriteria(person, "Country", MatchCriteria.EQUALS, this.address.getCountry()));
            }
            if (this.address.getPostCode() != null && !this.address.getPostCode().equals("NULL")) {
                squiggle.addCriteria(new MatchCriteria(person, "PostCode", MatchCriteria.EQUALS, this.address.getPostCode()));
            }
        }
        if (this.dob != null) {
            squiggle.addCriteria(new MatchCriteria(person, "DateOfBirth", MatchCriteria.EQUALS, this.dob));
        }
        if (this.ethnicity != null) {
            squiggle.addCriteria(new MatchCriteria(person, "Ethnicity", MatchCriteria.EQUALS, ethnicity));
        }
        if (this.gender != null) {
            squiggle.addCriteria(new MatchCriteria(person, "Gender", MatchCriteria.EQUALS, gender));
        }
        if (this.dod != null) {
            squiggle.addCriteria(new MatchCriteria(person, "DateOfDeath", MatchCriteria.EQUALS, this.dod));
        }
        if (this.locNews != null) {
            squiggle.addCriteria(new MatchCriteria(person, "requestNews", MatchCriteria.EQUALS, this.locNews.toString()));
        }
        if (this.natNews != null) {
            squiggle.addCriteria(new MatchCriteria(person, "requestNatNews", MatchCriteria.EQUALS, this.natNews.toString()));
        }
        if (this.reports != null) {
            squiggle.addCriteria(new MatchCriteria(person, "requestReports", MatchCriteria.EQUALS, this.reports.toString()));
        }
        if (this.mags != null) {
            squiggle.addCriteria(new MatchCriteria(person, "requestMags", MatchCriteria.EQUALS, this.mags.toString()));
        }
        if (this.faith != null) {
            squiggle.addCriteria(new MatchCriteria(person, "Faith", MatchCriteria.EQUALS, faith));
        }
        if (this.creationDate != null) {
            squiggle.addCriteria(new MatchCriteria(person, "CreationDate", MatchCriteria.EQUALS, this.creationDate));
        }
        if (this.referrer != null) {
            squiggle.addCriteria(new MatchCriteria(person, "Referrer", MatchCriteria.EQUALS, referrer));
        }
        return squiggle.toString();
    }

    public static commType resolveCommType(String str) {
        if (str == null) {
            return null;
        } else if ("Both".equals(str)) {
            return commType.Both;
        } else if ("Hard".equals(str)) {
            return commType.Hard;
        } else if ("Electronic".equals(str)) {
            return commType.Electronic;
        } else {
            throw new IllegalArgumentException();
        }
    }

    public void addKeyword(PersonKeyword keyword) {
        this.keywords.add(keyword);
    }
}
