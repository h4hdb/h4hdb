package h4hdb.data;

import com.truemesh.squiggle.SelectQuery;
import com.truemesh.squiggle.Table;
import com.truemesh.squiggle.criteria.MatchCriteria;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a Team in the database.
 *
 * @author Leliel Trethowen
 */
public class Team {

    private int TID;
    private String name;
    private Contacts contacts;
    private Address address;
    private List<Comment> comments;

    /**
     * Create a team with specified values
     *
     * @param TID --- int Team ID, primary key
     * @param name --- String team name
     * @param contacts --- Contacts contact details for the team
     * @param address --- Address of the team
     * @param comments --- List of comments associated with the team
     */
    public Team(int TID, String name, Contacts contacts, Address address, List<Comment> comments) {
        this.TID = TID;
        this.name = name;
        this.contacts = contacts;
        this.address = address;
        this.comments = comments;
    }

    /**
     * Creates an empty team
     */
    public Team() {
        this.comments = new ArrayList<Comment>();
    }

    public int getTID() {
        return TID;
    }

    public void setTID(int TID) {
        this.TID = TID;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public Contacts getContacts() {
        return contacts;
    }

    public void setContacts(Contacts contacts) {
        this.contacts = contacts;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Team other = (Team) obj;
        if (this.TID != other.TID) {
            return false;
        }
        if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
            return false;
        }
        if (this.contacts != other.contacts && (this.contacts == null || !this.contacts.equals(other.contacts))) {
            return false;
        }
        if (this.address != other.address && (this.address == null || !this.address.equals(other.address))) {
            return false;
        }
        if (this.comments != other.comments && (this.comments == null || !this.comments.equals(other.comments))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + this.TID;
        hash = 67 * hash + (this.name != null ? this.name.hashCode() : 0);
        hash = 67 * hash + (this.contacts != null ? this.contacts.hashCode() : 0);
        hash = 67 * hash + (this.address != null ? this.address.hashCode() : 0);
        hash = 67 * hash + (this.comments != null ? this.comments.hashCode() : 0);
        return hash;
    }

    /**
     * Writes a new Team into the database. Will not attempt to insert an
     * existing Team.
     *
     * @param con --- Database connection to write to
     * @return --- True iff write succeeded
     * @throws SQLException
     */
    public boolean insertIntoDB(Connection con) throws SQLException {
        if (this.TID != 0) {
            return false;
        } else {
            String query = "INSERT INTO team VALUES (DEFAULT, " + sqlSanitizer.sanitizeString(name)
                    + "," + sqlSanitizer.sanitizeString(contacts.getEmail()) + "," + sqlSanitizer.sanitizeString(contacts.gethPhone())
                    + "," + sqlSanitizer.sanitizeString(contacts.getwPhone()) + "," + sqlSanitizer.sanitizeString(contacts.getcPhone())
                    + "," + sqlSanitizer.sanitizeString(address.getAddr1()) + "," + sqlSanitizer.sanitizeString(address.getAddr2())
                    + "," + sqlSanitizer.sanitizeString(address.getSuburb()) + "," + sqlSanitizer.sanitizeString(address.getCity())
                    + "," + sqlSanitizer.sanitizeString(address.getRegion()) + "," + sqlSanitizer.sanitizeString(address.getCountry())
                    + "," + sqlSanitizer.sanitizeString(address.getPostCode()) + ");";
            Statement s = con.createStatement();
            int result = s.executeUpdate(query);
            if (result == 1) {
                ResultSet rs = s.executeQuery("SELECT LAST_INSERT_ID();");
                rs.next();
                this.TID = rs.getInt(1);
                rs.close();
                s.close();
                PreparedStatement insert = con.prepareStatement("INSERT INTO comments VALUES (DEFAULT, NULL, NULL, ?,"
                        + "?, " + con.getClientInfo("user") + ", ?, ?, NULL, NULL, NULL, NULL, NULL);");
                ResultSet r;
                for (Comment c : comments) {
                    if (c.getCID() == 0) {
                        c.setTID(TID);
                        insert.setInt(1, TID);
                        insert.setDate(2, c.getDate());
                        insert.setString(3, sqlSanitizer.sanitizeString(c.getText()));
                        insert.setInt(4, c.getPrivate() ? 1 : 0);
                        insert.executeUpdate();
                        r = s.executeQuery("SELECT LAST_INSERT_ID();");
                        r.next();
                        c.setCID(r.getInt(1));
                        r.close();
                    }
                }
                return true;
            } else {
                s.close();
                return false;
            }
        }
    }

    /**
     * Writes updates to a team into the database. will not attempt to insert a
     * new Team.
     *
     * @param con --- Database connection to write to
     * @return --- True iff write succeeded
     * @throws SQLException
     */
    public boolean updateIntoDB(Connection con) throws SQLException {
        if (this.TID == 0) {
            return false;
        } else {
            String query = "UPDATE team SET Name=" + sqlSanitizer.sanitizeString(this.name)
                    + ", Email=" + sqlSanitizer.sanitizeString(contacts.getEmail()) + ", LandLine=" + sqlSanitizer.sanitizeString(contacts.gethPhone())
                    + ", WorkPhone=" + sqlSanitizer.sanitizeString(contacts.getwPhone()) + ", Mobile=" + sqlSanitizer.sanitizeString(contacts.getcPhone())
                    + ", Address1=" + sqlSanitizer.sanitizeString(address.getAddr1()) + ", Address2=" + sqlSanitizer.sanitizeString(address.getAddr2())
                    + ", Suburb=" + sqlSanitizer.sanitizeString(address.getSuburb()) + ", City=" + sqlSanitizer.sanitizeString(address.getCity())
                    + ", Region=" + sqlSanitizer.sanitizeString(address.getRegion()) + ", Country=" + sqlSanitizer.sanitizeString(address.getCountry())
                    + ", PostCode=" + sqlSanitizer.sanitizeString(address.getPostCode()) + " WHERE TID=" + this.TID + ";";

            Statement s = con.createStatement();
            int result = s.executeUpdate(query);
            if (result == 1) {
                ResultSet rs;
                PreparedStatement insert = con.prepareStatement("INSERT INTO comments VALUES (DEFAULT, NULL, NULL, ?,"
                        + "?, " + con.getClientInfo("user") + ", ?, ?, NULL, NULL, NULL, NULL, NULL);");
                ResultSet r;
                for (Comment c : comments) {
                    if (c.getCID() == 0) {
                        insert.setInt(1, TID);
                        insert.setDate(2, c.getDate());
                        insert.setString(3, sqlSanitizer.sanitizeString(c.getText()));
                        insert.setInt(4, c.getPrivate() ? 1 : 0);
                        insert.executeUpdate();
                        r = s.executeQuery("SELECT LAST_INSERT_ID();");
                        r.next();
                        c.setCID(r.getInt(1));
                        r.close();
                    }
                }
                return true;
            } else {
                s.close();
                return false;
            }

        }
    }

    /**
     * Creates an SQL Select statement based on fields. uses all fields that are
     * set to find all teams matching.
     *
     * @return --- String SQL statement
     */
    public String generateSelectQuery() {
        SelectQuery query = new SelectQuery();
        Table team = new Table("team");
        query.addColumn(team, "*");
        if (this.name != null) {
            query.addCriteria(new MatchCriteria(team, "Name", MatchCriteria.EQUALS, this.name));
        }
        if (this.contacts != null) {
            if (this.contacts.getEmail() != null && !this.contacts.getEmail().equals("NULL")) {
                query.addCriteria(new MatchCriteria(team, "Email", MatchCriteria.EQUALS, this.contacts.getEmail()));

            }
            if (this.contacts.gethPhone() != null && !this.contacts.gethPhone().equals("NULL")) {
                query.addCriteria(new MatchCriteria(team, "LandLine", MatchCriteria.EQUALS, this.contacts.gethPhone()));
            }
            if (this.contacts.getwPhone() != null && !this.contacts.getwPhone().equals("NULL")) {
                query.addCriteria(new MatchCriteria(team, "WorkPhone", MatchCriteria.EQUALS, this.contacts.getwPhone()));
            }
            if (this.contacts.getcPhone() != null && !this.contacts.getcPhone().equals("NULL")) {
                query.addCriteria(new MatchCriteria(team, "Mobile", MatchCriteria.EQUALS, this.contacts.getcPhone()));
            }
        }
        if (this.address != null) {
            if (this.address.getAddr1() != null && !this.address.getAddr1().equals("NULL")) {
                query.addCriteria(new MatchCriteria(team, "Address1", MatchCriteria.EQUALS, this.address.getAddr1()));
            }
            if (this.address.getAddr2() != null && !this.address.getAddr2().equals("NULL")) {
                query.addCriteria(new MatchCriteria(team, "Address2", MatchCriteria.EQUALS, this.address.getAddr2()));
            }
            if (this.address.getSuburb() != null && !this.address.getSuburb().equals("NULL")) {
                query.addCriteria(new MatchCriteria(team, "Suburb", MatchCriteria.EQUALS, this.address.getSuburb()));
            }
            if (this.address.getCity() != null && !this.address.getCity().equals("NULL")) {
                query.addCriteria(new MatchCriteria(team, "City", MatchCriteria.EQUALS, this.address.getCity()));
            }
            if (this.address.getRegion() != null && !this.address.getRegion().equals("NULL")) {
                query.addCriteria(new MatchCriteria(team, "Region", MatchCriteria.EQUALS, this.address.getRegion()));
            }
            if (this.address.getCountry() != null && !this.address.getCountry().equals("NULL")) {
                query.addCriteria(new MatchCriteria(team, "Country", MatchCriteria.EQUALS, this.address.getCountry()));
            }
            if (this.address.getPostCode() != null && !this.address.getPostCode().equals("NULL")) {
                query.addCriteria(new MatchCriteria(team, "PostCode", MatchCriteria.EQUALS, this.address.getPostCode()));
            }
        }
        return query.toString();
    }

    public void addComment(Comment c) {
        this.comments.add(c);
    }

    public boolean isEmpty() {
        if (this.name != null) {
            return false;
        }
        if (!this.address.isEmpty()) {
            return false;
        }
        if (!this.contacts.isEmpty()) {
            return false;
        }
        return true;
    }
}
