package h4hdb.data;

import h4hdb.data.sqlSanitizer;

/**
 * Represents an Address as stored in the database
 *
 * @author Leliel Trethowen
 */
public class Address {

    public static int LengthOfAddr1 = 200;
    public static int LengthOfAddr2 = 200;
    public static int LengthOfSuburb = 200;
    public static int LengthOfCity = 200;
    public static int LengthOfRegion = 200;
    public static int LengthOfCountry = 200;
    public static int LengthOfPostCode= 10;

    @Override
    public String toString() {
        return addr1 + ", " + addr2 + ", " + suburb + ",  " + city + ", " + region + ", " + country + ", " + postCode;
    }
    
    private String addr1;
    private String addr2;
    private String suburb;
    private String city;
    private String region;
    private String country;
    private String postCode;

    /** Create an address from strings
     * predominantly used in extracting from database.
     * 
     * @param addr1 --- String address line 1
     * @param addr2 --- String address line 2
     * @param suburb --- String suburb
     * @param city ---- String city
     * @param region --- String region
     * @param country --- String country
     * @param postcode --- String postcode
     */
    public Address(String addr1, String addr2, String suburb, String city, String region, String country, String postcode) {
        this.addr1 = addr1;
        this.addr2 = addr2;
        this.suburb = suburb;
        this.city = city;
        this.region = region;
        this.country = country;
        this.postCode = postcode;
    }

    /**Constructor for Addresses
     * Creates an empty address, with no values set.
     * note - getters will return null until set.
     */
    public Address() {
        super();
    }

    /**Checks that the required address fields have been set
     * addr1, city, country, postcode.
     * 
     * @return --- True iff required address fields are filled in
     */
    public boolean isValid() {
        if (addr1 == null || addr1.equals("")) {
            return false;
        }
        if (city == null || city.equals("")) {
            return false;
        }
        if (country == null || country.equals("")) {
            return false;
        }
        if (postCode == null || postCode.equals("")) {
            return false;
        }
        return true;
    }

    public String getAddr1() {
        return addr1;
    }

    public String getAddr2() {
        return addr2;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public String getPostCode() {
        return postCode;
    }

    public String getSuburb() {
        return suburb;
    }

    public String getRegion() {
        return region;
    }

    public void setAddr1(String addr1) {
        if (addr1 == null || addr1.equals("")) {
            return;
        }
        this.addr1 = addr1;
    }

    public void setAddr2(String addr2) {
        this.addr2 = addr2;
    }

    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setCity(String city) {
        if (city == null || city.equals("")) {
            return;
        }
        this.city = city;
    }

    public void setCountry(String country) {
        if (country == null || country.equals("")) {
            return;
        }
        this.country = country;
    }

    public void setPostCode(String postCode) {
        if (postCode == null || postCode.equals("")) {
            return;
        }
        this.postCode = postCode;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (!o.getClass().equals(this.getClass())) {
            return false;
        }
        Address other = (Address) o;
        if ((other.addr1 != null && !other.addr1.equals(addr1)) || (addr1 != null && !addr1.equals(other.addr1))) {
            return false;
        }
        if ((other.addr2 != null && !other.addr2.equals(addr2)) || (addr2 != null && !addr2.equals(other.addr2))) {
            return false;
        }
        if ((other.suburb != null && !other.suburb.equals(suburb)) || (suburb != null && !suburb.equals(other.suburb))) {
            return false;
        }
        if ((other.city != null && !other.city.equals(city)) || (city != null && !city.equals(other.city))) {
            return false;
        }
        if ((other.country != null && !other.country.equals(country)) || (country != null && !country.equals(other.country))) {
            return false;
        }
        if ((other.postCode != null && !other.postCode.equals(postCode)) || (postCode != null && !postCode.equals(other.postCode))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    /**Checks if any fields have been set
     * Returns true iff all fields are unset.
     * 
     * @return --- True iff no lines have been set
     */
    boolean isEmpty() {
        if (this.addr1 != null) {
            return false;
        }
        if (this.addr2 != null) {
            return false;
        }
        if (this.suburb != null) {
            return false;
        }
        if (this.city != null) {
            return false;
        }
        if (this.region != null) {
            return false;
        }
        if (this.country != null) {
            return false;
        }
        if (this.postCode != null) {
            return false;
        }
        return true;
    }
}
