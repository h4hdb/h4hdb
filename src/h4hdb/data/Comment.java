package h4hdb.data;

import java.sql.Connection;
import java.sql.Date;

/**Represents a comment in the database
 *Comments may be attached to either a person, team or organisation
 * only one of PID, TID, OID can be > 0/.
 * @author Leliel Trethowen
 */
public class Comment {

    private int CID;
    private int PID;
    private int OID;
    private int TID;
    private String text;
    private Date date;
    private String commenter;
    private boolean priv;

    public Comment() {
    }

    /**Constructor taking all fields
     * used to build a comment from database
     * 
     * @param CID --- primary key >0
     * @param PID --- int Person id
     * @param OID --- int Org id
     * @param TID --- int Team id
     * @param text --- String text of the comment, may contain newlines.
     * @param date --- Date the comment was made
     * @param commenter --- String the user that made the comment
     * @param priv --- True if the comment is private,
     */
    public Comment(int CID, int PID, int OID, int TID, String text, Date date, String commenter, boolean priv) {
        this.CID = CID;
        this.PID = PID;
        this.OID = OID;
        this.TID = TID;
        this.text = text;
        this.date = date;
        this.commenter = commenter;
        this.priv = priv;
    }

    /**Create a new comment prior to association with an entity
     * 
     * @param text --- String text of the comment, may contain newlines.
     * @param date --- Date the comment was made
     * @param commenter --- String the user that made the comment
     * @param priv --- True if the comment is private,
     */
    public Comment(String text, Date date, String commenter, boolean priv) {
        this.text = text;
        this.date = date;
        this.commenter = commenter;
        this.priv = priv;
    }
    
     /**Create a new comment prior to association with an entity or commenter
     * 
     * @param text --- String text of the comment, may contain newlines.
     * @param date --- Date the comment was made
     * @param priv --- True if the comment is private,
     */
    public Comment(String text, Date date, boolean priv) {
        this.text = text;
        this.date = date;
        this.priv = priv;
    }

    public int getCID() {
        return CID;
    }

    public void setCID(int CID) {
        this.CID = CID;
    }

    public int getOID() {
        return OID;
    }

    public void setOID(int OID) {
        this.OID = OID;
    }

    public int getPID() {
        return PID;
    }

    public void setPID(int PID) {
        this.PID = PID;
    }

    public int getTID() {
        return TID;
    }

    public void setTID(int TID) {
        this.TID = TID;
    }

    public String getCommenter() {
        return commenter;
    }

    public void setCommenter(String commenter) {
        this.commenter = commenter;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean getPrivate() {
        return this.priv;
    }

    public void setPrivate(boolean priv) {
        this.priv = priv;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Comment other = (Comment) obj;
        if (this.CID != other.CID) {
            return false;
        }
        if (this.PID != other.PID) {
            return false;
        }
        if (this.OID != other.OID) {
            return false;
        }
        if (this.TID != other.TID) {
            return false;
        }
        if ((this.text == null) ? (other.text != null) : !this.text.equals(other.text)) {
            return false;
        }
        if ((this.date == null) ? (other.date != null) : !this.date.equals(other.date)) {
            return false;
        }
        if ((this.commenter == null) ? (other.commenter != null) : !this.commenter.equals(other.commenter)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + this.CID;
        hash = 29 * hash + this.PID;
        hash = 29 * hash + this.OID;
        hash = 29 * hash + this.TID;
        hash = 29 * hash + (this.text != null ? this.text.hashCode() : 0);
        hash = 29 * hash + (this.date != null ? this.date.hashCode() : 0);
        hash = 29 * hash + (this.commenter != null ? this.commenter.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "\n----Comment Made on :" + date.toString() + "----\n" + text + "\n";
    }
}
