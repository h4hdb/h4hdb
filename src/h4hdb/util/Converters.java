/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package h4hdb.util;

/**
 *
 * @author Mehendra
 */
public class Converters {
    public static int convertToInt(String stringToConvert)
    {
        if(stringToConvert != null && !stringToConvert.isEmpty() && Validators.isNumeric(stringToConvert))
        {
            if(stringToConvert.contains("."))
            {
                String wholeNumber = stringToConvert.split("\\.")[0];
                String fraction = "";
                try{
                    fraction = stringToConvert.split("\\.")[1];
                }
                catch(Throwable ex){
                    fraction = "00";
                }
                
                if(fraction.length() == 1){
                    fraction = fraction + "0";
                }
                return Integer.parseInt(wholeNumber + fraction);
            }
            else{
            return (int)Float.parseFloat(stringToConvert + "00") ;
            }
        }
        return 0; //Instead failing to convert return 0
    }
    
    public static String addtwoDecimalPlaces(int intToAddDecimal)
    {
        String stringToConvert = String.valueOf(intToAddDecimal);
        if(stringToConvert.length() > 2)
            return new StringBuffer(stringToConvert).insert(stringToConvert.length() - 2, ".").toString();
        
        return stringToConvert;
    }
}
