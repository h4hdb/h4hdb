/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package h4hdb.util;

/**
 *  Validation utilities class, all the members are intended to be static.
 *  @author Mehendra Munasinghe
 * 
 */
public class Validators {
    
    /**
     * Validates the field length
     * @param fieldValue indicates string to validate
     * @param length, maximum length of the field
     * @return true if the length is less than the maximum length, otherwise false
     */
    public static boolean isCorrectFieldLength(String fieldValue,int length){

        if(fieldValue != null && !fieldValue.isEmpty()){
            return fieldValue.length() <= length;
        }
        return true;
    }
    
    /**
     *  Validates a string to test if its numeric.
     *  Valid Examples: 12.34, 5364.00, 34563, 567.0, .45, 0.56
     *  Invalid Examples: $456.45, 567.
     *  @param numericString, the string to be validated
     *  @return true if the string is a valid numeric, otherwise false
     */
    public static boolean isNumeric(String numericString)
    {
        //Not implimented, good candidate for TDD, unit tests are already written
        if(numericString != null && !numericString.isEmpty()){
            //We do the numeric validation using regular expressions
            return numericString.matches("\\d{0,15}(\\.\\d{0,2}){0,1}");
        }
        return true;//Empty is valid
    }
}