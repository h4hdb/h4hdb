/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package h4hdb.guiComponents.util;

import h4hdb.data.Person;
import h4hdb.data.tableModels.PersonTableModel;
import java.beans.Beans;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.RowFilter;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author lelitu
 */
public class AttendeePicker extends javax.swing.JPanel {

    private Connection con = null;
    private TableRowSorter<PersonTableModel> sorter;

    /**
     * Creates new form AttendeePicker
     */
    public AttendeePicker() {
        initComponents();
    }

    public void setConnection(Connection con) {
        this.con = con;
        ((PersonTableModel)this.peopleTable.getModel()).replaceValues(this.getPeople());
        this.suburbFilter.setModel(this.getSuburbModel());
        this.cityFilter.setModel(this.getCityModel());
        this.regionFilter.setModel(this.getRegionModel());
    }

    public List<Integer> getSelectedPID() {
        int[] selections = this.peopleTable.getSelectedRows();
        int idx;
        PersonTableModel m = (PersonTableModel) this.peopleTable.getModel();
        List<Integer> result = new ArrayList<Integer>();
        for (int i = 0; i < selections.length; i++) {
            idx = this.peopleTable.convertRowIndexToModel(selections[i]);
            result.add(m.getUnderlyingRow(idx).getPID());
        }
        return result;
    }

    public void clear() {
        this.suburbFilter.setSelectedIndex(0);
        this.cityFilter.setSelectedIndex(0);
        this.regionFilter.setSelectedIndex(0);
        this.peopleTable.clearSelection();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        PersonTableModel model = new PersonTableModel(new ArrayList<Person>());
        sorter = new TableRowSorter<PersonTableModel>(model);
        peopleTable = new javax.swing.JTable(model);
        jLabel1 = new javax.swing.JLabel();
        suburbFilter = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        cityFilter = new javax.swing.JComboBox();
        regionFilter = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();

        peopleTable.setRowSorter(sorter);
        this.tableFilter(null);
        jScrollPane1.setViewportView(peopleTable);
        peopleTable.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

        jLabel1.setText("Suburb");

        suburbFilter.setModel(this.getSuburbModel());
        suburbFilter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                suburbFilterActionPerformed(evt);
            }
        });

        jLabel2.setText("City");

        cityFilter.setModel(this.getCityModel());
        cityFilter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cityFilterActionPerformed(evt);
            }
        });

        regionFilter.setModel(this.getRegionModel());
        regionFilter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                regionFilterActionPerformed(evt);
            }
        });

        jLabel3.setText("Region");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 374, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(cityFilter, javax.swing.GroupLayout.Alignment.TRAILING, 0, 114, Short.MAX_VALUE)
                    .addComponent(suburbFilter, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(regionFilter, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(suburbFilter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(cityFilter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(regionFilter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 195, Short.MAX_VALUE)))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void suburbFilterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_suburbFilterActionPerformed
        if (this.suburbFilter.getSelectedIndex() == 0)
            return;
        this.cityFilter.setSelectedIndex(0);
        this.regionFilter.setSelectedIndex(0);
        this.tableFilter(suburbFilter);
    }//GEN-LAST:event_suburbFilterActionPerformed

    private void cityFilterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cityFilterActionPerformed
        if (this.cityFilter.getSelectedIndex() == 0)
            return;
        this.suburbFilter.setSelectedIndex(0);
        this.regionFilter.setSelectedIndex(0);
        this.tableFilter(cityFilter);
    }//GEN-LAST:event_cityFilterActionPerformed

    private void regionFilterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_regionFilterActionPerformed
        if (this.regionFilter.getSelectedIndex() == 0)
            return;
        this.suburbFilter.setSelectedIndex(0);
        this.cityFilter.setSelectedIndex(0);
        this.tableFilter(regionFilter);
    }//GEN-LAST:event_regionFilterActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox cityFilter;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable peopleTable;
    private javax.swing.JComboBox regionFilter;
    private javax.swing.JComboBox suburbFilter;
    // End of variables declaration//GEN-END:variables

    private TableModel getTableModel() {
        return new PersonTableModel(this.getPeople());
    }

    private ComboBoxModel getSuburbModel() {
        DefaultComboBoxModel model = new DefaultComboBoxModel();
        model.addElement("Please Select");
        if (!Beans.isDesignTime() && con != null) {
            try {
                Statement s = con.createStatement();
                ResultSet rs = s.executeQuery("SELECT DISTINCT Suburb from Person;");
                while (rs.next()) {
                    model.addElement(rs.getString("Suburb"));
                }
            } catch (SQLException e) {
                // work out a good way to handle this.
                // dying isn't elegant, but if the db is MIA, not much else to do.
            }
        }
        return model;
    }

    private ComboBoxModel getCityModel() {
        DefaultComboBoxModel model = new DefaultComboBoxModel();
        model.addElement("Please Select");
        if (!Beans.isDesignTime() && con != null) {
            try {
                Statement s = con.createStatement();
                ResultSet rs = s.executeQuery("SELECT DISTINCT City from Person;");
                while (rs.next()) {
                    model.addElement(rs.getString("City"));
                }
            } catch (SQLException e) {
                // work out a good way to handle this.
                // dying isn't elegant, but if the db is MIA, not much else to do.
            }
        }
        return model;
    }

    private ComboBoxModel getRegionModel() {
        DefaultComboBoxModel model = new DefaultComboBoxModel();
        model.addElement("Please Select");
        if (!Beans.isDesignTime() && con != null) {
            try {
                Statement s = con.createStatement();
                ResultSet rs = s.executeQuery("SELECT DISTINCT Region from Person;");
                while (rs.next()) {
                    model.addElement(rs.getString("Region"));
                }
            } catch (SQLException e) {
                // work out a good way to handle this.
                // dying isn't elegant, but if the db is MIA, not much else to do.
            }
        }
        return model;
    }

    private void tableFilter(JComboBox item) {
        RowFilter<PersonTableModel, Integer> rf;
        if (item == null) {
            this.sorter.setRowFilter(null);
            return;
        }

        //If current expression doesn't parse, don't update.
        try {
            rf = RowFilter.regexFilter("^" + ((String) item.getSelectedItem()) + "$");
        } catch (java.util.regex.PatternSyntaxException e) {
            return;
        }
        this.sorter.setRowFilter(rf);
    }

    private List<Person> getPeople() {
        List<Person> outer = new ArrayList<Person>();
        Person p;
        if (con != null && !Beans.isDesignTime()) {
            try {
                Statement s = con.createStatement();
                ResultSet rs = s.executeQuery("SELECT pid, IFNULL(PrefName,CONCAT(FName, ' ', LName)) as name, "
                        + "Suburb, City, Region FROM Person;");
                while (rs.next()) {
                    p = new Person();
                    p.setPID(rs.getInt(1));
                    p.setfName(rs.getString(2));
                    p.getAddress().setSuburb(rs.getString(3));
                    p.getAddress().setCity(rs.getString(4));
                    p.getAddress().setRegion(rs.getString(5));
                    outer.add(p);
                }
            } catch (SQLException ex) {
                //TODO How do we die nicely here?
            }
        }
        return outer;
    }
}
