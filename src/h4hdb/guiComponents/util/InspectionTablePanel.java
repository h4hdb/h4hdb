package h4hdb.guiComponents.util;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * InspectionTablePanel.java
 *
 * Created on 23/09/2012, 2:50:39 PM
 */


import javax.swing.JTable;
import h4hdb.data.tableModels.InspectionTableModel;
import javax.swing.JScrollPane;
/**
 *
 * @author Fintan
 */
public class InspectionTablePanel extends javax.swing.JPanel {

    private InspectionTableModel model;
    private JTable table;
    private String tableName;
    
    public InspectionTablePanel(String name, String[][] tableModel){
        
        tableName = name;
        JScrollPane pane = new JScrollPane();
        model = new InspectionTableModel(name, tableModel);
        table = new JTable(model);
        table.getTableHeader().setResizingAllowed(false);
        table.getTableHeader().setReorderingAllowed(false);
        table.getColumnModel().getColumn(0).setMinWidth(160);
        table.getColumnModel().getColumn(0).setMaxWidth(160);
        pane.setViewportView(table);
        
         javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pane, javax.swing.GroupLayout.DEFAULT_SIZE, 831, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(pane, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(80, Short.MAX_VALUE))
        );
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
    public void setData(int row, String data){
        model.setValueAt(data, row, 1);
    }
    
    public String getNotes(){
        return model.getTableNotes();
    }
    
    public String tableName(){
        return tableName;
    }
    
    public InspectionTableModel getTableModel(){
        return model;
    }


}
