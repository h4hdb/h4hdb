/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package h4hdb.guiComponents.util;
import h4hdb.data.*;
import h4hdb.guiComponents.ConnectedComponent;
import h4hdb.guiComponents.addPersonPanel;
import h4hdb.guiComponents.dialogs.CommentDialog;
import h4hdb.mainFrame;
import java.awt.Component;
import java.awt.Frame;
import java.beans.Beans;
import java.sql.*;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 *
 * @author lelitu
 */
public class DemographicsPanel extends javax.swing.JPanel implements ConnectedComponent {

    private Connection con;
    private PersonPanel personPanel;
    private TextFieldListener listener = new TextFieldListener();

    /**
     * Creates new form DemographicsPanel
     */
    public DemographicsPanel() {
        initComponents();
    }

    public DemographicsPanel(PersonPanel p) {
        this.personPanel = p;
        initComponents();
        addTextListeners();
    }

    public void setPerson() {
        String temp = this.personPanel.getPerson().getTitle().toString();
        this.title.setSelectedItem(temp);
        this.fName.setText(this.personPanel.getPerson().getfName());
        this.lName.setText(this.personPanel.getPerson().getlName());
        this.prefName.setText(this.personPanel.getPerson().getPrefName());
        this.address.setAddress(this.personPanel.getPerson().getAddress());
        this.contact.setContact(this.personPanel.getPerson().getContacts());
        this.gender.setSelectedItem(this.personPanel.getPerson().getGender());
        this.faith.setSelectedItem(this.personPanel.getPerson().getFaith());
        this.ethnicity.setSelectedItem(this.personPanel.getPerson().getEthnicity());
        if (this.personPanel.getPerson().getCreationDate() != null) {
            Calendar cal = new GregorianCalendar();
            cal.setTime(this.personPanel.getPerson().getCreationDate());
            this.contactDate.setDate(cal.getTime());
        } else {
            this.contactDate.setEnabled(false);
        }
        if (this.personPanel.getPerson().getDob() != null) {
            Calendar cal = new GregorianCalendar();
            cal.setTime(this.personPanel.getPerson().getDob());
            this.dob.setDate(cal.getTime());
        } else {
            this.dob.setDate(null);
        }
        if (this.personPanel.getPerson().getDod() != null) {
            Calendar cal = new GregorianCalendar();
            cal.setTime(this.personPanel.getPerson().getDod());
            this.dod.setDate(cal.getTime());
            this.isActive.setSelected(false);
            this.isActive.setEnabled(false);
        } else {
            this.dod.setDate(null);
        }
        if (this.personPanel.getPerson().getLocNews() != null) {
            this.locNews.setSelectedIndex(this.personPanel.getPerson().getLocNews().ordinal() + 1);
        } else {
            this.locNews.setSelectedIndex(0);
        }
        if (this.personPanel.getPerson().getNatNews() != null) {
            this.natNews.setSelectedIndex(this.personPanel.getPerson().getNatNews().ordinal() + 1);
        } else {
            this.natNews.setSelectedIndex(0);
        }
        if (this.personPanel.getPerson().getReports() != null) {
            this.reports.setSelectedIndex(this.personPanel.getPerson().getReports().ordinal() + 1);
        } else {
            this.reports.setSelectedIndex(0);
        }
        if (this.personPanel.getPerson().getMags() != null) {
            this.mags.setSelectedIndex(this.personPanel.getPerson().getMags().ordinal() + 1);
        } else {
            this.mags.setSelectedIndex(0);
        }
        this.isActive.setSelected(this.personPanel.getPerson().isActive());
        for (Comment c : this.personPanel.getPerson().getComments()) {
            this.comments.append(c.toString());
        }
    }

    public Person getPerson() {
        return this.personPanel.getPerson();
    }

    @Override
    public void setConnection(Connection con) {
        this.con = con;
        faith.setModel(getFaithModel());
        ethnicity.setModel(getEthnicityModel());
    }

    private ComboBoxModel getEthnicityModel() {
        DefaultComboBoxModel model = new DefaultComboBoxModel();
        model.addElement("Please Select");
        if (!Beans.isDesignTime() && con != null) {
            try {
                Statement s = con.createStatement();
                ResultSet rs = s.executeQuery("select ethnicity from ethnicity;");
                while (rs.next()) {
                    model.addElement(rs.getString("ethnicity"));
                }
                s.close();
                rs.close();
            } catch (SQLException e) {
                // work out a good way to handle this.
                // dying isn't elegant, but if the db is MIA, not much else to do.
            }
        }
        model.addElement("Refused");
        return model;
    }

    private ComboBoxModel getFaithModel() {
        DefaultComboBoxModel model = new DefaultComboBoxModel();
        model.addElement("Please Select");
        if (!Beans.isDesignTime() && con != null) {
            try {
                Statement s = con.createStatement();
                ResultSet rs = s.executeQuery("select faith from faith;");
                while (rs.next()) {
                    model.addElement(rs.getString("faith"));
                }
                s.close();
                rs.close();
            } catch (SQLException e) {
                // work out a good way to handle this.
                // dying isn't elegant, but if the db is MIA, not much else to do.
            }
        }
        model.addElement("Refused");
        return model;
    }

    public boolean checkMandatoryFields() {
        String check = fName.getText();
        if (check == null || check.equals("")) {
            JOptionPane.showMessageDialog(null, "No First Name", "A first name is required", JOptionPane.ERROR_MESSAGE);
            fName.grabFocus();
        }
        check = lName.getText();
        if (check == null || check.equals("")) {
            JOptionPane.showMessageDialog(null, "No Last Name", "A last name is required", JOptionPane.ERROR_MESSAGE);
            lName.grabFocus();
            return false;
        }
        if (!contact.getContacts().isValid()) {
            JOptionPane.showMessageDialog(null, "No contact phone/email", "A phone number or email is required", JOptionPane.ERROR_MESSAGE);
            contact.grabFocus();
            return false;
        }
        if (gender.getSelectedIndex() == 0) {
            JOptionPane.showMessageDialog(null, "No Gender", "A selection must be made for gender", JOptionPane.ERROR_MESSAGE);
            gender.grabFocus();
            return false;
        }
        if (ethnicity.getSelectedIndex() == 0) {
            JOptionPane.showMessageDialog(null, "No Ethnicity", "A selection must be made for ethinicity", JOptionPane.ERROR_MESSAGE);
            ethnicity.grabFocus();
            return false;
        }
        if (faith.getSelectedIndex() == 0) {
            JOptionPane.showMessageDialog(null, "No Faith", "A selection must be made for faith", JOptionPane.ERROR_MESSAGE);
            faith.grabFocus();
            return false;
        }
        if (title.getSelectedIndex() == 0) {
            JOptionPane.showMessageDialog(null, "No Title", "A selection must be made for title", JOptionPane.ERROR_MESSAGE);
            title.grabFocus();
            return false;
        }
        if (!address.getAddress().isValid()) {
            JOptionPane.showMessageDialog(null, "Invalid Address", "A valid address must be entered", JOptionPane.ERROR_MESSAGE);
            address.grabFocus();
            return false;
        }
        if (this.dob.getDate() != null && this.dob.getDate().after(Calendar.getInstance().getTime())) {
            JOptionPane.showMessageDialog(null, "Invalid Birth Date", "Date of Birth cannot be after today's date.", JOptionPane.ERROR_MESSAGE);
            dob.grabFocus();
            return false;
        }
        if (this.dod.getDate() != null && this.isActive.isSelected() && this.dod.getDate().after(Calendar.getInstance().getTime())) {
            JOptionPane.showMessageDialog(null, "Invalid Death Date", "A person cannot be dead and active.", JOptionPane.ERROR_MESSAGE);
            dod.grabFocus();
            return false;
        }
        if(this.contactDate.getDate() == null)
        {
            JOptionPane.showMessageDialog(null, "Contact date is mandatory", "Contact date needs to be supplied", JOptionPane.ERROR_MESSAGE);
            contactDate.grabFocus();
            return false;            
        }
        return true;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        title = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        fName = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        lName = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        address = new h4hdb.guiComponents.util.addressPanel();
        contact = new h4hdb.guiComponents.util.contactPanel();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        comments = new javax.swing.JTextArea();
        jLabel6 = new javax.swing.JLabel();
        gender = new javax.swing.JComboBox();
        jLabel7 = new javax.swing.JLabel();
        ethnicity = new javax.swing.JComboBox();
        faith = new javax.swing.JComboBox();
        jLabel8 = new javax.swing.JLabel();
        prefName = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        referral = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        addComment = new javax.swing.JButton();
        isActive = new javax.swing.JCheckBox();
        dob = new com.toedter.calendar.JDateChooser();
        contactDate = new com.toedter.calendar.JDateChooser();
        jLabel12 = new javax.swing.JLabel();
        dod = new com.toedter.calendar.JDateChooser();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        locNews = new javax.swing.JComboBox();
        natNews = new javax.swing.JComboBox();
        reports = new javax.swing.JComboBox();
        mags = new javax.swing.JComboBox();

        jLabel1.setText("Title");

        title.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Please Select", "NONE", "MR", "MRS", "MISS", "MS", "DR" }));
        title.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                titleActionPerformed(evt);
            }
        });

        jLabel2.setText("First Name");

        jLabel3.setText("Last Name");

        jLabel4.setText("Preferred Name");

        jLabel5.setText("Comments");

        comments.setColumns(20);
        comments.setEditable(false);
        comments.setRows(5);
        jScrollPane1.setViewportView(comments);

        jLabel6.setText("Gender");

        gender.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Please select", "Male", "Female", "N/A", "Other", "Refused" }));
        gender.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                genderActionPerformed(evt);
            }
        });

        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel7.setText("Ethnicity");

        ethnicity.setEditable(true);
        ethnicity.setModel(getEthnicityModel());
        ethnicity.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ethnicityActionPerformed(evt);
            }
        });

        faith.setEditable(true);
        faith.setModel(getFaithModel());
        faith.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                faithActionPerformed(evt);
            }
        });

        jLabel8.setText("Faith");

        jLabel9.setLabelFor(referral);
        jLabel9.setText("Referrer");

        jLabel10.setLabelFor(contactDate);
        jLabel10.setText("Contact Date");

        jLabel11.setLabelFor(dob);
        jLabel11.setText("Date of Birth");

        addComment.setText("Add Comment");
        addComment.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addCommentActionPerformed(evt);
            }
        });

        isActive.setSelected(true);
        isActive.setText("is Active");
        isActive.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                isActiveStateChanged(evt);
            }
        });

        dob.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                dobFocusLost(evt);
            }
        });

        contactDate.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                contactDateFocusLost(evt);
            }
        });

        jLabel12.setText("Date of Death");

        dod.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                dodFocusLost(evt);
            }
        });

        jLabel13.setText("Local News");

        jLabel14.setText("National News");

        jLabel15.setText("Annual Reports");

        jLabel16.setText("Magazines");

        locNews.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "None", "Electronic", "Hard Copy", "Both" }));
        locNews.setSelectedIndex(1);
        locNews.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                locNewsActionPerformed(evt);
            }
        });

        natNews.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "None", "Electronic", "Hard Copy", "Both" }));
        natNews.setSelectedIndex(1);
        natNews.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                natNewsActionPerformed(evt);
            }
        });

        reports.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "None", "Electronic", "Hard Copy", "Both" }));
        reports.setSelectedIndex(1);
        reports.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reportsActionPerformed(evt);
            }
        });

        mags.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "None", "Electronic", "Hard Copy", "Both" }));
        mags.setSelectedIndex(1);
        mags.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                magsActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(260, 260, 260)
                        .addComponent(contact, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(38, 38, 38)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(ethnicity, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(gender, 0, 155, Short.MAX_VALUE)
                                    .addComponent(faith, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel11)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(dob, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(title, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(fName, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(33, 33, 33)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lName, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(prefName, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(118, 118, 118)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(dod, javax.swing.GroupLayout.DEFAULT_SIZE, 134, Short.MAX_VALUE)
                                    .addComponent(contactDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                            .addComponent(jLabel12)
                                            .addGap(7, 7, 7))
                                        .addComponent(jLabel10))
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel16, javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabel15, javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabel14, javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabel13, javax.swing.GroupLayout.Alignment.TRAILING)))
                                .addGap(9, 9, 9)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(natNews, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(reports, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(mags, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(locNews, 0, 122, Short.MAX_VALUE)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(address, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(24, 24, 24)
                                .addComponent(jLabel9)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(referral, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(isActive))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 487, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(addComment))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(title, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel2)
                        .addComponent(fName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel3)
                        .addComponent(lName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel4)
                        .addComponent(prefName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(address, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel9)
                            .addComponent(referral, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel10)
                            .addComponent(contactDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(dod, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel12, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(isActive, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel13)
                            .addComponent(locNews, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(11, 11, 11)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel14)
                            .addComponent(natNews, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(reports, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel15))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(jLabel16))
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(mags, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(contact, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel6)
                                    .addComponent(gender, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel7)
                                    .addComponent(ethnicity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel8)
                                    .addComponent(faith, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(8, 8, 8)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel11)
                                    .addComponent(dob, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(addComment))
                            .addComponent(jLabel5))))
                .addContainerGap(36, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void genderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_genderActionPerformed
        if (this.gender.getSelectedIndex() != 0) {
            this.personPanel.getPerson().setGender((String) this.gender.getSelectedItem());
        }
    }//GEN-LAST:event_genderActionPerformed

    private void titleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_titleActionPerformed
        if (this.title.getSelectedIndex() != 0) {
            this.personPanel.getPerson().setTitle(Title.valueOf(((String) this.title.getSelectedItem()).toUpperCase()));
        }
    }//GEN-LAST:event_titleActionPerformed

    private void ethnicityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ethnicityActionPerformed
        if (this.ethnicity.getSelectedIndex() != 0) {
            this.personPanel.getPerson().setEthnicity((String) this.ethnicity.getSelectedItem());
        }
        String eth = sqlSanitizer.sanitizeString((String) ethnicity.getSelectedItem());
        if (ethnicity.getSelectedIndex() < 0) {
            try {
                Statement s = con.createStatement();
                int result = s.executeUpdate("INSERT IGNORE INTO ethnicity VALUES ( " + eth + ");");
                s.close();
            } catch (SQLException ex) {
                Logger.getLogger(addPersonPanel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_ethnicityActionPerformed

    private void faithActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_faithActionPerformed
        if (this.faith.getSelectedIndex() != 0) {
            this.personPanel.getPerson().setFaith((String) this.faith.getSelectedItem());
        }
        String fait = sqlSanitizer.sanitizeString((String) faith.getSelectedItem());
        if (faith.getSelectedIndex() < 0) {
            try {
                Statement s = con.createStatement();
                int result = s.executeUpdate("INSERT IGNORE INTO faith VALUES (" + fait + ");");
                s.close();
            } catch (SQLException ex) {
                Logger.getLogger(addPersonPanel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_faithActionPerformed

    private void isActiveStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_isActiveStateChanged
        this.personPanel.getPerson().setActive(this.isActive.isSelected());
    }//GEN-LAST:event_isActiveStateChanged

    private void addCommentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addCommentActionPerformed
        CommentDialog com = new CommentDialog(this.getFrame(this.getParent()), true);
        com.setVisible(true);
        if (!com.getComment().getText().equalsIgnoreCase("")) {
            Comment c = com.getComment();
            c.setPID(this.personPanel.getPerson().getPID());
            this.personPanel.getPerson().addComment(c);
            this.comments.append(c.toString());
        }
        com.dispose();
    }//GEN-LAST:event_addCommentActionPerformed

    private void contactDateFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_contactDateFocusLost
        if (this.contactDate.getDate() != null) {
            this.personPanel.getPerson().setCreationDate(new Date(this.contactDate.getDate().getTime()));
        }
    }//GEN-LAST:event_contactDateFocusLost

    private void dodFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_dodFocusLost
        if (this.dod.getDate() != null) {
            this.personPanel.getPerson().setDod(new Date(this.dod.getDate().getTime()));
        } else {
            this.personPanel.getPerson().setDod(null);
        }
    }//GEN-LAST:event_dodFocusLost

    private void dobFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_dobFocusLost
        if (this.dob.getDate() != null) {
            this.personPanel.getPerson().setDob(new Date(this.dob.getDate().getTime()));
        }
    }//GEN-LAST:event_dobFocusLost

    private void locNewsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_locNewsActionPerformed
        if (this.locNews.getSelectedIndex() != 0) {
            this.personPanel.getPerson().setLocNews(Person.resolveCommType((String) this.locNews.getSelectedItem()));
        } else {
            this.personPanel.getPerson().setLocNews(commType.None);
        }
    }//GEN-LAST:event_locNewsActionPerformed

    private void natNewsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_natNewsActionPerformed
        if (this.natNews.getSelectedIndex() != 0) {
            this.personPanel.getPerson().setNatNews(Person.resolveCommType((String) this.natNews.getSelectedItem()));
        } else {
            this.personPanel.getPerson().setNatNews(commType.None);
        }
    }//GEN-LAST:event_natNewsActionPerformed

    private void reportsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reportsActionPerformed
        if (this.reports.getSelectedIndex() != 0) {
            this.personPanel.getPerson().setReports(Person.resolveCommType((String) this.reports.getSelectedItem()));
        } else {
            this.personPanel.getPerson().setReports(commType.None);
        }
    }//GEN-LAST:event_reportsActionPerformed

    private void magsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_magsActionPerformed
        if (this.mags.getSelectedIndex() != 0) {
            this.personPanel.getPerson().setMags(Person.resolveCommType((String) this.mags.getSelectedItem()));
        } else {
            this.personPanel.getPerson().setMags(commType.None);
        }
    }//GEN-LAST:event_magsActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addComment;
    private h4hdb.guiComponents.util.addressPanel address;
    private javax.swing.JTextArea comments;
    private h4hdb.guiComponents.util.contactPanel contact;
    private com.toedter.calendar.JDateChooser contactDate;
    private com.toedter.calendar.JDateChooser dob;
    private com.toedter.calendar.JDateChooser dod;
    private javax.swing.JComboBox ethnicity;
    private javax.swing.JTextField fName;
    private javax.swing.JComboBox faith;
    private javax.swing.JComboBox gender;
    private javax.swing.JCheckBox isActive;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField lName;
    private javax.swing.JComboBox locNews;
    private javax.swing.JComboBox mags;
    private javax.swing.JComboBox natNews;
    private javax.swing.JTextField prefName;
    private javax.swing.JTextField referral;
    private javax.swing.JComboBox reports;
    private javax.swing.JComboBox title;
    // End of variables declaration//GEN-END:variables

    public void clear() {
        this.address.clear();
        this.contact.clear();
        this.comments.setText("");
        this.title.setSelectedIndex(0);
        this.gender.setSelectedIndex(0);
        this.faith.setSelectedIndex(0);
        this.ethnicity.setSelectedIndex(0);
        this.locNews.setSelectedIndex(1);
        this.natNews.setSelectedIndex(1);
        this.reports.setSelectedIndex(1);
        this.mags.setSelectedIndex(1);
        this.fName.setText("");
        this.lName.setText("");
        this.prefName.setText("");
        this.referral.setText("");
        this.isActive.setSelected(true);
        this.contactDate.setDate(null);
        this.dob.setDate(null);
        this.dod.setDate(null);
    }

    private Frame getFrame(Component comp) {
        if (comp instanceof mainFrame) {
            return (Frame) comp;
        } else {
            return getFrame(comp.getParent());
        }
    }

    public Address getAddress() {
        return this.address.getAddress();
    }

    public Contacts getContacts() {
        return this.contact.getContacts();
    }

    private void updateStrings() {
        this.personPanel.getPerson().setfName(this.fName.getText());
        this.personPanel.getPerson().setlName(this.lName.getText());
        this.personPanel.getPerson().setPrefName(this.prefName.getText());
        this.personPanel.getPerson().setReferrer(this.referral.getText());
    }

    private void addTextListeners() {
        this.fName.getDocument().addDocumentListener(listener);
        this.lName.getDocument().addDocumentListener(listener);
        this.prefName.getDocument().addDocumentListener(listener);
        this.referral.getDocument().addDocumentListener(listener);
    }

    private class TextFieldListener implements DocumentListener {

        @Override
        public void insertUpdate(DocumentEvent e) {
            updateStrings();
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            updateStrings();
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            updateStrings();
        }
    }

    public Date getCreationDate () {
        if(this.contactDate.getDate() == null)
            return null;
        else
            return convertDate(this.contactDate.getCalendar());
    }

    public Date getDOB () {
        if(this.dob.getDate() == null)
            return null;
        else
            return convertDate(this.dob.getCalendar());
    }

    public Date getDOD () {
        if(this.dod.getDate() == null)
            return null;
        else
            return convertDate(this.dod.getCalendar());
    }

    private java.sql.Date convertDate(Calendar date) {
//        String year = "" + date.get(date.YEAR);
//        String month = "" + (date.get(date.MONTH) + 1);
//        while(month.length() < 2)
//            month = "0" + month;
//        String day = "" + date.get(date.DAY_OF_MONTH);
//        while(day.length() < 2)
//            day = "0" + day;
//        String dateString = "" + year + "-" + month + "-" + day;
        return new java.sql.Date(date.getTimeInMillis());
//        return java.sql.Date.valueOf(dateString);
    }

    public boolean getActive() {
        return isActive.isSelected(); 
    }

    public commType getLocNews() {
        return Person.resolveCommType((String) this.locNews.getSelectedItem());
    }

    public commType getNatNews() {
        return Person.resolveCommType((String) this.natNews.getSelectedItem());
    }

    public commType getReports() {
        return Person.resolveCommType((String) this.reports.getSelectedItem());
    }

    public commType getMags() {
        return Person.resolveCommType((String) this.mags.getSelectedItem());
    }
}
