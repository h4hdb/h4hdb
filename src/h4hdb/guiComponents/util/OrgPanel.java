/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package h4hdb.guiComponents.util;

import h4hdb.data.Comment;
import h4hdb.data.Contact;
import h4hdb.data.tableModels.ContactTableModel;
import h4hdb.data.Org;
import h4hdb.guiComponents.ConnectedComponent;
import h4hdb.guiComponents.dialogs.CommentDialog;
import h4hdb.guiComponents.dialogs.ContactDialog;
import h4hdb.mainFrame;
import java.awt.Component;
import java.awt.Frame;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.TableModel;

/**
 *
 * @author lelitu
 */
public class OrgPanel extends javax.swing.JPanel implements ConnectedComponent {

    private Connection con;
    private Org org;

    /**
     * Creates new form addOrgPanel
     */
    public OrgPanel() {
        this.org = new Org();
        initComponents();
        this.orgName.getDocument().addDocumentListener(new TextFieldListener());
    }

    @Override
    public void setConnection(Connection con) {
        this.con = con;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        address = new h4hdb.guiComponents.util.addressPanel();
        jLabel1 = new javax.swing.JLabel();
        orgName = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        comments = new javax.swing.JTextArea();
        addComment = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        contactsTable = new javax.swing.JTable();
        addContact = new javax.swing.JButton();
        editContact = new javax.swing.JButton();
        removeContact = new javax.swing.JButton();

        jLabel1.setLabelFor(orgName);
        jLabel1.setText("Org Name");

        jLabel2.setText("Comments");

        comments.setColumns(20);
        comments.setEditable(false);
        comments.setRows(5);
        jScrollPane1.setViewportView(comments);

        addComment.setText("Add Comment");
        addComment.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addCommentActionPerformed(evt);
            }
        });

        contactsTable.setModel(this.getContactModel());
        jScrollPane2.setViewportView(contactsTable);

        addContact.setText("Add Contact");
        addContact.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addContactActionPerformed(evt);
            }
        });

        editContact.setText("Edit Contact");
        editContact.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editContactActionPerformed(evt);
            }
        });

        removeContact.setText("Delete");
        removeContact.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeContactActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(address, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(orgName, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 437, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(addContact)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(removeContact, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(editContact, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(addComment)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jLabel2)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 451, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(39, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(orgName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(address, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(addContact)
                                .addGap(4, 4, 4)
                                .addComponent(editContact)
                                .addGap(4, 4, 4)
                                .addComponent(removeContact)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(addComment)
                .addContainerGap(30, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void clear() {
        this.org = new Org();
        orgName.setText("");
        address.clear();
    }
    private void addCommentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addCommentActionPerformed
        CommentDialog d = new CommentDialog(this.getFrame(this.getParent()), true);
        d.setVisible(true);
        if (!d.getComment().getText().equalsIgnoreCase("")) {
            Comment c = d.getComment();
            c.setOID(this.org.getOID());
            this.org.addComment(c);
            this.comments.append(c.toString());
        }
        d.dispose();
    }//GEN-LAST:event_addCommentActionPerformed

    private void addContactActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addContactActionPerformed
        ContactDialog d = new ContactDialog(this.getFrame(this.getParent()), true, con);
        d.setVisible(true);
        if (d.getContact().isValid()) {
            ((ContactTableModel) this.contactsTable.getModel()).addRow(d.getContact());
        }
        d.dispose();
    }//GEN-LAST:event_addContactActionPerformed

    private void editContactActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editContactActionPerformed
        int index = this.contactsTable.getSelectedRow();
        if (index == -1) {
            return;
        }
        ContactTableModel m = (ContactTableModel) this.contactsTable.getModel();
        Contact c = m.getUnderlyingRow(this.contactsTable.convertRowIndexToModel(index));
        ContactDialog d = new ContactDialog(this.getFrame(this.getParent()), true, c, con);
        d.setVisible(true);
        m.replaceRow(index, d.getContact());
        d.dispose();
    }//GEN-LAST:event_editContactActionPerformed

    private void removeContactActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeContactActionPerformed
        if (this.contactsTable.getSelectedRowCount() == 0) {
            return;
        }
        int[] indexs = this.contactsTable.getSelectedRows();
        ContactTableModel m = (ContactTableModel) this.contactsTable.getModel();
        for (int i : indexs) {
            this.org.removeContact(m.getUnderlyingRow(this.contactsTable.convertRowIndexToModel(i)));
            m.removeRow(this.contactsTable.convertRowIndexToModel(i));
        }
    }//GEN-LAST:event_removeContactActionPerformed

    public boolean checkConstraints() {
        if (orgName.getText() == null || orgName.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "A name is required", "No Name", JOptionPane.ERROR_MESSAGE);
            orgName.grabFocus();
            return false;
        }
        if (!address.getAddress().isValid()) {
            JOptionPane.showMessageDialog(null, "A valid address must be entered", "Invalid Address", JOptionPane.ERROR_MESSAGE);
            address.grabFocus();
            return false;
        }
        if (contactsTable.getRowCount() == 0) {
            JOptionPane.showMessageDialog(null, "A contact person is required", "No contact person", JOptionPane.ERROR_MESSAGE);
            addContact.grabFocus();
            return false;
        }
        return true;
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addComment;
    private javax.swing.JButton addContact;
    private h4hdb.guiComponents.util.addressPanel address;
    private javax.swing.JTextArea comments;
    private javax.swing.JTable contactsTable;
    private javax.swing.JButton editContact;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField orgName;
    private javax.swing.JButton removeContact;
    // End of variables declaration//GEN-END:variables

    private Frame getFrame(Component comp) {
        if (comp instanceof mainFrame) {
            return (Frame) comp;
        } else {
            return getFrame(comp.getParent());
        }
    }

    private void updateStrings() {
        this.org.setName(this.orgName.getText());
    }

    public Org getOrg() {
        this.org.setAddr(this.address.getAddress());
        return this.org;
    }

    public void setOrg(Org org) {
        this.org = org;
        this.orgName.setText(org.getName());
        this.address.setAddress(org.getAddr());
        ((ContactTableModel) this.contactsTable.getModel()).replaceValues(org.getContacts());
        this.comments.setText("");
        for (Comment c : org.getComments()) {
            this.comments.append(c.toString());
        }
    }

    private TableModel getContactModel() {
            return new ContactTableModel(this.org.getContacts());
    }

    private class TextFieldListener implements DocumentListener {

        @Override
        public void insertUpdate(DocumentEvent e) {
            updateStrings();
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            updateStrings();
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            updateStrings();
        }
    }
}