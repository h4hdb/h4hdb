/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package h4hdb.guiComponents;

import h4hdb.data.Person;
import h4hdb.data.sqlSanitizer;
import h4hdb.guiComponents.dialogs.LoginDialog;
import h4hdb.mainFrame;
import java.awt.Component;
import java.awt.Frame;
import java.beans.Beans;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;

/**
 *
 * @author lelitu
 */
public class addPersonToTeam extends javax.swing.JFrame implements ConnectedComponent {

    private Connection con;
    private int tid;

    /**
     * Creates new form addPersonToTeam required by netbeans guibubuilder, do
     * not use.
     */
    public addPersonToTeam() {
        initComponents();
        this.setVisible(true);
    }

    /**
     * Creates new form for adding members to a team.
     *
     * @param teamName --- Name of the team people will be added to
     * @param con --- database connection to use.
     */
    public addPersonToTeam(String teamName, int tid, Connection con) {
        this.con = con;
        this.tid = tid;
        initComponents();
        this.teamName.setText(teamName);
        this.pack();
    }

    public void setConnection(Connection con) {
        this.con = con;
        this.people.setModel(this.getPersonModel());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        role = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        teamName = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        people = new javax.swing.JComboBox();
        member = new javax.swing.JRadioButton();
        boardMember = new javax.swing.JRadioButton();
        title = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        addMember = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setLabelFor(teamName);
        jLabel1.setText("Team :");

        jLabel3.setLabelFor(people);
        jLabel3.setText("Person:");

        people.setModel(this.getPersonModel());

        role.add(member);
        member.setSelected(true);
        member.setText("Member");

        role.add(boardMember);
        boardMember.setText("Board Member");
        boardMember.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                boardMemberItemStateChanged(evt);
            }
        });

        title.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Please Select", "President", "Secretary", "Treasurer", " " }));
        title.setEnabled(false);

        jLabel4.setText("Position :  ");

        addMember.setText("Add Member");
        addMember.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addMemberActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(teamName, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(people, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(title, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(38, 38, 38)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(boardMember)
                        .addComponent(member))
                    .addComponent(addMember))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(teamName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(member)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel3)
                        .addComponent(people, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(boardMember))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel4)
                        .addComponent(title, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(addMember))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void boardMemberItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_boardMemberItemStateChanged
        this.title.setEnabled(this.boardMember.isSelected());
    }//GEN-LAST:event_boardMemberItemStateChanged

    private void addMemberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addMemberActionPerformed
        if (!this.checkConstraints()) {
            return;
        }
        try {
            if (con.isClosed()) {
                LoginDialog l = new LoginDialog(this.getFrame(this.getParent()), true);
                l.setVisible(true);
                l.dispose();
            }
            Statement s = con.createStatement();
            int pid = ((Person) this.people.getSelectedItem()).getPID();
            String query;
            if (this.title.isEnabled()) {
                query = "INSERT INTO Member VALUES (DEFAULT," + pid + "," + tid
                        + "," + sqlSanitizer.sanitizeString((String) this.title.getSelectedItem()) + ");";
            } else {
                query = "INSERT INTO Member VALUES (DEFAULT," + pid + "," + tid
                        + ",NULL);";
            }
            int result = s.executeUpdate(query);
            if (result == 1) {
                JOptionPane.showMessageDialog(this, "Team member added successfully", "Success", JOptionPane.INFORMATION_MESSAGE);
                this.clear();
            }
            s.close();
        } catch (SQLException ex) {
        }
    }//GEN-LAST:event_addMemberActionPerformed

    private boolean checkConstraints() {
        if (this.people.getSelectedIndex() == 0) {
            JOptionPane.showMessageDialog(this, "A person must be selected to add", "No Person", JOptionPane.ERROR_MESSAGE);
            this.people.grabFocus();
            return false;
        }
        if (this.boardMember.isSelected() && this.title.getSelectedIndex() == 0) {
            JOptionPane.showMessageDialog(this, "A board member must have a title", "No Title", JOptionPane.ERROR_MESSAGE);
            this.title.grabFocus();
            return false;
        }
        return true;
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addMember;
    private javax.swing.JRadioButton boardMember;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JRadioButton member;
    private javax.swing.JComboBox people;
    private javax.swing.ButtonGroup role;
    private javax.swing.JLabel teamName;
    private javax.swing.JComboBox title;
    // End of variables declaration//GEN-END:variables

    private ComboBoxModel getPersonModel() {
        DefaultComboBoxModel model = new DefaultComboBoxModel();
        model.addElement(new Person(-1, "Please Select"));
        if (!Beans.isDesignTime() && con != null) {
            try {
                Statement s = con.createStatement();
                ResultSet rs = s.executeQuery("select PID, IFNULL(PrefName,CONCAT(FName, ' ', LName)) as name from Person;");
                while (rs.next()) {
                    model.addElement(new Person(rs.getInt("PID"), rs.getString("name")));
                }
                rs.close();
                s.close();
            } catch (SQLException e) {
                // work out a good way to handle this.
                // dying isn't elegant, but if the db is MIA, not much else to do.
            }
        }
        return model;
    }

    private void clear() {
        this.people.setSelectedIndex(0);
        this.title.setSelectedIndex(0);
        this.member.setSelected(true);
        this.title.setEnabled(false);
    }

    private Frame getFrame(Component comp) {
        if (comp instanceof mainFrame) {
            return (Frame) comp;
        } else {
            return getFrame(comp.getParent());
        }
    }
}
