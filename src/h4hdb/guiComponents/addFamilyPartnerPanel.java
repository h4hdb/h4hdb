/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package h4hdb.guiComponents;

import h4hdb.data.FamilyPartner;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Mehendra
 */
public class addFamilyPartnerPanel extends javax.swing.JPanel implements ConnectedComponent{
    private FamilyPartner familyPartnership;
    private Connection con;

    /**
     * Creates new form addFamilyPartnerPanel
     */
    public addFamilyPartnerPanel() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        familyPartnershipPanel = new h4hdb.guiComponents.util.FamilyPartnershipPanel();
        addButton = new javax.swing.JButton();

        addButton.setText("Save Partnership");
        addButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(727, Short.MAX_VALUE)
                .addComponent(addButton)
                .addGap(31, 31, 31))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                    .addContainerGap(17, Short.MAX_VALUE)
                    .addComponent(familyPartnershipPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(17, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(501, Short.MAX_VALUE)
                .addComponent(addButton)
                .addGap(37, 37, 37))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(familyPartnershipPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 482, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(68, Short.MAX_VALUE)))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void addButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addButtonActionPerformed
    familyPartnership = this.familyPartnershipPanel.getFamilyPartnershipFromControls();
    if(familyPartnership.isValid())
    {
        try {            
            familyPartnership.insertIntoDB(con);
            JOptionPane.showMessageDialog(this, "Family partner successfully saved.");
        } catch (SQLException ex) {
            Logger.getLogger(addPersonPanel.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }
    else{
        JOptionPane.showMessageDialog(null, familyPartnership.getErrors().toArray()[0], "Not enough details",JOptionPane.ERROR_MESSAGE);
    }
    }//GEN-LAST:event_addButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addButton;
    private h4hdb.guiComponents.util.FamilyPartnershipPanel familyPartnershipPanel;
    // End of variables declaration//GEN-END:variables

    @Override
    public void setConnection(Connection con) {
        this.con = con;
        this.familyPartnershipPanel.setConnection(con);
    }
}
