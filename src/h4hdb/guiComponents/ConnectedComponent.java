/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package h4hdb.guiComponents;

import java.sql.Connection;

/**
 * Interface implemented by all h4hDB gui components
 * which store a DB connection, allows reconnects to propagate.
 * @author lelitu
 */
public interface ConnectedComponent {
    public void setConnection(Connection con);
}
