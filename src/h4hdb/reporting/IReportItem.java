/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package h4hdb.reporting;

/**
 *Any item that implements the following should be able to produce a valid HTML document based on a predefined 
 * XSLT.
 * @author Mehendra
 */
public interface IReportItem {
    //Converts an object to a valid XML item
    String getDataInXml();
    //Gets a XSLT file that could stule the XML thats returned on getDataInXml function
    String getXSLTFilename();
    //The html file where the transformed text would be written.
    String getOuptputFileName();
}
