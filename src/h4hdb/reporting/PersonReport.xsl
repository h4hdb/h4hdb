<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:template match="/">
<html>
<head>
    <link href="http://www.habitat.org.nz/index.php/ps_pagename/stylesheet" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" media="all" href="http://wwwhabitat.org.nz/scripts/calendar-green.css" title="" />    
    <title>List of people</title>
</head>
<body>
    <div style="padding:15px;">
    <div><img src="http://www.habitat.org.nz/images/logo.gif" /></div>
    <br /><br/>    
        
    <h2>Address : <xsl:value-of select="property/propertyname"/></h2>
    <p>Following people are working at the above property</p>
    <table border="1">    
    <tr>
    <th>First Name</th>
    <th>Last Name</th>
    </tr>
    <xsl:for-each select="property/people/person">
    <tr>
    <td><xsl:value-of select="firstname"/></td>
    <td><xsl:value-of select="lastname"/></td>
    </tr>
    </xsl:for-each>
    </table>        
    </div>
</body></html>
</xsl:template>
</xsl:stylesheet>