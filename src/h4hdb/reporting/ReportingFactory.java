/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package h4hdb.reporting;

import h4hdb.configSettings.configReader;
import java.io.FileOutputStream;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.EmptyStackException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
/**
 * A single point of entry class for all reporting actions.
 * @author Mehendra
 */
public class ReportingFactory {
    private String xsltPath;
    private String outputPath;
    private TransformerFactory tFactory;
    
    /*
     * Constructor for creating the report factory, method is left private and the responsibility
     * of initiating it with the correct parameters are handed to the factory itself.
     */
   
    private ReportingFactory() {
        configReader cf = new configReader();
        xsltPath = cf.read("ReportXSLTPath");
        outputPath = cf.read("DefaultReportCrationPath");
        tFactory = TransformerFactory.newInstance();
    }
    
    /*
     * Gets the instance of the singleton
     */
    public static ReportingFactory getInstance() {
        return ReportingFactoryHolder.INSTANCE;
    }
    
    /*
     * Create a reporting and output the path of the report.
     */
    public String createReport(IReportItem reportData) 
    {
        try{
            
            //TODO some cleanup code (Delete all files that are about an hour old)
            String filePath = xsltPath  +  reportData.getXSLTFilename();
            Transformer transformer = tFactory.newTransformer (new javax.xml.transform.stream.StreamSource(filePath));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HHmmss");
            
            cleanupFiles(outputPath);
            filePath = outputPath  +  sdf.format(new Date()) + "_" + reportData.getOuptputFileName() + ".html";
            StringReader reader = new StringReader(reportData.getDataInXml());
            transformer.transform (new javax.xml.transform.stream.StreamSource(reader),
            new javax.xml.transform.stream.StreamResult(new FileOutputStream(filePath)));
            java.awt.Desktop.getDesktop().browse(java.net.URI.create("file:///" + filePath));
            return filePath;
        }
        catch(Exception ex)
        {
            return null;
        }

    }
            
    private void cleanupFiles(String filePathToDelete)
    {
        //TODO Delete all the old files
    }
    
    private static class ReportingFactoryHolder {

        private static final ReportingFactory INSTANCE = new ReportingFactory();
    }
}
