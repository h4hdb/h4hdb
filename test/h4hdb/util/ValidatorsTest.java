/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package h4hdb.util;

import org.junit.AfterClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Mehendra
 */
public class ValidatorsTest {
    
    public ValidatorsTest() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of isNumeric method, of class Validators.
     */
    @Test
    public void testIsNumeric() {
        System.out.println("testIsNumeric");
        
        assertTrue(Validators.isNumeric("7777"));//True, as a string is a number 
        
        assertTrue(Validators.isNumeric("77.77"));//True, as this string can be converted to an int 
       
        assertTrue(Validators.isNumeric("77"));//True, dont fail on an empty string

        assertTrue(Validators.isNumeric(null));//True, dont fail on a null
   
        assertFalse(Validators.isNumeric("$7777"));//False, as a this is not considered a number
    }

    /**
     * Test of isCorrectFieldLength method, of class Validators.
     */
    @Test
    public void testIsCorrectFieldLength() {
        System.out.println("isCorrectFieldLength");
        boolean expResult = true;
        boolean result =  Validators.isCorrectFieldLength("field",8);
        
        assertEquals(expResult, result);//True, as the field length is less than 8
        result = Validators.isCorrectFieldLength("12345678",8);
        
        assertEquals(expResult, result);//True as field length matches exactly        
        result = Validators.isCorrectFieldLength("",8);
        
        assertEquals(expResult, result);//True, as field is an empty string
        result = Validators.isCorrectFieldLength(null,8);
        
        assertEquals(expResult, result);//True, as field is null
        result = Validators.isCorrectFieldLength("123456789",8);
        
        assertNotSame(expResult, result);//False, as the field is too long
    }
}
