/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package h4hdb.util;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Mehendra
 */
public class ConvertersTest {
    
    public ConvertersTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of convertToInt method, of class Converters.
     */
    @Test
    public void testConvertToInt() {
        System.out.println("convertToInt");        
        String stringToConvert = "";
        int expResult = 0;
        int result = Converters.convertToInt(stringToConvert);
        assertEquals(expResult, result);
        
        assertEquals(1278, Converters.convertToInt("12.78"));
        
        assertEquals(0, Converters.convertToInt("1.278"));
        
        assertEquals(127800, Converters.convertToInt("1278."));
        
        assertEquals(127800, Converters.convertToInt("1278"));
    }
    
    @Test
    public void testaddtwoDecimalPlaces(){
        System.out.println("add two decimal places");
        assertEquals("12.68", Converters.addtwoDecimalPlaces(1268));
    }
}