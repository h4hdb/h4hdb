/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package h4hdb.mockItems;

import h4hdb.reporting.IReportItem;
/**
 *
 * @author Mehendra
 */
public class ReportDataItemMock implements IReportItem{

    @Override
    public String getDataInXml() {
        return "<?xml version=\"1.0\"?>"
                + "<people>"
                + "<person><firstname>John</firstname><lastname>Doe</lastname></person>"
                + "<person><firstname>Chuck</firstname><lastname>Norris</lastname></person>"
                + "<person><firstname>John</firstname><lastname>Carter</lastname></person>"
                + "<person><firstname>Bruce</firstname><lastname>Banner</lastname></person>"
                + "<person><firstname>Nick</firstname><lastname>Fury</lastname></person>"
                + "</people>";
    }

    @Override
    public String getXSLTFilename() {
        return "PersonReport.xsl";
    }

    @Override
    public String getOuptputFileName() {
        return "PersonReport";
    }
    
}
