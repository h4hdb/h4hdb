/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package h4hdb.data;

import h4hdb.DBConnectionSingleton;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Unit tests for the family partner class
 *
 * @author Mehendra
 */
public class FamilyPartnerTest {

    private FamilyPartner fm;
    private Address ad;
    private Connection con;

    private Date createDate(String dateString) {
        return java.sql.Date.valueOf(dateString);
    }

    public FamilyPartnerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        try {
            con = DBConnectionSingleton.getInstance().getDbConnection("h4hdbdevelopment", "On1T0ug5Pass@");
        } catch (SQLException ex) {
            int error = ex.getErrorCode();
            if (error == 1044 || error == 1045) {
            } else {
                System.exit(1);
            }
        } catch (ClassNotFoundException ex) {
            System.exit(1);
        }

        ad = new Address("234", "kelburn parade", "Kel;burn", "Wellington", "Wellington Region", "New Zealand", "5019");
        fm = new FamilyPartner(10, ad, "SAMPLE STAGE", FamilyPartnerType.Build, 123, 879, true, createDate("2012-03-06"), createDate("2012-05-06"), createDate("2013-05-06"), createDate("2013-04-06"));

    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getfID method, of class FamilyPartner.
     */
    @Test
    public void testGetfID() {
        System.out.println("getfID");
        int expResult = 10;
        int result = fm.getfID();
        assertEquals(expResult, result);
    }

    /**
     * Test of getLocation method, of class FamilyPartner.
     */
    @Test
    public void testGetLocation() {
        System.out.println("getLocation");
        Address result = fm.getLocation();
        assertEquals(ad, result);
    }

    /**
     * Test of getStage method, of class FamilyPartner.
     */
    @Test
    public void testGetStage() {
        System.out.println("getStage");
        String expResult = "SAMPLE STAGE";
        String result = fm.getStage();
        assertEquals(expResult, result);
    }

    /**
     * Test of getType method, of class FamilyPartner.
     */
    @Test
    public void testGetType() {
        System.out.println("getType");
        FamilyPartnerType expResult = FamilyPartnerType.Build;
        FamilyPartnerType result = fm.getType();
        assertEquals(expResult, result);
    }

    /**
     * Test of getValue method, of class FamilyPartner.
     */
    @Test
    public void testGetValue() {
        System.out.println("getValue");
        int expResult = 123;
        int result = fm.getValue();
        assertEquals(expResult, result);
    }

    /**
     * Test of getCost method, of class FamilyPartner.
     */
    @Test
    public void testGetCost() {
        System.out.println("getCost");
        int expResult = 879;
        int result = fm.getCost();
        assertEquals(expResult, result);
    }

    /**
     * Test of isConsentRequired method, of class FamilyPartner.
     */
    @Test
    public void testIsConsentRequired() {
        System.out.println("isConsentRequired");
        boolean expResult = true;
        boolean result = fm.isConsentRequired();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDateOfConsent method, of class FamilyPartner.
     */
    @Test
    public void testGetDateOfConsent() {
        System.out.println("getDateOfConsent");
        Date expResult = createDate("2012-03-06");
        Date result = fm.getDateOfConsent();
        assertEquals(expResult, result);
    }

    /**
     * Test of getStartDate method, of class FamilyPartner.
     */
    @Test
    public void testGetStartDate() {
        System.out.println("getStartDate");
        Date expResult = createDate("2012-05-06");
        Date result = fm.getStartDate();
        assertEquals(expResult, result);
    }

    /**
     * Test of getEndDate method, of class FamilyPartner.
     */
    @Test
    public void testGetEndDate() {
        System.out.println("getEndDate");
        Date expResult = createDate("2013-05-06");
        Date result = fm.getEndDate();
        assertEquals(expResult, result);
    }

    /**
     * Test of getActualEndDate method, of class FamilyPartner.
     */
    @Test
    public void testGetActualEndDate() {
        System.out.println("getActualEndDate");
        Date expResult = createDate("2013-04-06");
        Date result = fm.getActualEndDate();
        assertEquals(expResult, result);
    }

    /**
     * Test of getErrors method, of class FamilyPartner.
     */
    @Test
    public void testGetErrors() {
        System.out.println("getErrors");
        List result = fm.getErrors();
        assertTrue(result.isEmpty());
        FamilyPartner fm2 = new FamilyPartner(10, ad, "SAMPLE STAGE", FamilyPartnerType.Build, 123, 879, true, createDate("2012-03-06"), createDate("2020-05-06"), createDate("2013-05-06"), createDate("2013-04-06"));
        fm2.isValid();
        assertFalse(fm2.getErrors().isEmpty());
    }

    /**
     * Test of isValid method, of class FamilyPartner.
     */
    @Test
    public void testIsValid() {
        System.out.println("isValid");

        assertTrue(fm.isValid());

        FamilyPartner instance = new FamilyPartner(10, ad, "SAMPLE STAGE", FamilyPartnerType.Build, 123, 879, true, new Date(2012, 3, 6), new Date(2020, 5, 6), new Date(2014, 5, 6), new Date(2013, 4, 6));
        assertFalse(instance.isValid());
        assertTrue(instance.getErrors().contains("Start date has to before the end date"));

    }

    @Test
    public void testIsValidFieldLengths() {
        String longString = "Abcdefg hijklmn opqrstuv wxyz. Abcdefg hijklmn opqrstuv wxyz Abcdefg hijklmn opqrstuv wxyzAbcdefg hijklmn opqrstuv wxyzAbcdefg hijklmn opqrstuv wxyzAbcdefg hijklmn opqrstuv wxyz Abcdefg xkchhga hkskdhgf";
        String smallString = "Abcdef ghyui";
        System.out.println("testIsValidFieldLengths");
        FamilyPartner instance = new FamilyPartner(10, ad, "SAMPLE STAGE More Chars to fail", FamilyPartnerType.Build, 123, 879, true, createDate("2012-03-06"), createDate("2012-05-06"), createDate("2013-05-06"), createDate("2013-04-06"));
        assertFalse(instance.isValid());
        assertTrue(instance.getErrors().contains("Stage has to be less than 15 characters"));

        instance = new FamilyPartner(10, ad, "SAMPLE STAGE", FamilyPartnerType.Build, 123, 879, true, new Date(2012, 3, 6), new Date(2020, 5, 6), new Date(2014, 5, 6), new Date(2013, 4, 6));
        ad.setAddr1(longString);
        assertFalse(instance.isValid());
        ad.setAddr1(smallString);

        ad.setAddr2(longString);
        assertFalse(instance.isValid());
        ad.setAddr2(smallString);

        ad.setCity(longString);
        assertFalse(instance.isValid());
        ad.setCity(smallString);
    }

    private FamilyPartner loadObjectFromDb(int fid) {
        try {
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery("SELECT * FROM fampartner WHERE fid=" + fid);
            FamilyPartner famPartner = null;
            if (rs.next()) {
                famPartner = new FamilyPartner(rs);
            }
            rs.close();
            s.close();
            return famPartner;
        } catch (SQLException ex) {
            return null;
        }
    }

    /*
     * Had to test all the basic database ineractions together becasue of the dependencies
     */
    @Test
    public void testCRUDDatabaseIneractions() {
        System.out.println("CRUDDatabaseIneractions");
        try {
           // fm.addPeople(new ArrayList<Person>());
            fm.insertIntoDB(con);
            FamilyPartner fmFromDb = loadObjectFromDb(fm.getfID());
            assertTrue(fm.equals(fmFromDb));

            fm.setStage("JustChanged");
            fm.updateIntoDB(con);
            fmFromDb = loadObjectFromDb(fm.getfID());
            assertTrue(fm.equals(fmFromDb));

            fm.deleteFromDB(con);
            fmFromDb = loadObjectFromDb(fm.getfID());
            assertTrue(fmFromDb == null);

            assert true;
        } catch (SQLException ex) {
            assert false;
        }
    }

//    /**
//     * Add people to family partner test
//     *
//     */
//    @Test
//    public void famPartnerAddPeople() {
//        try {        
//            int count = 0;
//            int pid1 = 0, pid2 = 0;
//
//            List<Person> people = new ArrayList<Person>();
//            Person p1 = new Person();
//            Person p2 = new Person();
//            p1.setPID(1);
//            p2.setPID(2);
//            people.add(p1);
//            people.add(p2);
//            fm.addPeople(people);
//            fm.insertIntoDB(con);
//            Statement s = con.createStatement();
//            
//            ResultSet rs = s.executeQuery("SELECT * FROM ponfampartner WHERE fid = 10");
//            System.out.println("query successful");
//            System.out.println(rs == null);
//            while (rs.next()) {
//                if (count == 0) {
//                    pid1 = rs.getInt(2);
//                } else if (count == 1) {
//                    pid2 = rs.getInt(2);
//                }
//                System.out.println("count: "+ count);
//                count++;
//            }
//            fm.deleteFromDB(con);
//            System.out.println("delete successful");
//            assertTrue(count == 2);
//            assertTrue(pid1 == 1);
//            assertTrue(pid2 == 2);
//        } catch (SQLException ex) {
//            Logger.getLogger(FamilyPartnerTest.class.getName()).log(Level.SEVERE, null, ex);
//            assert false;
//        }
//    }
}