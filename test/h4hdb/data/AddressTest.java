package h4hdb.data;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class AddressTest {
	private Address ad; 
	private String testString = "testing 123"; 

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
    	
        ad = new Address("234","kelburn parade","Kelburn","Wellington","Wellington Region","New Zealand","5019");
	}

	@After
	public void tearDown() throws Exception {
        ad = new Address("234","kelburn parade","Kelburn","Wellington","Wellington Region","New Zealand","5019");
	}

	/**
	 * test is empty method call
	 */
	@Test
	public void isEmptyTest() {
		assertFalse("ad is empty", ad.isEmpty()); 
		assertTrue("New address is not empty. ", new Address().isEmpty()); 
	}

	/**
	 * test set addr1 method call
	 */
	@Test
	public void setAddr1Test() {
		ad.setAddr1(testString); 
		assertTrue("Set/Get addr1 failed", ad.getAddr1().equalsIgnoreCase(testString)); 
	}

	/**
	 * test set addr2 method call
	 */
	@Test
	public void setAddr2Test() {
		ad.setAddr2(testString); 
		assertTrue("Set/Get addr2 failed", ad.getAddr2().equalsIgnoreCase(testString)); 
	}

	/**
	 * test set suburb method call
	 */
	@Test
	public void setSuburbTest() {
		ad.setSuburb(testString); 
		assertTrue("Set/Get suburb failed", ad.getSuburb().equalsIgnoreCase(testString)); 
	}

	/**
	 * test set region method call
	 */
	@Test
	public void setRegionTest() {
		ad.setRegion(testString); 
		assertTrue("Set/Get region failed", ad.getRegion().equalsIgnoreCase(testString)); 
	}

	/**
	 * test set city method call
	 */
	@Test
	public void setCityTest() {
		ad.setCity(testString); 
		assertTrue("Set/Get city failed", ad.getCity().equalsIgnoreCase(testString)); 
	}

	/**
	 * test set region method call
	 */
	@Test
	public void setCountryTest() {
		ad.setCountry(testString); 
		assertTrue("Set/Get country failed", ad.getCountry().equalsIgnoreCase(testString)); 
	}

	/**
	 * test set postcode method call
	 */
	@Test
	public void setPostCodeTest() {
		ad.setPostCode(testString); 
		assertTrue("Set/Get postcode failed", ad.getPostCode().equalsIgnoreCase(testString)); 
	}

	/**
	 * TODO
	 * test set equal method call
	 */
	@Test
	public void equalTest() {
		assertTrue("Equal test have variance", ad.equals(new Address("234","kelburn parade","Kelburn","Wellington","Wellington Region","New Zealand","5019"))); 
	}

	/**
	 * TODO
	 * test set hashcode method call
	 */
	@Test
	public void setHashCodeTest() {
		
	}

}
