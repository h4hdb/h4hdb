package h4hdb.data;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class EventTest {
	
	private Event e;
	private Address ad;
    private Connection con; 
	
    private Date createDate(String dateString){
        return java.sql.Date.valueOf(dateString);
    }

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		try {
            String driverName = "org.gjt.mm.mysql.Driver";
            Class.forName(driverName);

            String serverName = "BAOBAB.AD.ECS.VUW.AC.NZ:3306";
            String mydatabase = "h4h";
            String url = "jdbc:mysql://" + serverName + "/" + mydatabase;

            con = DriverManager.getConnection(url, "root", "adminadmin");
        } catch (SQLException ex) {
            int error = ex.getErrorCode();
            if (error == 1044 || error == 1045){
            	ex.printStackTrace(); 
            } else {
            	ex.printStackTrace(); 
                System.exit(1);
            }
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace(); 
            System.exit(1);
        }
    	
        ad = new Address("234","kelburn parade","Kelburn","Wellington","Wellington Region","New Zealand","5019");
        e = new Event(0, "Bogus FundRaiser", ad, createDate("2012-03-06")); 
        
	}

	@After
	public void tearDown() throws Exception {
        ad = new Address("234","kelburn parade","Kelburn","Wellington","Wellington Region","New Zealand","5019");
        e = new Event(0, "Bogus FundRaiser", ad, createDate("2012-03-06")); 
	}

	/**
	 * test getter for EID
	 */
	@Test
	public void testGetEID() {
		e.setEID(20); 
		assertEquals("Get EID return vary", 20, e.getEID()); 
	}

	/**
	 * test getter for address
	 */
	@Test
	public void testGetAddress() {
		assertTrue("Get address return vary", ad.equals(e.getAddress())); 
	}

	/**
	 * test getter for name
	 */
	@Test
	public void testGetName() {
		assertEquals("Get name return vary", "Bogus FundRaiser", e.getName()); 
	}

	/**
	 * test getter for DOE
	 */
	@Test
	public void testGetDOE() {
		assertEquals("Get DOE return vary", createDate("2012-03-06"), e.getDoe()); 
	}

	/**
	 * test setter for EID
	 */
	@Test
	public void testSetEID() {
		e.setEID(50); 
		assertEquals("Set EID return vary", 50, e.getEID()); 
	}

	/**
	 * test setter for address
	 */
	@Test
	public void testSetAddress() {
		Address test = new Address("123","kelburn","porirua","Wellington","Wellington Region","New Zealand","5011");
		e.setAddress(test); 
		assertTrue("Set address return vary", e.getAddress().equals(test)); 
	}

	/**
	 * test setter for name
	 */
	@Test
	public void testSetName() {
		e.setName("CRAZY JAMZ"); 
		assertEquals("Set name return vary", "CRAZY JAMZ", e.getName()); 
	}

	/**
	 * test setter for DOE
	 */
	@Test
	public void testSetDOE() {
		e.setDoe(createDate("2012-10-20")); 
		assertEquals("Set DOE return vary", createDate("2012-10-20"), e.getDoe()); 
	}

	/**
	 * test equal
	 */
	@Test
	public void testEqual() {
		assertTrue("Equal return vary", e.equals(e)); 
	}

	/**
	 * Loads the event from the database
	 * 
	 * @param eid
	 * @return an instance of the event from the database
	 */
    private Event loadObjectFromDb(int eid)
    {
        try {
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery("SELECT * FROM Event WHERE eid="+eid);
            Event event = null;
            if (rs.next()){
                event = new Event(rs);
            }
            rs.close();
            s.close();
            return event;
        } catch (SQLException ex) {
            return null;
        }        
    }

	/**
	 * test insert and delete sql query
	 */
    @Test
    public void testSQLInsertDelete() {
        try {
        	
			e.insertIntoDB(con);
			Event event = loadObjectFromDb(e.getEID()); 
			assertTrue(e.equals(event)); 
			
			e.deleteFromDB(con); 
            event = loadObjectFromDb(e.getEID());
            assertTrue(event == null);
            
            assert true; 
		} catch (SQLException e) {
			assert false; 
		} 
    }

	/**
	 * test update query
	 */
    @Test
    public void testSQLUpdate() {
        try {
        	
			e.insertIntoDB(con);
			Event event = loadObjectFromDb(e.getEID()); 
			assertTrue(e.equals(event)); 
			
			e.setName("JAMZ CRAZY"); 
			e.updateIntoDB(con); 
            event = loadObjectFromDb(e.getEID());
            assertTrue(e.equals(event)); 
			
			e.deleteFromDB(con); 
            event = loadObjectFromDb(e.getEID());
            assertTrue(event == null);
            
            assert true; 
		} catch (SQLException e) {
			assert false; 
		} 
    }

    //TODO black box testing for wrong inputs
}
