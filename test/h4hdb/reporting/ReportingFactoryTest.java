/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package h4hdb.reporting;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Mehendra
 */
public class ReportingFactoryTest {
    
    public ReportingFactoryTest() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of getInstance method, of class ReportingFactory.
     */
    @Test
    public void testGetInstance() {
        System.out.println("getInstance");
        ReportingFactory result = ReportingFactory.getInstance();
        assertNotNull(result);
    }

    /**
     * Test of createReport method, of class ReportingFactory.
     */
    @Test
    public void testCreateReport() {
        System.out.println("createReport");
        IReportItem reportData = new h4hdb.mockItems.ReportDataItemMock();
        ReportingFactory instance = ReportingFactory.getInstance();
        Assert.assertNotNull(instance.createReport(reportData));
    }
}
