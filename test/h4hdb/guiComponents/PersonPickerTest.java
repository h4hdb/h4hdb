/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package h4hdb.guiComponents;

import h4hdb.data.*;
import h4hdb.guiComponents.dialogs.personPicker;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;

/**
 *
 * @author Adam
 */
public class PersonPickerTest {
    
    public static void main(String[] args){
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                
    
        
        
        
        
    List<Comment> comments = new ArrayList<Comment>();
    List<PersonKeyword> keywords = new ArrayList<PersonKeyword>();
    Date date = new Date(1,2,3);
    Person p1 = new Person(01, Title.MR, "Adam", "Mills","Adam","049041162","","0225652326","aftmills@gmail.com",
            "2 Norna Crescent","","Kelburn","Wellington","WELLINGTON","New Zealand","6012",date,date,date,
            "Pakeha","Male","None","",true,commType.None,commType.None,commType.None,commType.None,comments,keywords);
    Person p2 = new Person(01, Title.MR, "John", "Mills","JM","049041162","","0225652326","aftmills@gmail.com",
            "4 Norna Crescent","","Kelburn","Wellington","WELLINGTON","New Zealand","6012",date,date,date,
            "Pakeha","Male","None","",true,commType.None,commType.None,commType.None,commType.None,comments,keywords);
     Person p3 = new Person(01, Title.MR, "John", "Smith","JS","049041162","","0225652326","aftmills@gmail.com",
            "123 Fake Street","","Mt Cook","Wellington","WELLINGTON","New Zealand","6012",date,date,date,
            "Pakeha","Male","None","",true,commType.None,commType.None,commType.None,commType.None,comments,keywords);
     Person p4 = new Person(01, Title.MR, "Tim", "Smith","TS","049041162","","0225652326","aftmills@gmail.com",
            "55 Apple Street","","Kelburn","Wellington","WELLINGTON","Australia","6012",date,date,date,
            "Pakeha","Male","None","",true,commType.None,commType.None,commType.None,commType.None,comments,keywords);
    Person p5 = new Person(01, Title.MR, "Jimmy", "Kingston","JK","049041162","","0225652326","aftmills@gmail.com",
            "2 Norna Crescent","","Kelburn","Wellington","WELLINGTON","New Zealand","1234",date,date,date,
            "Pakeha","Male","None","",true,commType.None,commType.None,commType.None,commType.None,comments,keywords);
     Person p6 = new Person(01, Title.MR, "Jimmy", "Kingston","Brian","049041162","","0225652326","aftmills@gmail.com",
            "2 Norna Crescent","","Mt Cook","Wellington","WELLINGTON","New Zealand","1234",date,date,date,
            "Pakeha","Male","None","",true,commType.None,commType.None,commType.None,commType.None,comments,keywords);
    
    List<Person> ppl = new ArrayList<Person>();
    ppl.add(p1);
    ppl.add(p2);
    ppl.add(p3);
    ppl.add(p4);
    ppl.add(p5);
    ppl.add(p6);
    java.awt.Frame parent = new java.awt.Frame();
    personPicker pp = new personPicker(parent, true, ppl);
    
   pp.addWindowListener(new java.awt.event.WindowAdapter() {

                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                pp.setVisible(true);
            }
        });
    
    }
}

//(int PID, Title title, String fName, String lName, String prefName,
//            String hPhone, String wPhone, String cPhone, String email,
//            String addr1, String addr2, String suburb, String city, String region,
//            String country, String postCode, Date dob, Date dod, Date creationDate,
//            String ethnicity, String gender, String faith, String referrrer, boolean active,
//            commType natNews, commType locNews, commType reports, commType mags, List<Comment> comments,
//            List<PersonKeyword> keywords)