/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package h4hdb.guiComponents;

import h4hdb.data.Address;
import h4hdb.data.FamilyPartner;
import h4hdb.data.FamilyPartnerType;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;

/**
 *
 * @author Fintan
 */
public class FamPartnerPickerTest {

    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                String dateString1 = "11-June-07";
                String dateString2 = "01-January-08";
                String dateString3 = "11-May-07";
                String dateString4 = "11-March-07";
                String dateString5 = "11-December-07";
                String dateString6 = "11-August-07";
                String dateString7 = "11-June-08";
                String dateString8 = "10-June-07";
                java.util.Date date1 = new java.util.Date();
                java.util.Date date2 = new java.util.Date();
                java.util.Date date3 = new java.util.Date();
                java.util.Date date4 = new java.util.Date();
                java.util.Date date5 = new java.util.Date();
                java.util.Date date6 = new java.util.Date();
                java.util.Date date7 = new java.util.Date();
                java.util.Date date8 = new java.util.Date();
                DateFormat formatter = new SimpleDateFormat("dd-MMM-yy");
                try {
                    date1 = (java.util.Date) formatter.parse(dateString1);
                    date2 = (java.util.Date) formatter.parse(dateString2);
                    date3 = (java.util.Date) formatter.parse(dateString3);
                    date4 = (java.util.Date) formatter.parse(dateString4);
                    date5 = (java.util.Date) formatter.parse(dateString5);
                    date6 = (java.util.Date) formatter.parse(dateString6);
                    date7 = (java.util.Date) formatter.parse(dateString7);
                    date8 = (java.util.Date) formatter.parse(dateString8);

//                    System.out.println("Today is " + new Date(date.getTime()));
                } catch (ParseException ex) {
                    Logger.getLogger(FamPartnerPickerTest.class.getName()).log(Level.SEVERE, null, ex);
                }
                List<FamilyPartner> fPartnerships = new ArrayList<FamilyPartner>();

                Address addr1 = new Address("12 Main Rd", "", "Lower Hutt", "Wellington", "Lower North Island", "New Zealand", "5011");
                Address addr2 = new Address("12 High", "Street", "Porirua", "Wellington", "Lower North Island", "New Zealand", "5011");
                Address addr3 = new Address("1025 Main Rd", "", "Wainuiomata", "Wellington", "Lower North Island", "New Zealand", "5011");
                Address addr4 = new Address("132 Kelburn Pde", "", "Johnsonsville", "Auckland", "Upper North Island", "New Zealand", "5011");
                Address addr5 = new Address("98 The Terrace", "", "Kelburn", "Wellington", "Lower South Island", "New Zealand", "5011");
                Address addr6 = new Address("12 The Esplanade", "", "Kelburn", "Wellington", "Upper South Island", "New Zealand", "5011");
                Address addr7 = new Address("122 Killer", "Crescent", "Karori", "Wellington", "Lower North Island", "New Zealand", "5011");
                Address addr8 = new Address("52 Main Road", "", "Upper Hutt", "Wellington", "Lower North Island", "New Zealand", "5011");

                FamilyPartner fp1 = new FamilyPartner(addr1, "Stage 1", FamilyPartnerType.BrushWithKindness, 12000, 10000, true, new Date(date1.getTime()), new Date(date8.getTime()));
                FamilyPartner fp2 = new FamilyPartner(addr2, "Stage 2", FamilyPartnerType.BrushWithKindness, 1200000, 1210000, true, new Date(date2.getTime()), new Date(date7.getTime()));
                FamilyPartner fp3 = new FamilyPartner(addr3, "Stage 3", FamilyPartnerType.BrushWithKindness, 900000, 802000, true, new Date(date3.getTime()), new Date(date6.getTime()));
                FamilyPartner fp4 = new FamilyPartner(addr4, "Stage 4", FamilyPartnerType.BrushWithKindness, 522000, 133000, true, new Date(date4.getTime()), new Date(date5.getTime()));
                FamilyPartner fp5 = new FamilyPartner(addr5, "Stage 1", FamilyPartnerType.BrushWithKindness, 1200620, 131000, true, new Date(date5.getTime()), new Date(date4.getTime()));
                FamilyPartner fp6 = new FamilyPartner(addr6, "Stage 2", FamilyPartnerType.BrushWithKindness, 120500, 10010, true, new Date(date6.getTime()), new Date(date3.getTime()));
                FamilyPartner fp7 = new FamilyPartner(addr7, "Stage 3", FamilyPartnerType.BrushWithKindness, 122000, 65200, true, new Date(date7.getTime()), new Date(date2.getTime()));
                FamilyPartner fp8 = new FamilyPartner(addr8, "Stage 4", FamilyPartnerType.BrushWithKindness, 125000, 131300, true, new Date(date8.getTime()), new Date(date1.getTime()));

                fPartnerships.add(fp8);
                fPartnerships.add(fp7);
                fPartnerships.add(fp6);
                fPartnerships.add(fp5);
                fPartnerships.add(fp4);
                fPartnerships.add(fp3);
                fPartnerships.add(fp2);
                fPartnerships.add(fp1);

                JFrame parent = new JFrame();
//                FPPicker fpPicker = new FPPicker(parent, true, fPartnerships);
                FamPartnerSearchTestDialog fpPicker = new FamPartnerSearchTestDialog(parent, true, fPartnerships);
                fpPicker.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                fpPicker.setVisible(true);
            }
        });

    }
}
